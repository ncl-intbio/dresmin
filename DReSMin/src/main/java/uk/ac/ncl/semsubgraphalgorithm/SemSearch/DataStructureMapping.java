/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.SemSearch;

import java.util.ArrayList;
import java.util.List;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.SimpleGraph;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.SourceGraphNEW;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedEdgeUID;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedNode;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedNodeUID;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Class represents the data structure for which mappings are held in during a
 * search.
 *
 */
public class DataStructureMapping {

    private UndirectedGraph<TypedNodeUID, TypedEdgeUID> structure;
    private TypedNodeUID rootNode;

    public DataStructureMapping(TypedNodeUID root) {
        this.rootNode = root;
        this.structure = new SimpleGraph<TypedNodeUID, TypedEdgeUID>(TypedEdgeUID.class);
        structure.addVertex(root);
    }

    public void addNode(TypedNodeUID add, TypedNodeUID[] core) {
        //add the node
        structure.addVertex(add);
        //then add the edge
        structure.addEdge(core[core.length-1], add, new TypedEdgeUID(core[core.length-1], add));

    }

    public UndirectedGraph<TypedNodeUID, TypedEdgeUID> getStructure() {
        return structure;
    }

    public TypedNodeUID getRootNode() {
        return rootNode;
    }

    /**
     * Returns the nodes from the shortest path in order.
     *
     * @param start
     * @param end
     * @param edges
     * @return
     */
    public TypedNodeUID[] sortPathUID(TypedNodeUID start, TypedNodeUID end, List<TypedEdgeUID> edges) {

        TypedNodeUID[] nodes = new TypedNodeUID[edges.size() + 1];
        nodes[0] = start;
        nodes[edges.size()] = end;

        //sort the nodes
        List<TypedEdgeUID> duplicate = new ArrayList<TypedEdgeUID>();
        duplicate.addAll(edges);

        int count = 1;
        while (count < edges.size()) {
            TypedEdgeUID delete = null;
            TypedNodeUID t = nodes[count - 1];
            for (TypedEdgeUID e : duplicate) {
                TypedNodeUID from = e.getV1();
                TypedNodeUID to = e.getV2();
                if (from == t) {
                    nodes[count] = e.getV2();
                    delete = e;
                    break;
                } else if (to == t) {
                    nodes[count] = e.getV1();
                    delete = e;
                    break;
                }
            }
            duplicate.remove(delete);
            count++;
        }
        return nodes;
    }

    /**
     * Returns the temp (match) core
     *
     * @param nodeInQuestion
     * @param targetNode
     * @return
     */
    public TypedNodeUID[] popUIDCore(TypedNodeUID targetNode) {
        DijkstraShortestPath tr2 = new DijkstraShortestPath(structure,
                rootNode, targetNode);

        TypedNodeUID[] nodes = null;
        TypedNodeUID start = (TypedNodeUID) tr2.getPath().getStartVertex();
        TypedNodeUID end = (TypedNodeUID) tr2.getPath().getEndVertex();
        List<TypedEdgeUID> edges = tr2.getPath().getEdgeList();
        nodes = sortPathUID(start, end, edges);
        return nodes;

    }

  public TypedNode[] removeUIDs(TypedNodeUID[] core) {

        TypedNode[] UID_core = new TypedNode[core.length];
        for (int i = 0; i < UID_core.length; i++) {
            UID_core[i] = core[i].getTypedNode();
        }
        return UID_core;
    }
}
