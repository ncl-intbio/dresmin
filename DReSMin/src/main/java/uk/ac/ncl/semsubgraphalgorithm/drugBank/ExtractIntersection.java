/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.drugBank;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.semsubgraphalgorithm.DrugTargetWork.Pair;
import uk.ac.ncl.semsubgraphalgorithm.drugBank.DrugBankDataManager.DrugBankVersion;
import uk.ac.ncl.semsubgraphalgorithm.management.OutPutManagement;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Class extracts all associations in file B that are not in file A.
 *
 */
public class ExtractIntersection {

    private final static Logger LOGGER = Logger.getLogger(ExtractIntersection.class.getName());
    private DrugBankVersion A;
    private DrugBankVersion B;
    private Set<Pair> associationsA;
    private Set<Pair> associationsB;
    private Set<Pair> intersection;
    private String outPutFile;
      
    public static void main (String [] args){
    
        OutPutManagement op = new OutPutManagement ();
        ExtractIntersection extract = new ExtractIntersection(DrugBankVersion.DATASET, DrugBankVersion.THREE, op.getDrugBank4Path()+"uniqueDB3_V_dataset.txt" );
        extract.run();
    }

    public ExtractIntersection(DrugBankVersion a, DrugBankVersion b, String outPutFile) {
        this.A = a;
        this.B = b;
        this.outPutFile = outPutFile;
        this.intersection = new HashSet<>();
    }
    
    
    public void run(){
        getAssociations();
        calcDifference();
        writeToFile();
    }

    public void getAssociations() {

        //get the first set of associations
        if (A == DrugBankVersion.DATASET) {
            DrugBankDataManager dbm = new DrugBankDataManager();
            dbm.parseDatasetRels();
            associationsA = dbm.getDatasetPairs();
        } else if (A == DrugBankVersion.THREE) {
            DrugBankDataManager dbm = new DrugBankDataManager(DrugBankVersion.THREE);
            try {
                dbm.parseDrugBankRels();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ExtractIntersection.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ExtractIntersection.class.getName()).log(Level.SEVERE, null, ex);
            }
            associationsA = dbm.getDrugBankPairs();
        } else if (A == DrugBankVersion.FOUR) {
            DrugBankDataManager dbm = new DrugBankDataManager(DrugBankVersion.FOUR);
            try {
                dbm.parseDrugBankRels();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ExtractIntersection.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ExtractIntersection.class.getName()).log(Level.SEVERE, null, ex);
            }
            associationsA = dbm.getDrugBankPairs();
        }

        //get the second set of associations
        if (B == DrugBankVersion.DATASET) {
            DrugBankDataManager dbm = new DrugBankDataManager();
            dbm.parseDatasetRels();
            associationsB = dbm.getDatasetPairs();
        } else if (B == DrugBankVersion.THREE) {
            DrugBankDataManager dbm = new DrugBankDataManager(DrugBankVersion.THREE);
            try {
                dbm.parseDrugBankRels();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ExtractIntersection.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ExtractIntersection.class.getName()).log(Level.SEVERE, null, ex);
            }
            associationsB = dbm.getDrugBankPairs();
        } else if (B == DrugBankVersion.FOUR) {
            DrugBankDataManager dbm = new DrugBankDataManager(DrugBankVersion.FOUR);
            try {
                dbm.parseDrugBankRels();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ExtractIntersection.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ExtractIntersection.class.getName()).log(Level.SEVERE, null, ex);
            }
            associationsB = dbm.getDrugBankPairs();
        }

    }

    public void calcDifference() {
        for (Pair p : associationsB) {
            if (!associationsA.contains(p)) {
                intersection.add(p);
            }
        }
    }

    public void writeToFile() {
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(new File(outPutFile)));
        } catch (IOException ex) {
            Logger.getLogger(ExtractIntersection.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (Pair p : intersection) {
            try {
                bw.append(p.toString() + '\n');
            } catch (IOException ex) {
                Logger.getLogger(ExtractIntersection.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try {
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(ExtractIntersection.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.INFO, "Written {0} associations to {1}", new Object[]{intersection.size(), outPutFile});

    }
}
