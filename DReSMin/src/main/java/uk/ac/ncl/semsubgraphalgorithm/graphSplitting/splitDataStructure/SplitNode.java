/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.graphSplitting.splitDataStructure;

import java.util.List;
import java.util.Set;
import uk.ac.ncl.semsubgraphalgorithm.graphSplitting.GraphSplitInstance;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedNode;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class SplitNode {
    
    private String nodeUID;
    private GraphSplitInstance split;
    private int STATE;
    private List<TypedNode[]> mergedMappings;
    private TypedNode [] s1SubOrder;
    private TypedNode [] s2SubOrder;
    private TypedNode [] mappedSubOrder;  
    private boolean splits1;
    private boolean splits2;
    private boolean checked;

    
    public SplitNode(GraphSplitInstance gs, int state, String UID){
        this.STATE = state;
        this.nodeUID = UID;
        this.split = gs;
        this.splits1 = false;
        this.splits2 = false;
        this.checked = false;
    }

    public boolean isSplits1() {
        return splits1;
    }

    public boolean isSplits2() {
        return splits2;
    }
    
    public void setChecked(boolean checked) {
        this.checked = checked;
    }
    

    public boolean isChecked() {
        return checked;
    }
    
    
    public void setSplits1(boolean splits1) {
        this.splits1 = splits1;
    }

    public void setSplits2(boolean splits2) {
        this.splits2 = splits2;
    }
     
    
    public String getUIDs(){
        return nodeUID;   
    }
    
    public void setSplit(GraphSplitInstance split) {
        this.split = split;
    }

    public void setSTATE(int STATE) {
        this.STATE = STATE;
    }

    public void setMergedMappings(List<TypedNode[]> mergedMappings) {
        this.mergedMappings = mergedMappings;
    }

    public void setS1SubOrder(TypedNode[] s1SubOrder) {
        this.s1SubOrder = s1SubOrder;
    }

    public void setS2SubOrder(TypedNode[] s2SubOrder) {
        this.s2SubOrder = s2SubOrder;
    }

    public void setMappedSubOrder(TypedNode[] mappedSubOrder) {
        this.mappedSubOrder = mappedSubOrder;
    }    
    
    public TypedNode[] getS1SubOrder() {
        return s1SubOrder;
    }

    public TypedNode[] getS2SubOrder() {
        return s2SubOrder;
    }

    public TypedNode[] getMappedSubOrder() {
        return mappedSubOrder;
    }   
    
    public GraphSplitInstance getSplit() {
        return split;
    }

    public int getSTATE() {
        return STATE;
    }

    public List<TypedNode[]> getMergedMappings() {
        return mergedMappings;
    }   
}
