/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.drugBank;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.Random;
import uk.ac.ncl.semsubgraphalgorithm.drugBank.DrugBankDataManager.DrugBankVersion;
import uk.ac.ncl.semsubgraphalgorithm.management.OutPutManagement;

/**
 *
 * @author joemullen
 */
//this class takes a list of DBIDs and a list of UNIPROTIds, and randomly creates N drug interactions
public class RandomDTInteractionTest {

    private Set<String> DBIDs;
    private Set<String> UNIPROTIDs;
    private Set<String> validPAIRS;
    private Set<String> randomRELATIONS;
    private int numberOfActualMatches;
    private DrugBankDataManager pi;
    private OutPutManagement op;

    public static void main(String[] args) throws FileNotFoundException, IOException {

        OutPutManagement op = new OutPutManagement();
        BufferedWriter bw = new BufferedWriter(new FileWriter(op.getDrugBank3Path()+"RandomD_T_PredictionAverage.txt"));
        int numberofrelations = 5000;
        int numberOfTests = 100;

        int [] relations =new int []{10,100,1000,10000,100000,1000000};
        
        float[] mean = new float[15];
        for (int d = 0; d < 6; d++) {
            int numberofrelations2 =  relations[d];
            System.out.println("num: "+numberofrelations2);
            int[] allScores = new int[numberOfTests];
            int total = 0;
            for (int i = 0; i < numberOfTests; i++) {
                RandomDTInteractionTest rt = new RandomDTInteractionTest(numberofrelations2);
                rt.createRandomRelations();
                int matches = rt.matches();
                allScores[i] = matches;
                total = total + matches;
            }
            System.out.println("[INFO] SUMMARY---------- " + numberofrelations2);
            float average = (float) total / numberOfTests;
            for (int y = 0; y < allScores.length; y++) {
                bw.append(allScores[y] + ",");
            }
            bw.append("\n");

            System.out.println("[INFO] Mean: " + average + "  " + allScores[0] + "  " + allScores[numberOfTests - 1]);
        }
        
        System.out.println("[INFO] MEAN: " + Arrays.toString(mean));
        bw.close();
    }

    public RandomDTInteractionTest(int number) throws FileNotFoundException, IOException {
        this.pi = new DrugBankDataManager(DrugBankVersion.THREE);
        pi.getDatassetInfo();
        pi.parseDrugBankRels();
        pi.getValidPairsInfo();
        this.DBIDs = pi.getallCompoundsDATASET();
        this.UNIPROTIDs = pi.getallProtiensDATASET();
        this.validPAIRS = new HashSet<String>();
        this.randomRELATIONS = new HashSet<String>();
        this.numberOfActualMatches = number;
        this.validPAIRS = pi.getallValidUNIQUEPairsDB();
        this.op = new OutPutManagement();
    }

    public void createRandomRelations() {


        String[] drugbank = new String[DBIDs.size()];
        int count = 0;
        for (String id : DBIDs) {
            drugbank[count] = id;
            count++;

        }

        String[] uniprot = new String[UNIPROTIDs.size()];
        int count2 = 0;
        for (String id : UNIPROTIDs) {
            uniprot[count2] = id;
            count2++;
        }

        for (int i = 0; i < numberOfActualMatches; i++) {
            Random random = new Random();
            int dbid = random.nextInt(drugbank.length);
            String db = drugbank[dbid];

            int unipro = random.nextInt(uniprot.length);
            String up = uniprot[unipro];
            randomRELATIONS.add(db + "\t" + up);

        }


    }

    public int matches() {

        int count = 0;

        for (String rel : randomRELATIONS) {

            if (validPAIRS.contains(rel)) {
                count++;
            }


        }

        return count;
    }
}
