/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.management;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class GZIP {
    
    private final static Logger LOGGER = Logger.getLogger(GZIP.class.getName());

    public static void main(String[] args) {
        String gzipfile = "/Users/joemullen/Desktop/testResults/[f9c10add_CTTT]_ST0.8_14268348991.txt.gz";
        // String gzipFile = "/Users/pankaj/sitemap.xml.gz";
        //String newFile = "/Users/pankaj/new_sitemap.xml";

        //compressGzipFile(file, gzipFile)
        GZIP gz = new GZIP();
        String newFile= gz.decompressGzipFile(gzipfile);
        gz.deleteFile(newFile);

    }

    /**
     * Decompress a gzip file
     *
     * @param gzipFile
     * @param newFile
     */
    public String decompressGzipFile(String gzipFile) {
        String newFile = gzipFile.replace(".gz", "");
        try {
            FileInputStream fis = new FileInputStream(gzipFile);
            GZIPInputStream gis = new GZIPInputStream(fis);
            FileOutputStream fos = new FileOutputStream(newFile);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = gis.read(buffer)) != -1) {
                fos.write(buffer, 0, len);
            }
            //close resources
            fos.close();
            gis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        LOGGER.log(Level.INFO, "Decompressed file {0} to {1}", new Object[]{gzipFile, newFile});
        return newFile;

    }

    /**
     * Delete the uncompressed file after use
     *
     * @param file
     */
    public void deleteFile(String file) {
        File delfile = new File(file);
        if (delfile.delete()) {
            LOGGER.log(Level.INFO, "{0} is deleted!", delfile.getName());
        } else {
           LOGGER.info("Delete operation failed....");
        }

    }


    /*
     * Compress a gzip file and delete the previous file.
     */
    private void compressGzipFile(String file) {
        String gzipFile = file.concat(".gz");
        try {
            FileInputStream fis = new FileInputStream(file);
            FileOutputStream fos = new FileOutputStream(gzipFile);
            GZIPOutputStream gzipOS = new GZIPOutputStream(fos);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = fis.read(buffer)) != -1) {
                gzipOS.write(buffer, 0, len);
            }
            //close resources
            gzipOS.close();
            fos.close();
            fis.close();
            //delete the non-zipped version of the file
            File delfile = new File(file);
            if (delfile.delete()) {
                System.out.println(delfile.getName() + " is deleted!");
            } else {
                System.out.println("Delete operation is failed.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
