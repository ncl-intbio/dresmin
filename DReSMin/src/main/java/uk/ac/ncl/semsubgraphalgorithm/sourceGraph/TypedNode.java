/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.sourceGraph;

import java.io.Serializable;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class TypedNode implements Serializable{

    String type;
    String name;
    int id;
    String pid;

    public TypedNode(String name, String type, String pid, int id) {
        this.name = name;
        this.type = type;
        this.pid = pid;
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public String getPid() {
        return pid;
    }
    
    @Override
    public String toString(){
        return name+"\t"+type+"\t"+id+"\t"+pid;
    }
}
