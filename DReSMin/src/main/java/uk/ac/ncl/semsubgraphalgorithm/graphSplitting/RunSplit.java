/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.graphSplitting;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.semsubgraphalgorithm.graphSplitting.splitDataStructure.DataStructureSplit;
import uk.ac.ncl.semsubgraphalgorithm.graphSplitting.splitDataStructure.SplitNode;
import uk.ac.ncl.semsubgraphalgorithm.queryGraphs.QueryGraph;
import uk.ac.ncl.semsubgraphalgorithm.queryGraphs.QueryGraph.SemanticSub;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class RunSplit {

    private DataStructureSplit data;
    private final static Logger LOGGER = Logger.getLogger(RunSplit.class.getName());
    private int splitMax;

    public RunSplit(QueryGraph Sub) {
        this.data = new DataStructureSplit(Sub);
        this.splitMax = 3;
    }

    public static void main(String[] args) {
        QueryGraph q = new QueryGraph(SemanticSub.SIXNODELINEAR);
        RunSplit r = new RunSplit(q);
        r.setSplitMax(2);
        r.split();
    }
    
    public void setSplitMax(int nodeSet){
        this.splitMax = nodeSet; 
    }

    /**
     * Method takes the original sub graph and splits iteratively until we have
     * our data-structure populated.
     */
    public void split() {
        LOGGER.info("Started splitting sub ...");
        //first split the original graph
        GraphSplit gp = new GraphSplit(data.getOrigiGraph(), false);
        GraphSplitInstance gs = gp.getSplit();
        SplitNode sp = new SplitNode(gs, 1, data.getOrigiGraph().getSubgraphName());
        data.addNode(sp, null);
        //then go through each node in the graph and see if we have any subs that have a nodeset of greater than 3
        boolean check = true;
        while (check) {
            check = splitAgain();
        }
        LOGGER.log(Level.INFO, "Finished splitting sub, we have {0} split node in the split datastructure...", data.getStructure().vertexSet().size());
        data.printTree();
        //data.traverse();
    }

    public boolean splitAgain() {
        boolean added = false;
        Set<SplitNode> nonDupe = data.getStructure().vertexSet();
        Set<SplitNode> dupe = new HashSet<SplitNode>();
        dupe.addAll(nonDupe);

        for (SplitNode check : dupe) {
            if (check.getSplit().getQueryGraph1().getSub().vertexSet().size() > splitMax && check.isSplits1() == false) {
                String name = check.getSplit().getQueryGraph1().getSubgraphName();
                GraphSplit gpone = new GraphSplit(check.getSplit().getQueryGraph1(), false);
                GraphSplitInstance gsone = gpone.getSplit();
                SplitNode spone = new SplitNode(gsone, 2, check.getSplit().getQueryGraph1().getSubgraphName());
                data.addNode(spone, name);
                check.setSplits1(true);
                added = true;
            }
            if (check.getSplit().getQueryGraph2().getSub().vertexSet().size() > splitMax && check.isSplits2() == false) {
                String name = check.getSplit().getQueryGraph2().getSubgraphName();
                GraphSplit gpone = new GraphSplit(check.getSplit().getQueryGraph2(), false);
                GraphSplitInstance gsone = gpone.getSplit();
                SplitNode spone = new SplitNode(gsone, 2, check.getSplit().getQueryGraph2().getSubgraphName());
                data.addNode(spone, name);
                check.setSplits2(true);
                added = true;
            }
        }
        return added;
    }

    public DataStructureSplit getData() {
        return data;
    }
}
