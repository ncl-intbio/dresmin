/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.drugBankWork.version4;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.*;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import uk.ac.ncl.semsubgraphalgorithm.DrugTargetWork.Pair;
import uk.ac.ncl.semsubgraphalgorithm.management.OutPutManagement;

/**
 * @author joemullen Class parsers in an XML representation of the DrugBank
 * database. Extracts: [Nodes] drugs (both small molecules and biotech) [Nodes]
 * human targets (mainly proteins) [Interactions] drug-drug [Interactions]
 * drug-target
 */
public class Parser {

    //required
    private String filepath;
    private Scanner sc;
    private OutPutManagement opMan;
    private final static Logger LOGGER = Logger.getLogger(Parser.class.getName());
    //specific to this parser
    private String ELEMENT = "drug";
    private final String DRUG_NAME_XPATH = "/drug/name";
    private final String DRUG_DBID_XPATH = "/drug/drugbank-id";
    private final String DRUG_TYPE_XPATH = "/drug/@type";
    private final String DRUG_GROUPS_XPATH = "/drug/groups";
    private final String DRUG_CATEGORIES_XPATH = "/drug/categories";
    private final String DRUG_AFFORG_XPATH = "/drug/affected-organisms";
    private final String TARGET_XPATH = "/drug/targets/target";
    private final String TRANSPORTER_TARGET_XPATH = "/drug/transporters/transporter";
    private final String ENZYME_TARGET_XPATH = "/drug/enzymes/enzyme";
    private final String INTERACTIONS_XPATH = "/drug/drug-interactions/drug-interaction";
    private final String DRUG_PROPERTIES_XPATH = "/drug/calculated-properties/property";
    private boolean parseDrug = false;
    private boolean element = false;
    private String content = "";
    private int count = 1;
    private BufferedWriter op;
    private Set<Pair> associations;
    private Set<Pair> unmappedassociations;
    private String opFile;
    private Set<String> uniqueDrugs;
    private Set<String> uniqueTargets;
    private Set<String> uniqueTarNotUni;
    
    public Parser(String filepath, String opFile) {
        this.filepath = filepath;
        this.associations = new HashSet<>();
        this.unmappedassociations = new HashSet<>();
        this.opMan = new OutPutManagement();
        this.opFile = opFile;
        this.uniqueDrugs = new HashSet<>();
        this.uniqueTargets = new HashSet<>();
        this.uniqueTarNotUni = new HashSet<>();
        try {
            this.op = new BufferedWriter(new FileWriter(new File(opMan.getDrugBank4Path() + opFile)));
        } catch (IOException ex) {
            Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void main(String[] args) throws Exception {
        
        String db4 = "/Volumes/MyPassport/Datasets/DrugBank/drugbank4pt2.xml";
        String op = "ALLdb4pairs.txt";
        Parser p = new Parser(db4, op);
        p.parse();
    }

    /**
     * Parse in the XML database as a stream
     *
     */
    public void parse() {
        BufferedReader br = null;
        
        try {
            String line;
            br = new BufferedReader(new FileReader(filepath));
            while ((line = br.readLine()) != null) {
                
                if (line.equals("</drug>")) {
                    content = content + line;
                    Document drugDoc = getXML(content);
                    //count++;
                    try {
                        getDrugs(drugDoc);
                    } catch (XPathExpressionException ex) {
                        Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    content = "";
                    parseDrug = false;
                }
                if (parseDrug) {
                    content = content + line;
                }
                if (line.startsWith("<drug ")) {
                    content = line;
                    parseDrug = true;
                }
            }
            
        } catch (IOException e) {
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException ex) {
            }
        }
        try {
            op.close();
        } catch (IOException ex) {
            Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        System.out.println(uniqueTarNotUni.toString());
        LOGGER.log(Level.INFO, "Added {0} associations to file {1}{2}", new Object[]{associations.size(), opMan.getDrugBank4Path(), opFile});
        LOGGER.log(Level.INFO, "Involving {0} uniqueDrugs", uniqueDrugs.size());
        LOGGER.log(Level.INFO, "Involving {0} uniqueTargets", uniqueTargets.size());
        LOGGER.log(Level.INFO, "{0} unmappable associations", unmappedassociations.size());
        LOGGER.log(Level.INFO, "{0} unique targets have no UniProt ID", uniqueTarNotUni.size());
        
    }

    /**
     * Convert a String to an XML document
     *
     * @param convert
     */
    public Document getXML(String convert) {
        
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Document document = null;
        try {
            document = builder.parse(new InputSource(new StringReader(convert)));
        } catch (SAXException | IOException ex) {
            Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return document;
        
    }

    /**
     * Retrieve all the required information from the XML document
     *
     * @param document
     * @throws XPathExpressionException
     */
    public void getDrugs(Document document) throws XPathExpressionException {
        
        NodeList nodes = document.getChildNodes();
        XPathFactory xpf = XPathFactory.newInstance();
        XPath xpath = xpf.newXPath();
        //drug attributes to be extracted     
        String dbID = null;
        String name = null;
        
        dbID = (String) xpath.evaluate(DRUG_DBID_XPATH, document, XPathConstants.STRING);
        //type = (String) xpath.evaluate(DRUG_TYPE_XPATH, document, XPathConstants.STRING);
        //name = (String) xpath.evaluate(DRUG_NAME_XPATH, document, XPathConstants.STRING);

        //get targets
        Set<String> targetIds = null;
        //get  targets
        NodeList targets = (NodeList) xpath.evaluate(TARGET_XPATH, document, XPathConstants.NODESET);
        targetIds = getTargets(xpath, targets, dbID);
        //targetIds.addAll(getTargets(xpath, targets));

        uniqueDrugs.add(dbID);
        System.out.println(dbID);
        //add all the pairs to set
        for (String id : targetIds) {
            Pair p = new Pair(dbID, id);
            uniqueTargets.add(id);
            associations.add(p);
            try {
                op.append(p.toString() + "\n");
            } catch (IOException ex) {
                Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }

    /**
     * Create target nodes and interactions from the drug to these
     *
     * @param xpath
     * @param targets
     */
    public Set<String> getTargets(XPath xpath, NodeList targets, String dbID) {
        
        Set<String> targetIDs = new HashSet<String>();
        for (int i = 0; i < targets.getLength(); i++) {
            try {
                String id = null;
                String SwissProtpolypeptideID = null;
                String tarName = null;
                
                try {
                    id = (String) xpath.evaluate("id", targets.item(i), XPathConstants.STRING);
                } catch (XPathExpressionException ex) {
                    Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                String polypepSource = (String) xpath.evaluate("polypeptide/@source", targets.item(i), XPathConstants.STRING);
                if (polypepSource.equals("Swiss-Prot")) {
                    SwissProtpolypeptideID = (String) xpath.evaluate("polypeptide/@id", targets.item(i), XPathConstants.STRING);
                }
                tarName = (String) xpath.evaluate("name", targets.item(i), XPathConstants.STRING);
                
                if (SwissProtpolypeptideID != null) {
                    targetIDs.add(SwissProtpolypeptideID);
                } else {
                    System.out.println("Swissprot ID = null " + count + " " + tarName);
                    uniqueTarNotUni.add(tarName);
                    unmappedassociations.add(new Pair(dbID, tarName));
                    count++;
                }
                
            } catch (XPathExpressionException ex) {
                Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return targetIDs;
    }

    /**
     * Returns the set of attributes from a nodeList, when it is the same tag to
     * be used each time.
     *
     * @param nodes
     * @param tag
     * @param xpath
     * @return
     */
    public Set<String> getAttributeFromNodeList(NodeList nodes, String tag, XPath xpath) {
        Set<String> attributes = new HashSet<>();
        for (int i = 0; i < nodes.getLength(); i++) {
            try {
                attributes.add((String) xpath.evaluate(tag, nodes.item(i), XPathConstants.STRING));
            } catch (XPathExpressionException ex) {
                Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        return attributes;
    }
}
