/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.spikingGraph;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import uk.ac.ncl.semsubgraphalgorithm.management.SerialisedGraph;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.SourceGraphNEW;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedEdge;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedNode;


/**
 *
 * @author joemullen
 */
public class DeleteRandomRelations {

    private SourceGraphNEW s;
    private DirectedGraph<TypedNode, TypedEdge> newtarget;
    private SourceGraphNEW spiked;
    private Random rand;
    private long startTime;
    private long endTime;
    private int relsDELETED;
    private String CCFROM;
    private String CCTO;
    private int deletions;
    private ArrayList<TypedNode> allTO;
    private ArrayList<TypedNode> allFROM;
    private boolean plug;
    private int plugged;

    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException {
        SerialisedGraph ser = new SerialisedGraph();
        SourceGraphNEW source = ser.useSerializedNEW("graph.data");
        //RandomGraphNEW sem = new RandomGraphNEW(source, 1000, 1);
        //DeleteRandomRelations r = new DeleteRandomRelations(sem.getRandomSourceGraph(), "Compound", "Target", 50, true);
    }

    public DeleteRandomRelations(SourceGraphNEW s, String from, String to, int deletions, boolean plug) throws IOException {

        this.s = s;
        this.newtarget = s.getSourceGraph();
        this.deletions = deletions;
        this.rand = new Random();
        this.CCFROM = from;
        this.CCTO = to;
        this.allTO = new ArrayList<TypedNode>();
        this.allFROM = new ArrayList<TypedNode>();
        this.relsDELETED = 0;
        this.plug = plug;

        if (this.plug == true) {
            getAllRelevantNodes();
            plugRelations();

        }

        deleteRelations();
        setNewSource();
        System.out.println(getSummary());

    }

    public void deleteRelations() throws IOException {
        getAllRelevantNodes();
        startTime = System.currentTimeMillis();

        while (relsDELETED < deletions && allFROM.size() > 0 && allTO.size() > 0) {
            deleteSingleRelation();
        }

        endTime = System.currentTimeMillis();

    }

    public void getAllRelevantNodes() {

        for (TypedNode node : newtarget.vertexSet()) {

            if (node.getType().equals(CCFROM)) {
                allFROM.add(node);
            } 
            if (node.getType().equals(CCTO)) {
                allTO.add(node);
            }
        }

    }

    public void deleteSingleRelation() {

        boolean deleted = false;

        if (allFROM.size() == 0 || allTO.size() == 0) {
            System.out.println("NO MORE EDGES TO DELETE");

        } else {

            int from = rand.nextInt(allFROM.size());
            TypedNode fromNODE = allFROM.get(from);

            int to = rand.nextInt(allTO.size());
            TypedNode toNODE = allTO.get(to);


            if (newtarget.containsEdge(fromNODE, toNODE)) {
                newtarget.removeEdge(fromNODE, toNODE);
                relsDELETED++;
                allFROM.remove(fromNODE);
                allTO.remove(toNODE);
            }
        }
    }

    //method creates relations between all nodes of type 'from' and all nodes 
    //of type 'to', meaning no more relations can be inferred other than those 
    //deleted
    public void plugRelations() {

        for (TypedNode node : allFROM) {
            for (TypedNode node2 : allTO) {
                if (!newtarget.containsEdge(node, node2)) {
                    newtarget.addEdge(node, node2);
                    plugged++;
                }
            }
        }

    }

    public void setNewSource() throws IOException {
        spiked = new SourceGraphNEW(newtarget);
        s = null;
    }

    public SourceGraphNEW getSpikedGraph() {
        return spiked;
    }

    public int deleteNumber() {
        return deletions;
    }

    public String getSummary() {
        return "-----------------------------------------------------" + "\n" +"[SPIKEGRAPH_DELETE TOOK: " + getTime() + " seconds]" + "\n" + "[DELETED: " + relsDELETED + "]" + "\n" + "[PLUGGED: " + plugged + "]" + "\n" + "[NEW_GRAPH_SIZE: " + spiked.getSourceGraph().vertexSet().size() + "/" + spiked.getSourceGraph().edgeSet().size() + "]";

    }

    public String getTime() {
        String time = (" " + (double) (endTime - startTime) / 1000);
        return time.trim();
    }
}
