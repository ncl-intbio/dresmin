/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.visualisation;

import java.io.IOException;
import java.util.HashMap;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.graphstream.graph.*;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.stream.file.FileSinkImages;
import org.graphstream.stream.file.FileSinkImages.LayoutPolicy;
import org.graphstream.stream.file.FileSinkImages.OutputType;
import org.graphstream.stream.file.FileSinkImages.RendererType;
import org.graphstream.stream.file.FileSinkImages.Resolutions;
import uk.ac.ncl.semsubgraphalgorithm.management.OutPutManagement;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedEdge;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedNode;

/**
 *
 * @author joemullen
 */
public class GS_Core_VisualisationNEW {

    private Set<TypedNode> nodes;
    private Set<TypedEdge> edges;
    private Graph graph;
    private String name;
    private Set<String> highLight;
    private boolean onlyToFile;
    private OutPutManagement op;
    private String location;

    public GS_Core_VisualisationNEW(Set<TypedNode> nodes, Set<TypedEdge> edges, String name, String location, boolean onlyToFile) {
        this.location = location;
        this.name = name;
        graph = new SingleGraph(name);
        this.nodes = nodes;
        this.edges = edges;
        this.onlyToFile = onlyToFile;
        this.op = new OutPutManagement();

    }

    public void visualise() {

        FileSinkImages pic = new FileSinkImages(OutputType.PNG, Resolutions.VGA);
        pic.setRenderer(RendererType.SCALA);

        pic.setLayoutPolicy(LayoutPolicy.COMPUTED_FULLY_AT_NEW_IMAGE);

        for (TypedNode n : nodes) {
            System.out.println(n.getName()+"\t"+ n.getId());
            String type = n.getType();
            String name = n.getName()+"_"+n.getId();
            graph.addNode(name);
            if (type.equals("Target")) {
                Node node2 = graph.getNode(name);
                node2.addAttribute("ui.class", "target");
            }
            if (type.equals("Compound")) {
                Node node2 = graph.getNode(name);
                node2.addAttribute("ui.class", "compound");
            }

            if (type.equals("MolFunc")) {
                Node node2 = graph.getNode(name);
                node2.addAttribute("ui.class", "molfunc");
            }


            if (type.equals("Indications")) {
                Node node2 = graph.getNode(name);
                node2.addAttribute("ui.class", "indication");
            }

            if (type.equals("Protein")) {
                Node node2 = graph.getNode(name);
                node2.addAttribute("ui.class", "protein");
            }

            if (type.equals("Publication")) {
                Node node2 = graph.getNode(name);
                node2.addAttribute("ui.class", "publication");
            }

        }
        for (TypedEdge e : edges) {
            graph.addEdge(e.getV1().getName()+"_"+e.getV1().getId() + e.getV2().getName()+"_"+e.getV2().getId() + ">>>" + e.getType(), e.getV1().getName()+"_"+e.getV1().getId(), e.getV2().getName()+"_"+e.getV2().getId(), true);
        }

        for (Node node : graph) {
            node.addAttribute("ui.label", node.getId());
        }

        for (Edge edge : graph.getEdgeSet()) {
            String[] edgeType = edge.getId().split(">>>");
            edge.addAttribute("ui.label", edgeType[edgeType.length - 1]);
        }

        System.setProperty("org.graphstream.ui.renderer",
                "org.graphstream.ui.j2dviewer.J2DGraphRenderer");


        if (onlyToFile == false) {
            graph.display();
        }


        graph.addAttribute(
                "ui.stylesheet", "url('" + op.getCSSFile() + "')");

        String directory = location;

        try {
            pic.writeAll(graph, directory + name + ".png");
        } catch (IOException ex) {
            Logger.getLogger(GS_Core_VisualisationNEW.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
