package uk.ac.ncl.semsubgraphalgorithm.queryGraphs;

import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedNode;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedEdge;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.GraphMethodsNEW;
import uk.ac.ncl.semsubgraphalgorithm.visualisation.GS_Core_VisualisationNEW;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author joemullen
 */
public class QueryGraph extends GraphMethodsNEW {

    private QueryGraph.SemanticSub semSubType;
    private String MOTIFNAME = "";
    private DirectedGraph<TypedNode, TypedEdge> Sub;
    private TypedEdge inferredEdge;

    public enum SemanticSub {

        CTT, SIXNODE, SIXNODELINEAR, CPT, TIT, TWONODE, BINDS_TO;
    }

    public static void main(String[] args) {
    }

    /**
     * Constructor takes the output from a shortest path String and creates a
     * query Graph.
     *
     * @param shorestePath
     */
    public QueryGraph(String shortestPath) {

        String[] split = shortestPath.split("\n");
        String name = split[0];
        String[] nodes = split[1].split("\t");
        String[] fromRels = split[2].split("\t");
        String[] toRels = split[3].split("\t");

        this.Sub = new DefaultDirectedGraph<TypedNode, TypedEdge>(
                TypedEdge.class);
        this.MOTIFNAME = name;

        TypedNode first = null;
        TypedNode last = null;
        //if there is only one possible subgraph create it

        for (int i = 1; i < nodes.length; i++) {
            //create the nodes
            TypedNode from = null;
            TypedNode to = new TypedNode((i) + ">>>" + nodes[i], nodes[i], (i) + nodes[i], i);
            //add the nodes to the graph
            if (i == 1) {
                from = new TypedNode((i - 1) + ">>>" + nodes[i - 1], nodes[i - 1], (i - 1) + nodes[i - 1], i - 1);

                //these are used to create the inferred relation
                first = from;
                Sub.addVertex(from);
            } else {
                for (TypedNode n : Sub.vertexSet()) {
                    if (n.getName().equals((i - 1) + ">>>" + nodes[i - 1])) {
                        from = n;
                        break;
                    }
                }
            }
            if (!Sub.containsVertex(to)) {
                Sub.addVertex(to);
            }
            //create the relations
            //from
            if (!fromRels[i - 1].equals("NONE")) {
                //create the relation
                TypedEdge t = new TypedEdge(from, to, fromRels[i - 1]);
                Sub.addEdge(from, to, t);

            }
            //to
            if (!toRels[i - 1].equals("NONE")) {
                TypedEdge t = new TypedEdge(to, from, toRels[i - 1]);
                Sub.addEdge(to, from, t);
            }

            if (i == nodes.length - 1) {
                last = to;

            }
        }
        this.inferredEdge = new TypedEdge(first, last, "binds_to");
        //<TODO> what do we do if there is more than one possible subgraphs????
    }

    public QueryGraph(DirectedGraph<TypedNode, TypedEdge> graph, String motifname) {
        this.MOTIFNAME = motifname;
        this.Sub = graph;
    }

    public QueryGraph(QueryGraph.SemanticSub sub) {
        this.semSubType = sub;
        this.Sub = new DefaultDirectedGraph<TypedNode, TypedEdge>(
                TypedEdge.class);

        switch (semSubType) {
            case CTT:
                CTT();
                break;

            case SIXNODE:
                SixNodes();
                break;

            case SIXNODELINEAR:
                SixNodesLinear();
                break;

            case CPT:
                CPT();
                break;

            case TWONODE:
                twoNode();
                break;

            case TIT:
                TIT();
                break;

            case BINDS_TO:
                bindsTo();
                break;
        }

    }

    private void TIT() {
        TypedNode tar1 = new TypedNode("1>>>Tar", "Target", "test", 1);
        TypedNode ind = new TypedNode("2>>>Ind", "Indications", "test", 2);
        TypedNode tar2 = new TypedNode("3>>>Tar", "Target", "test", 3);
        //addNodes
        TypedEdge t = new TypedEdge(tar1, ind, "disgenet_involved_in");
        TypedEdge t2 = new TypedEdge(tar2, ind, "disgenet_involved_in");

        //add all the nodes
        Sub.addVertex(tar1);
        Sub.addVertex(tar2);
        Sub.addVertex(ind);
        //add all the edges
        Sub.addEdge(tar1, ind, t);
        Sub.addEdge(tar2, ind, t2);


        setSubgraphName("TIT");

    }

    private void CPT() {
        TypedNode comp1 = new TypedNode("1>>>Compound", "Compound", "test", 1);
        TypedNode targ2 = new TypedNode("2>>>Target", "Target", "test", 2);
        TypedNode prot3 = new TypedNode("3>>>Protein", "Protein", "test", 3);
        //addNodes
        TypedEdge t = new TypedEdge(comp1, targ2, "binds_to");
        TypedEdge t2 = new TypedEdge(targ2, prot3, "has_similar_sequence");
        TypedEdge t3 = new TypedEdge(prot3, targ2, "has_similar_sequence");
        //add all the nodes
        Sub.addVertex(comp1);
        Sub.addVertex(targ2);
        Sub.addVertex(prot3);
        //add all the edges
        Sub.addEdge(comp1, targ2, t);
        Sub.addEdge(targ2, prot3, t2);
        Sub.addEdge(prot3, targ2, t3);

        TypedEdge inferred = new TypedEdge(comp1, prot3, "binds_to");
        setInferredEdge(inferred);
        setSubgraphName("CPT");

    }

    /**
     * Binds to
     */
    private void bindsTo() {
        TypedNode comp1 = new TypedNode("1>>>Compound", "Compound", "test", 1);
        TypedNode targ2 = new TypedNode("2>>>Target", "Target", "test", 2);
        //addNodes
        TypedEdge t = new TypedEdge(comp1, targ2, "binds_to");

        //add all the nodes
        Sub.addVertex(comp1);
        Sub.addVertex(targ2);

        //add all the edges
        Sub.addEdge(comp1, targ2, t);

        // TypedEdge inferred = new TypedEdge(comp1, targ3, "binds_to");
        //setInferredEdge(inferred);
        setSubgraphName("binds_To");

    }

    /**
     * Compound-target-target.
     */
    private void twoNode() {
        TypedNode comp1 = new TypedNode("1>>>Compound", "Protein", "test", 1);
        TypedNode targ2 = new TypedNode("2>>>Target", "Publication", "test", 2);
        //addNodes
        TypedEdge t = new TypedEdge(comp1, targ2, "published_in");

        //add all the nodes
        Sub.addVertex(comp1);
        Sub.addVertex(targ2);

        //add all the edges
        Sub.addEdge(comp1, targ2, t);


        // TypedEdge inferred = new TypedEdge(comp1, targ3, "binds_to");
        //setInferredEdge(inferred);
        setSubgraphName("2Node");

    }

    /**
     * Compound-target-target.
     */
    private void CTT() {
        TypedNode comp1 = new TypedNode("1>>>Compound", "Compound", "test", 1);
        TypedNode targ2 = new TypedNode("2>>>Target", "Target", "test", 2);
        TypedNode targ3 = new TypedNode("3>>>Target", "Target", "test", 3);
        //addNodes
        TypedEdge t = new TypedEdge(comp1, targ2, "binds_to");
        TypedEdge t2 = new TypedEdge(targ2, targ3, "has_similar_sequence");
        TypedEdge t3 = new TypedEdge(targ3, targ2, "has_similar_sequence");
        //add all the nodes
        Sub.addVertex(comp1);
        Sub.addVertex(targ2);
        Sub.addVertex(targ3);
        //add all the edges
        Sub.addEdge(comp1, targ2, t);
        Sub.addEdge(targ2, targ3, t2);
        Sub.addEdge(targ3, targ2, t3);

        TypedEdge inferred = new TypedEdge(comp1, targ3, "binds_to");
        setInferredEdge(inferred);
        setSubgraphName("CTT");

    }

    /**
     * Compound-target-target.
     */
    private void SixNodesLinear() {
        TypedNode comp1 = new TypedNode("1>>>Compound", "Compound", "test", 1);
        TypedNode targ2 = new TypedNode("2>>>Target", "Target", "test", 2);
        TypedNode targ3 = new TypedNode("3>>>Protein", "Protein", "test", 3);
        TypedNode ind5 = new TypedNode("5>>>Protein", "Protein", "test", 5);
        TypedNode ind6 = new TypedNode("6>>>Target", "Target", "test", 6);
        TypedNode comp2 = new TypedNode("6>>>Compound", "Compound", "test", 4);
        // TypedNode ind7 = new TypedNode("7>>>Target", "Target", "test", 7);

        //addNodes
        TypedEdge t = new TypedEdge(comp1, targ2, "binds_to");

        TypedEdge t2 = new TypedEdge(targ2, targ3, "has_similar_sequence");
        TypedEdge t3 = new TypedEdge(targ3, targ2, "has_similar_sequence");

        TypedEdge t4 = new TypedEdge(ind5, targ3, "has_similar_sequence");
        TypedEdge t5 = new TypedEdge(targ3, ind5, "has_similar_sequence");

        TypedEdge t6 = new TypedEdge(ind5, ind6, "has_similar_sequence");
        TypedEdge t7 = new TypedEdge(ind6, ind5, "has_similar_sequence");

        TypedEdge t8 = new TypedEdge(comp2, ind6, "binds_to");
        //TypedEdge t9 = new TypedEdge(comp2, ind7, "binds_to");

        //add all the nodes
        Sub.addVertex(comp1);
        Sub.addVertex(comp2);
        Sub.addVertex(targ2);
        Sub.addVertex(targ3);
        Sub.addVertex(ind6);
        Sub.addVertex(ind5);
        // Sub.addVertex(ind7);

        //add all the edges
        Sub.addEdge(comp1, targ2, t);
        Sub.addEdge(targ2, targ3, t2);
        Sub.addEdge(targ3, targ2, t3);
        Sub.addEdge(ind5, targ3, t4);
        Sub.addEdge(targ3, ind5, t5);
        Sub.addEdge(ind5, ind6, t6);
        Sub.addEdge(ind6, ind5, t7);
        Sub.addEdge(comp2, ind6, t8);
        // Sub.addEdge(comp2, ind7, t9);


        //  TypedEdge inferred = new TypedEdge(comp1, targ4, "binds_to");
        // setInferredEdge(inferred);
        setSubgraphName("sixNode");

    }

    /**
     * Compound-target-target.
     */
    private void SixNodes() {
        TypedNode comp1 = new TypedNode("1>>>Compound", "Compound", "test", 1);
        TypedNode targ2 = new TypedNode("2>>>Target", "Target", "test", 2);
        TypedNode targ3 = new TypedNode("3>>>Target", "Target", "test", 3);
        TypedNode ind5 = new TypedNode("5>>>Protein", "Protein", "test", 5);
        TypedNode ind6 = new TypedNode("6>>>Protein", "Protein", "test", 6);
        //TypedNode ind6 = new TypedNode("6>>>Indications", "Indications", "test", 6);

        //addNodes
        TypedEdge t = new TypedEdge(comp1, targ2, "binds_to");
        TypedEdge t2 = new TypedEdge(targ2, targ3, "has_similar_sequence");
        TypedEdge t3 = new TypedEdge(targ3, targ2, "has_similar_sequence");

        TypedEdge t4 = new TypedEdge(ind5, targ3, "has_similar_sequence");
        TypedEdge t5 = new TypedEdge(targ3, ind5, "has_similar_sequence");

        TypedEdge t6 = new TypedEdge(targ2, ind6, "has_similar_sequence");
        TypedEdge t7 = new TypedEdge(ind6, targ2, "has_similar_sequence");


        //add all the nodes
        Sub.addVertex(comp1);
        Sub.addVertex(targ2);
        Sub.addVertex(targ3);
        Sub.addVertex(ind6);
        Sub.addVertex(ind5);

        //add all the edges
        Sub.addEdge(comp1, targ2, t);
        Sub.addEdge(targ2, targ3, t2);
        Sub.addEdge(targ3, targ2, t3);


        Sub.addEdge(ind5, targ3, t4);
        Sub.addEdge(targ3, ind5, t5);
        Sub.addEdge(targ2, ind6, t6);
        Sub.addEdge(ind6, targ2, t7);


        //  TypedEdge inferred = new TypedEdge(comp1, targ4, "binds_to");
        // setInferredEdge(inferred);
        setSubgraphName("sixNode");

    }

    @Override
    public String toString() {

        String nodes = "";
        String edges = "";
        String name = "[Name]" + getSubgraphName() + "\n";
        Set<TypedNode> typenodes = Sub.vertexSet();
        for (TypedNode n : typenodes) {
            nodes = nodes + "[Node]" + n.toString() + "\n";
        }
        Set<TypedEdge> typeedges = Sub.edgeSet();
        for (TypedEdge n : typeedges) {
            edges = edges + "[Edge]" + n.toString() + "\n";
        }

        String inferredRel = null;
        if (inferredEdge == null) {
            inferredRel = "[InferredRel] NONE";
        } else {
            inferredRel = "[InferredRel]" + inferredEdge.toString();
        }

        return name + nodes + edges + inferredRel + "\n";

    }

    public String getSubgraphName() {

        return MOTIFNAME;
    }

    public void setSubgraphName(String name) {

        this.MOTIFNAME = name;
    }

    public TypedEdge getInferredEdge() {
        return inferredEdge;
    }

    public void setInferredEdge(TypedEdge t) {
        this.inferredEdge = t;
    }

    public DirectedGraph<TypedNode, TypedEdge> getSub() {
        return Sub;

    }

    public void setSub(DirectedGraph<TypedNode, TypedEdge> sub) {
        this.Sub = sub;

    }

    public void draw(String location) {
        GS_Core_VisualisationNEW gs = new GS_Core_VisualisationNEW(Sub.vertexSet(), Sub.edgeSet(), getSubgraphName(), location, true);
        gs.visualise();

    }
}
