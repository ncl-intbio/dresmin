/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.SemSearch;

import java.util.ArrayList;
import java.util.List;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedEdge;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedEdgeUID;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedNode;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Class represents the data structure for which mappings are held in during a
 * search.
 *
 */
public class DataStructureSub {

    private UndirectedGraph<TypedNode, TypedEdge> structure;
    private TypedNode rootNode;

    public DataStructureSub(TypedNode root) {
        this.rootNode = root;
        this.structure = new SimpleGraph<>(TypedEdge.class);
        structure.addVertex(root);
    }

    public void addNode(TypedNode add, TypedNode[] core) {

        //add the node
        structure.addVertex(add);
        //then add the relation
        structure.addEdge(core[core.length-1], add, new TypedEdge(core[core.length-1], add,"w"));

    }

    /**
     * Populates the sub graph core
     *
     * @param nodeInQuestion
     * @param targetNode
     * @return
     */
    public TypedNode[] popSubCore(TypedNode targetNode) {

        DijkstraShortestPath tr2 = new DijkstraShortestPath(structure,
                rootNode, targetNode);
        Object[] why = tr2.getPathEdgeList().toArray();
        TypedNode[] nodes = null;
        TypedNode start = (TypedNode) tr2.getPath().getStartVertex();
       // System.out.println("Start "+ start.toString());
        TypedNode end = (TypedNode) tr2.getPath().getEndVertex();
       // System.out.println("End "+ end.toString());
        List<TypedEdge> edges = tr2.getPath().getEdgeList();
        nodes = sortPath(start, end, edges);
        return nodes;
    }

    /**
     * Returns the nodes from the shortest path in order.
     *
     * @param start
     * @param end
     * @param edges
     * @return
     */
    public TypedNode[] sortPath(TypedNode start, TypedNode end, List<TypedEdge> edges) {

        TypedNode[] nodes = new TypedNode[edges.size() + 1];
        nodes[0] = start;
        nodes[edges.size()] = end;

        //sort the nodes
        List<TypedEdge> duplicate = new ArrayList<TypedEdge>();
        duplicate.addAll(edges);

        int count = 1;
        while (count < edges.size()) {
            TypedEdge delete = null;
            TypedNode t = nodes[count - 1];
            for (TypedEdge e : duplicate) {
                TypedNode from = e.getV1();
                TypedNode to = e.getV2();
                if (from == t) {
                    nodes[count] = e.getV2();
                    delete = e;
                    break;
                } else if (to == t) {
                    nodes[count] = e.getV1();
                    delete = e;
                    break;
                }
            }
            duplicate.remove(delete);
            count++;
        }
        return nodes;
    }
}
