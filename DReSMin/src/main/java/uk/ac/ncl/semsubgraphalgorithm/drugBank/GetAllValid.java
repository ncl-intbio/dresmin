/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.drugBank;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.semsubgraphalgorithm.DrugTargetWork.Pair;
import uk.ac.ncl.semsubgraphalgorithm.drugBank.DrugBankDataManager.DrugBankVersion;
import uk.ac.ncl.semsubgraphalgorithm.management.OutPutManagement;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Class takes a set of pairs and writes to file those hat are valid (i.e those
 * that contain a compound that is in the dataset as well as a target that is in
 * the dataset)
 *
 */
public class GetAllValid {

    private final static Logger LOGGER = Logger.getLogger(GetAllValid.class.getName());
    private Set<Pair> associations;
    private String outPutFile;
    private String inPutFile;
    private Set<Pair> validAssociations;
    private DrugBankDataManager dbm;

    public static void main(String[] args) {

        OutPutManagement op = new OutPutManagement();
        GetAllValid extract = new GetAllValid(op.getDrugBank3Path()+"uniqueDB3_V_dataset.txt",op.getDrugBank4Path() + "validDB3_V_dataset.txt");
        extract.run();
    }

    private GetAllValid( String inputFile,String outputFile) {
        this.outPutFile = outputFile;
        this.inPutFile = inputFile;
        this.validAssociations = new HashSet<>();
    }

    public void run() {
        getAssociations();
        getValid();
        writeToFile();
    }

    public void getAssociations() {

        this.dbm = new DrugBankDataManager(DrugBankVersion.THREE);
        try {
            dbm.parseRelsFromFile(inPutFile);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ExtractIntersection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ExtractIntersection.class.getName()).log(Level.SEVERE, null, ex);
        }
        associations = dbm.getDrugBankPairs();

    }

    public void getValid() {
        try {
            dbm.getDatassetInfo();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GetAllValid.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GetAllValid.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (Pair p : associations) {
            if (dbm.checkValidDrug(p.getDrug()) && dbm.checkValidTarget(p.getTarget())) {
                validAssociations.add(p);
            }
        }
    }

    public void writeToFile() {
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(new File(outPutFile)));
        } catch (IOException ex) {
            Logger.getLogger(ExtractIntersection.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (Pair p : validAssociations) {
            try {
                bw.append(p.toString() + '\n');
            } catch (IOException ex) {
                Logger.getLogger(ExtractIntersection.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try {
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(ExtractIntersection.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.INFO, "Written {0} valid associations to {1}", new Object[]{validAssociations.size(), outPutFile});

    }
}
