/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.DrugTargetWork;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.semsubgraphalgorithm.drugBank.ExtractDrugTargetAssociations;
import uk.ac.ncl.semsubgraphalgorithm.management.SerialisedGraph;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.SourceGraphNEW;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class Main {

    public static void main(String[] args) throws IOException {


        /**
         * STEP 1- get the shortest paths between the drug and target
         * interactions not captured in the dataset.
         */
        SerialisedGraph sg = new SerialisedGraph();
        SourceGraphNEW source = null;
        try {
            source = sg.useSerializedNEW("graphNEW.data");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
//
//        GetShortestPathNEW shortest = new GetShortestPathNEW(source);
//        shortest.Run();

        /**
         * STEP 2- run a search for each of the subgraphs.
         */
//        SearchShortestPaths search = new SearchShortestPaths(source);
//        search.run();
//
//        /**
//         * STEP 3- extract the drug- target associations in the form
//         * [DrugBank+"\t"+ UniProt]
//         */
//        
//        ExtractDrugTargetAssociations getAssociations = new ExtractDrugTargetAssociations();
        
        /**
         * STEP 4- score and rank the inferred drug-target associations.
         */
    }
}
