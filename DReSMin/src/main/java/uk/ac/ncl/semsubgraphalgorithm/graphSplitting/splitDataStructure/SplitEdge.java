/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.graphSplitting.splitDataStructure;

import java.io.Serializable;
import org.jgrapht.graph.DefaultEdge;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class SplitEdge<V> extends DefaultEdge implements Serializable{

    private SplitNode v1;
    private SplitNode v2;


    public SplitEdge(SplitNode v1, SplitNode v2) {
        this.v1 = v1;
        this.v2 = v2;

    }

    public SplitNode getV1() {
        return v1;
    }

    public SplitNode getV2() {
        return v2;
    }

}
