/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.drugBank;

import uk.ac.ncl.semsubgraphalgorithm.drugBankWork.version4.Parser;
import uk.ac.ncl.semsubgraphalgorithm.management.OutPutManagement;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class Main {

    public static void main(String[] args) {
        OutPutManagement op = new OutPutManagement();
        //1. Parse
        String db4 = "/Volumes/MyPassport/Datasets/DrugBank/drugbank4pt2.xml";
        String outPut = "ALLdb4pairs.txt";
        Parser p = new Parser(db4, outPut);
        p.parse();
        //2. Identify those that aren't in the dataset
        ExtractIntersection extract = new ExtractIntersection(DrugBankDataManager.DrugBankVersion.DATASET, DrugBankDataManager.DrugBankVersion.FOUR, op.getDrugBank4Path() + "uniqueDB4_V_dataset.txt");
        extract.run();
        //3. Identify of those how many are 'valid'
        


    }
}
