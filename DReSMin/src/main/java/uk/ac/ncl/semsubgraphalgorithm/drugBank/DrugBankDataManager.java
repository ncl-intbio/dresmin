/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.drugBank;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.semsubgraphalgorithm.DrugTargetWork.Pair;
import uk.ac.ncl.semsubgraphalgorithm.management.OutPutManagement;

/**
 *
 * @author joemullen
 */
public class DrugBankDataManager {

    private final static Logger LOGGER = Logger.getLogger(DrugBankDataManager.class.getName());
    private Set<String> allCompoundsDATASET;
    private Set<String> allProteinsDATASET;
    private Set<String> allCompoundsDB;
    private Set<String> allProteinsUni;
    private Set<String> allValidUNIQUEPairsDB;
    private Set<String> allUniquePairsDB;
    private Map<String, String> DrugBank2NAMEsDATASET;
    private Map<String, String> Uniprot2NAMEsDATASET;
    private Map<String, String> ID2DrugBankDATASET;
    private Map<String, String> ID2UniprotDATASET;
    private OutPutManagement op;
    private Set<Pair> drugBankPairs;
    private Set<Pair> datasetPairs;
    private DrugBankVersion dbversion;

    public DrugBankDataManager() {
        this.allCompoundsDATASET = new HashSet<>();
        this.allProteinsDATASET = new HashSet<>();
        this.allCompoundsDB = new HashSet<>();
        this.allProteinsUni = new HashSet<>();
        this.allValidUNIQUEPairsDB = new HashSet<>();
        this.allUniquePairsDB = new HashSet<>();
        this.DrugBank2NAMEsDATASET = new HashMap<>();
        this.Uniprot2NAMEsDATASET = new HashMap<>();
        this.ID2DrugBankDATASET = new HashMap<>();
        this.ID2UniprotDATASET = new HashMap<>();
        this.op = new OutPutManagement();
        this.drugBankPairs = new HashSet<>();
        this.datasetPairs = new HashSet<>();

    }

    public DrugBankDataManager(DrugBankVersion dbversion) {
        this.dbversion = dbversion;
        this.allCompoundsDATASET = new HashSet<>();
        this.allProteinsDATASET = new HashSet<>();
        this.allCompoundsDB = new HashSet<>();
        this.allProteinsUni = new HashSet<>();
        this.allValidUNIQUEPairsDB = new HashSet<>();
        this.allUniquePairsDB = new HashSet<>();
        this.DrugBank2NAMEsDATASET = new HashMap<>();
        this.Uniprot2NAMEsDATASET = new HashMap<>();
        this.ID2DrugBankDATASET = new HashMap<>();
        this.ID2UniprotDATASET = new HashMap<>();
        this.op = new OutPutManagement();
        this.drugBankPairs = new HashSet<>();
        this.datasetPairs = new HashSet<>();

    }

    public enum DrugBankVersion {

        DATASET, THREE, FOUR;
    }

    public Set<Pair> getDBPairsACCESSION(String fileLocationNodeData, String fileLocationValidNonMatchesDB3) {

        try {
            getDatassetInfoNoREMOVAL(fileLocationNodeData);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DrugBankDataManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DrugBankDataManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        BufferedReader validpairs = null;
        try {
            validpairs = new BufferedReader(new FileReader(fileLocationValidNonMatchesDB3));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DrugBankDataManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        String line2;
        String dbID;
        String uniprotID;
        try {
            while ((line2 = validpairs.readLine()) != null) {
                dbID = line2.split("\t")[0];
                uniprotID = line2.split("\t")[1];
                drugBankPairs.add(new Pair(dbID, uniprotID));
            }
        } catch (IOException ex) {
            Logger.getLogger(DrugBankDataManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return drugBankPairs;
    }

    public Set<Pair> getDBPairs(String fileLocationNodeData, String fileLocationValidNonMatchesDB3) {

        try {
            getDatassetInfoNoREMOVAL(fileLocationNodeData);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DrugBankDataManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DrugBankDataManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        //String DBfile = op.getDrugBank3Path();
        BufferedReader validpairs = null;
        try {
            //validpairs = new BufferedReader(new FileReader(DBfile + "VALIDnomatchesdb3.txt"));
            validpairs = new BufferedReader(new FileReader(fileLocationValidNonMatchesDB3));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DrugBankDataManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        String line2;
        String dbID;
        String uniprotID;
        try {
            while ((line2 = validpairs.readLine()) != null) {
                dbID = line2.split("\t")[0];
                uniprotID = line2.split("\t")[1];
                drugBankPairs.add(new Pair(getDrugNameFromDBID(dbID), getProteinNameFromUniID(uniprotID)));
            }
        } catch (IOException ex) {
            Logger.getLogger(DrugBankDataManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return drugBankPairs;
    }

    public void getDatassetInfo() throws FileNotFoundException, IOException {
        String DBfile = op.getGraphFolder();
        BufferedReader dbs = new BufferedReader(new FileReader(DBfile + "con_listOP_55e0c.tsv"));
        String line;
        dbs.readLine();
        while ((line = dbs.readLine()) != null) {
            String[] split = line.split("\t");

            if (!split[4].equals("NO_DRUGBANK")) {
                allCompoundsDATASET.add(split[4]);

            }
            if (!split[5].equals("NO_UNIPROT")) {

                allProteinsDATASET.add(split[5]);
            }
            if (split.length > 1) {
                String GRAPHID = split[0];
                String NAME = split[3];
                String DBID = split[4];
                String UNIID = split[5];

                if (!DBID.equals("NO_DRUGBANK")) {
                    String fromName = NAME.replaceAll("[^A-Za-z0-9]", "").trim().toLowerCase();
                    DrugBank2NAMEsDATASET.put(fromName, DBID);
                    ID2DrugBankDATASET.put(GRAPHID, DBID);

                }
                if (!UNIID.equals("NO_UNIPROT")) {
                    String toName = NAME.replaceAll("[^A-Za-z0-9]", "").trim().toLowerCase();
                    Uniprot2NAMEsDATASET.put(toName, UNIID);
                    ID2UniprotDATASET.put(GRAPHID, UNIID);
                }
            }
        }
    }

    public void getDatassetInfoNoREMOVAL(String fileLocationNodeData) throws FileNotFoundException, IOException {
        //String DBfile = op.getGraphFolder();
        //BufferedReader dbs = new BufferedReader(new FileReader(DBfile + "con_listOP_55e0c.tsv"));
        BufferedReader dbs = new BufferedReader(new FileReader(fileLocationNodeData));
        String line;
        dbs.readLine();
        while ((line = dbs.readLine()) != null) {
            String[] split = line.split("\t");

            if (!split[4].equals("NO_DRUGBANK")) {
                allCompoundsDATASET.add(split[4]);

            }
            if (!split[5].equals("NO_UNIPROT")) {

                allProteinsDATASET.add(split[5]);
            }
            if (split.length > 1) {
                String NAME = split[3];
                String DBID = split[4];
                String UNIID = split[5];

                if (!DBID.equals("NO_DRUGBANK")) {
                    DrugBank2NAMEsDATASET.put(NAME, DBID);
                }
                if (!UNIID.equals("NO_UNIPROT")) {
                    Uniprot2NAMEsDATASET.put(NAME, UNIID);
                }
            }
        }
    }

    public void parseDatasetRels() {
        String DBfile = null;
        BufferedReader dbs = null;

        DBfile = op.getDatasetDrugTargetFolder();
        try {
            dbs = new BufferedReader(new FileReader(DBfile + "ALLdatasetpairs.txt"));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DrugBankDataManager.class.getName()).log(Level.SEVERE, null, ex);
        }

        String line2;
        try {
            while ((line2 = dbs.readLine()) != null) {
                String[] split = line2.split("\t");
                String DBID = split[0];
                String unip = split[1];
                datasetPairs.add(new Pair(DBID, unip));
            }
        } catch (IOException ex) {
            Logger.getLogger(DrugBankDataManager.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.INFO, "Extracted {0} assocations from dataset from {1}ALLdatasetpairs.txt", new Object[]{datasetPairs.size(), DBfile});

    }

    public void parseRelsFromFile(String file) throws FileNotFoundException, IOException {

        BufferedReader dbs = null;
        dbs = new BufferedReader(new FileReader(file));
        LOGGER.log(Level.INFO, "Extracted assocations from {0}", file);
        
        String line2;
        while ((line2 = dbs.readLine()) != null) {
            String[] split = line2.split("\t");
            String DBID = split[0];
            String unip = split[1];
            allCompoundsDB.add(DBID);
            allProteinsUni.add(unip);
            drugBankPairs.add(new Pair(DBID, unip));
        }

        LOGGER.log(Level.INFO, "Added {0} pairs", drugBankPairs.size());
        LOGGER.log(Level.INFO, "With {0} uniqueDrugs", allCompoundsDB.size());
        LOGGER.log(Level.INFO, "With {0} uniqueTargets", allProteinsUni.size());
    }

    public void parseDrugBankRels() throws FileNotFoundException, IOException {
        String DBfile = null;
        BufferedReader dbs = null;
        if (dbversion == DrugBankVersion.THREE) {
            DBfile = op.getDrugBank3Path();
            dbs = new BufferedReader(new FileReader(DBfile + "ALLdb3pairs.txt"));
            LOGGER.log(Level.INFO, "Extracted assocations from Drugbank version 3 from {0}", DBfile + "ALLdb3pairs.txt");
        } else {
            DBfile = op.getDrugBank4Path();
            dbs = new BufferedReader(new FileReader(DBfile + "ALLdb4pairs.txt"));
            LOGGER.log(Level.INFO, "Extracted assocations from Drugbank version 4 from {0}", DBfile + "ALLdb4pairs.txt");
        }


        String line2;
        while ((line2 = dbs.readLine()) != null) {
            String[] split = line2.split("\t");
            String DBID = split[0];
            String unip = split[1];
            allCompoundsDB.add(DBID);
            allProteinsUni.add(unip);
            drugBankPairs.add(new Pair(DBID, unip));
        }

        LOGGER.log(Level.INFO, "Added {0} pairs", drugBankPairs.size());
        LOGGER.log(Level.INFO, "With {0} uniqueDrugs", allCompoundsDB.size());
        LOGGER.log(Level.INFO, "With {0} uniqueTargets", allProteinsUni.size());
    }

    public void getValidPairsInfo() throws FileNotFoundException, IOException {
        String DBfile = op.getDrugBank3Path();
        BufferedReader validpairs = new BufferedReader(new FileReader(DBfile + "VALIDnomatchesdb3.txt"));
        String line2;
        while ((line2 = validpairs.readLine()) != null) {
            allValidUNIQUEPairsDB.add(line2);
        }
    }

    public Map<String, String> getDrugBank2NAMEsDATASET() {
        return DrugBank2NAMEsDATASET;

    }

    public Map<String, String> getUniprot2NAMEsDATASET() {
        return Uniprot2NAMEsDATASET;

    }

    public Set<String> getallCompoundsDATASET() {

        return allCompoundsDATASET;
    }

    public Set<String> getallProtiensDATASET() {

        return allProteinsDATASET;
    }

    public Set<String> getallCompoundsDB() {

        return allCompoundsDB;
    }

    public Set<String> getallProteinsDB() {

        return allProteinsUni;
    }

    public Set<String> getallValidUNIQUEPairsDB() {

        return allValidUNIQUEPairsDB;
    }

    public Set<String> getallUniquePairsDB() {

        return allUniquePairsDB;
    }

    public String getProteinNameFromUniID(String UniID) {
        for (String ke : Uniprot2NAMEsDATASET.keySet()) {
            if (Uniprot2NAMEsDATASET.get(ke).equals(UniID)) {
                return ke;
            }
        }
        return "nahh";
    }

    public Set<Pair> getDatasetPairs() {
        return datasetPairs;
    }

    public Set<Pair> getDrugBankPairs() {
        return drugBankPairs;
    }

    public String getUNIIDfromDatasetID(String datasetID) {
        if (ID2UniprotDATASET.containsKey(datasetID)) {
            return ID2UniprotDATASET.get(datasetID);
        }
        return "nahh";
    }

    public String getDBIDfromDatasetID(String datasetID) {
        if (ID2DrugBankDATASET.containsKey(datasetID)) {
            return ID2DrugBankDATASET.get(datasetID);
        }
        return "nahh";
    }

    public String getDrugNameFromDBID(String DBID) {
        for (String ke : DrugBank2NAMEsDATASET.keySet()) {
            if (DrugBank2NAMEsDATASET.get(ke).equals(DBID)) {
                return ke;
            }
        }
        return "nahh";
    }

    public boolean checkValidDrug(String DBID) {
        return allCompoundsDATASET.contains(DBID);
    }

    public boolean checkValidTarget(String UniID) {
        return allProteinsDATASET.contains(UniID);
    }
}
