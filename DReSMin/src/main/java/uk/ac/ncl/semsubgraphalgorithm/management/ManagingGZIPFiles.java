/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.management;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Class takes a gzip file and returns a buffered stream
 *
 */
public class ManagingGZIPFiles {

    //keep a list of processes so they can be desroyed
    private static List<Process> processes;
    private String file;
    private int chunkSize;
    private int previous;
    private int chunk;
    private String scriptLoc;

    public ManagingGZIPFiles(String file, int chunksize, String script) {
        this.file = file;
        this.processes = new ArrayList<>();
        this.chunkSize = chunksize;
        this.previous = chunksize;
        this.chunk = 1;
        this.scriptLoc = script;

    }

    /**
     * Check that there is still lines to be streamed.
     *
     * @return
     */
    public boolean checkStreamLive() {
        if (previous < chunkSize) {
            destroyProcesses();
            return false;
        } else if (previous == chunkSize) {
            return true;
        }
        return false;
    }

    public void setPreviousLines(int previous) {
        this.previous = previous;

    }

    public static void main(String[] args) {
        String script ="Scripts/streamGZ.py";
        String file = "/Volumes/MyPassport/Results192_STRIPPED/[c4623a17_CTITCTCTCT]_ST0.8_14290500950.txt.gz";
        int chunksize = 50000000;
        ManagingGZIPFiles gzip = new ManagingGZIPFiles(file, chunksize, script);
        BufferedReader br = null;
        boolean openStream = true;
        int lineCount = 0;
        while (openStream) {
            br = gzip.getStream();
            String s = null;
            try {
                while ((s = br.readLine()) != null) {
                    lineCount++;
                }
            } catch (IOException ex) {
                Logger.getLogger(ManagingGZIPFiles.class.getName()).log(Level.SEVERE, null, ex);
            }
            gzip.setPreviousLines(lineCount);
            openStream = gzip.checkStreamLive();
            System.out.println(lineCount);
            lineCount = 0;
        }
        destroyProcesses();
    }

    /**
     * Gets the next chunk of the file using the python script.
     *
     * @return
     */
    public BufferedReader getStream() {
        Process p = null;
        String s = null;
        StringBuilder command = new StringBuilder();
        command.append("python").append("  ").append(scriptLoc).append(" ").append(file).append(" ").append(chunkSize).append(" ").append(chunk);
        try {
            // System.out.println(command.toString());
            p = Runtime.getRuntime().exec(command.toString());
            processes.add(p);
        } catch (IOException ex) {
            Logger.getLogger(ManagingGZIPFiles.class.getName()).log(Level.SEVERE, null, ex);
        }

        BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
        chunk++;
        return stdInput;
    }

    /**
     * Destroys all python processes that have been called from the class.
     */
    public static void destroyProcesses() {
        for (Process p : processes) {
            p.destroy();
        }
    }
}
