/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.visualisation;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import uk.ac.ncl.semsubgraphalgorithm.management.OutPutManagement;
import uk.ac.ncl.semsubgraphalgorithm.management.SerialisedGraph;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.SourceGraphNEW;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedEdge;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedNode;

/**
 *
 * @author joemullen
 */
public class ExtractSubgraph {

    private TypedNode[] initialNodes;
    private SourceGraphNEW source;
    private String name;
    private Set<TypedNode> subNodes;
    private Set<TypedEdge> subEdges;
    private boolean neighbours;
    private boolean onlyToFile;
    private String dir;
    private int depth;

    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException {

        //DRUGS
        //1. Verapamil
        //2. Mibefradil        
        //3. Bepridil 
        //4. Promazine
        //5. Perphenazine
        //6. Thioridazine
        //7. Propiomazine
        //8. Clozapine
        //9. Propranolol
        //10. Zonisamide
        //11. Levetiracetam
        //12. Quetiapine
        
        
        
        //TARGETS
        //1. 5 Hydroxytryptamine 7 Receptor 
        //2. Alpha 1A Adrenergic Receptor
        //3. 5 Hydroxytryptamine 2B Receptor
        //4. D 1A Dopamine Receptor     
        //5. Voltage Dependent P Q Type Calcium Channel Subunit Alpha 1A
        //6. Voltage Dependent N Type Calcium Channel Subunit Alpha 1B
        //7. Calcium Channel Voltage Dependent L Type Alpha 1B Subunit
        //8. Voltage Dependent L Type Calcium Channel Subunit Alpha 1C

        SerialisedGraph sg = new SerialisedGraph();
        SourceGraphNEW source = null;
        OutPutManagement op = new OutPutManagement();
        source = sg.useSerializedNEW(op.getGraphFolder() + "graphNEW.data");
        TypedNode drug = source.getNode("NAME", "Quetiapine");
        //TypedNode target = source.getNode("ID", "32576");
        TypedNode[] search = new TypedNode[1];
        search[0] = drug;
        //search[1] = target;
        //source =sg.useSerializedNEW(op.getGraphFolder()+"graphNEW.data");

        //SerialisedGraph ser = new SerialisedGraph();
        ExtractSubgraph ex = new ExtractSubgraph(search, source, "/Users/joemullen/Dropbox/LatexPapers/PlosCompAlgorithm/plos_template_SUPP/Graphics/Drugs/", "Quetiapine", true, false, 1);
        ex.draw();

    }

    public ExtractSubgraph(TypedNode[] nodes, SourceGraphNEW source, String dir, String name, boolean neighbours, boolean onlyToFile, int depth) {
        this.initialNodes = nodes;
        this.subEdges = new HashSet<TypedEdge>();
        this.subNodes = new HashSet<TypedNode>();
        this.source = source;
        this.name = name;
        this.dir = dir;
        this.depth = depth;
        this.subNodes = new HashSet<>();
        this.neighbours = neighbours;
        this.onlyToFile = onlyToFile;
        if (neighbours == true) {
            getNearestNeighbours_NODES();
        } else {
            getOnlySubgraph();
        }
        draw();
    }

    public void draw() {

        GS_Core_VisualisationNEW gs = new GS_Core_VisualisationNEW(subNodes, subEdges, name, dir, onlyToFile);
        gs.visualise();

    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public void getNearestNeighbours_NODES() {
        System.out.println("DEPTH  " + depth);
        for (int i = 0; i < depth; i++) {
            getNearestNeighbours(i);
        }

    }

    public void getNearestNeighbours(int depth) {
        System.out.println("calling get neighbours.... " + depth);
        if (depth < 1) {
            for (TypedNode node : initialNodes) {
                subNodes.add(node);
                //get all the nodes it has edges too
                Set<TypedEdge> edges = source.getSourceGraph().edgesOf(node);
                for (TypedEdge edg : edges) {
                    TypedNode from = source.getSourceGraph().getEdgeSource(edg);
                    TypedNode to = source.getSourceGraph().getEdgeTarget(edg);
                    String fromName = from.getName();
                    String toName = to.getName();
                    String ccto = to.getType();
                    String ccfrom = from.getType();
                    String relType = edg.getType();
                    if (ccto.equals("Indications")) {
                        subNodes.add(to);
                        subNodes.add(from);
                        subEdges.add(edg);
                    }
                }
            }
        } else {
            Set<TypedNode> dupe = new HashSet<TypedNode>();
            dupe.addAll(subNodes);
            for (TypedNode node : dupe) {
                //get all the nodes it has edges too
                Set<TypedEdge> edges = source.getSourceGraph().edgesOf(node);
                for (TypedEdge edg : edges) {
                    TypedNode from = source.getSourceGraph().getEdgeSource(edg);
                    TypedNode to = source.getSourceGraph().getEdgeTarget(edg);
                    String fromName = from.getName();
                    String toName = to.getName();
                    String ccto = to.getType();
                    String ccfrom = from.getType();
                    String relType = edg.getType();
                    if (!subNodes.contains(to)) {
                        subNodes.add(to);
                    }
                    if (!subNodes.contains(from)) {
                        subNodes.add(from);
                    }
                    if (!subEdges.contains(edg)) {
                        subEdges.add(edg);
                    }
                }
            }
        }

    }

    public void getOnlySubgraph() {

        for (TypedNode node : initialNodes) {
            subNodes.add(node);
            //get all the nodes it has edges too
            Set<TypedEdge> edges = source.getSourceGraph().edgesOf(node);
            for (TypedEdge edg : edges) {
                TypedNode from = source.getSourceGraph().getEdgeSource(edg);
                TypedNode to = source.getSourceGraph().getEdgeTarget(edg);

                boolean fromCHECK = false;
                boolean toCHECK = false;
                for (int y = 0; y < initialNodes.length; y++) {
                    if (initialNodes[y] == from) {
                        fromCHECK = true;
                    }
                    if (initialNodes[y] == to) {
                        toCHECK = true;
                    }
                }
                if ((fromCHECK) && (toCHECK)) {
                    String fromName = from.getName();
                    String toName = to.getName();
                    String ccto = to.getType();
                    String ccfrom = from.getType();
                    String relType = edg.getType();
                    subNodes.add(to);
                    subNodes.add(from);
                    subEdges.add(edg);
                }

            }

        }
    }
}
