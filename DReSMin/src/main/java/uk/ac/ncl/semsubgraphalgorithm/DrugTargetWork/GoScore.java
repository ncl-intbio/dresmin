/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.DrugTargetWork;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.semsubgraphalgorithm.DrugTargetWork.scoreAssociations.GoScoreAssociations;
import uk.ac.ncl.semsubgraphalgorithm.DrugTargetWork.scoreAssociations.GoScoreSub;
import uk.ac.ncl.semsubgraphalgorithm.drugBank.DrugBankDataManager;
import uk.ac.ncl.semsubgraphalgorithm.drugBank.DrugBankDataManager.DrugBankVersion;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class GoScore {

    private final static Logger LOGGER = Logger.getLogger(GoScore.class.getName());
    private int chunk;

    public static void main(String[] args) {
//
        LOGGER.log(Level.INFO, "Scoring subs from directory {0}", args[0]);
        LOGGER.log(Level.INFO, "Getting drugbank and uniprot data for nodes from {0}", args[1]);
        LOGGER.log(Level.INFO, "Getting valid DB3 pairs from {0}", args[2]);
        LOGGER.log(Level.INFO, "Sub scores file {0}", args[3]);


//        //String Dir = "/Volumes/MyPassport/Results192_STRIPPED";
//        String Dir = "/Users/joemullen/Desktop/testResults";
//        String graphData = "/Users/joemullen/Desktop/SemSubNEW/DReSMin/Graph/con_listOP_55e0c.tsv";
//        String validMatches = "/Users/joemullen/Desktop/SemSubNEW/DReSMin/Results/DB3WORK/uniqueDB3_V_dataset.txt";
//        String scriptLoc = "Scripts/streamGZ.py";
//        boolean instance = false;
//        String name = "DB3_FALSE";
//        int chunkSize = 1300000000;

        //java -jar SemSubgraphAlgorithm-PROG-jar-with-dependencies.jar /Users/joemullen/Desktop/testResults /Users/joemullen/Desktop/SemSubNEW/DReSMin/Graph/con_listOP_55e0c.tsv /Users/joemullen/Desktop/SemSubNEW/DReSMin/Results/DB3WORK/uniqueDB3_V_dataset.txt false DB3_FALSE

        DrugBankDataManager db = new DrugBankDataManager(DrugBankVersion.THREE);
        try {
            db.getDatassetInfoNoREMOVAL(args[1]);
            //db.getDatassetInfoNoREMOVAL(graphData);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GoScore.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GoScore.class.getName()).log(Level.SEVERE, null, ex);
        }

        //goScoreSubs(Dir, db.getDBPairs(graphData, validMatches), chunkSize, name);
        //goScoreAssociations(Dir, db.getDrugBank2NAMEsDATASET(), db.getUniprot2NAMEsDATASET(), chunkSize, instance, name);

      //  goScoreSubs(args[0], db.getDBPairs(args[1], args[2]),args[4]);
        goScoreAssociations(args[0], db.getDrugBank2NAMEsDATASET(), db.getUniprot2NAMEsDATASET(), args[3]);


    }

    public static void goScoreSubs(String directory, Set<Pair> known) {

        String opFile = "sub_scores.txt";

        LOGGER.log(Level.INFO, "Scoring all subs from {0}", directory);
        LOGGER.log(Level.INFO, "We have {0} known associations", known.size());

        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(new File(opFile)));
        } catch (IOException ex) {
            Logger.getLogger(GoScore.class.getName()).log(Level.SEVERE, null, ex);
        }

        File dir = new File(directory);
        File[] directoryListing = dir.listFiles();
        if (directoryListing != null) {
            for (File child : directoryListing) {
                if (!child.getName().startsWith(".")) {
                    LOGGER.log(Level.INFO, "Scoring subs in {0}", child);
                    GoScoreSub go = new GoScoreSub(known, child);
                    // if (child.getName().endsWith(".txt")) {
                    go.goGetInferredAssociationsTXT();
//                    } else if (child.getName().endsWith(".gz")) {
//                        go.goGetInferredAssociationsGZIP(chunk);
//                    }
                    go.goScore();
                    try {
                        bw.append(go.getSummary() + "\n");
                        LOGGER.info(go.getSummary());
                    } catch (IOException ex) {
                        Logger.getLogger(GoScore.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        } else {
            LOGGER.info("Errr nahhhh....");
        }
        try {
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(GoScore.class.getName()).log(Level.SEVERE, null, ex);
        }
        LOGGER.log(Level.INFO, "Finished scoring subs; output found in {0}", opFile);

    }

    public static void goScoreAssociations(String directory, Map<String, String> DrugBank2NAMEsDATASET, Map<String, String> Uniprot2NAMEsDATASET, String subscores) {
        GoScoreAssociations goScore = new GoScoreAssociations(getSubScores(subscores), directory, DrugBank2NAMEsDATASET, Uniprot2NAMEsDATASET);
        goScore.scoreAssociations();
        goScore.addToFile();
        LOGGER.log(Level.INFO, "Finished scoring associations....");
    }

    public static HashMap<String, Double> getSubScores(String file) {

        HashMap<String, Double> scores = new HashMap<>();
        BufferedReader br = null;

        try {
            br = new BufferedReader(new FileReader(new File(file)));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GoScore.class.getName()).log(Level.SEVERE, null, ex);
        }

        String thisLine;
        String subName;
        double subScore;
        try {
            while ((thisLine = br.readLine()) != null) {
                subName = thisLine.split("\t")[0];
                subScore = Double.parseDouble(thisLine.split("\t")[4]);
                scores.put(subName, subScore);
            }
        } catch (IOException ex) {
            Logger.getLogger(GoScore.class.getName()).log(Level.SEVERE, null, ex);
        }
        return scores;
    }
}
