/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.drugBankWork.version3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import uk.ac.ncl.semsubgraphalgorithm.drugBank.DrugBankDataManager;
import uk.ac.ncl.semsubgraphalgorithm.drugBank.DrugBankDataManager.DrugBankVersion;
import uk.ac.ncl.semsubgraphalgorithm.management.OutPutManagement;

/**
 *
 * @author joemullen
 */
public class GetValidDB3Pairs {

    private Set<String> proteins = new HashSet<String>();
    private Set<String> compounds = new HashSet<String>();
    private DrugBankDataManager pi;
    private OutPutManagement op;

    public GetValidDB3Pairs() throws FileNotFoundException, IOException {
        this.pi = new DrugBankDataManager(DrugBankVersion.THREE);
        pi.getDatassetInfo();
        pi.parseDrugBankRels();
        this.op = new OutPutManagement();


    }

    public static void main(String[] args) throws FileNotFoundException, IOException {

        GetValidDB3Pairs el = new GetValidDB3Pairs();
        el.getCommonCompounds();
        el.getCommonProteins();
        el.ExtractMissingElements();
    }

    public void getCommonCompounds() throws FileNotFoundException, IOException {

        int NOMATCH = 0;
        int MATCH = 0;

        Set<String> alldb2 = pi.getallCompoundsDATASET();

        Set<String> alldb3 = pi.getallCompoundsDB();

        for (String DBID : alldb3) {
            if (!alldb2.contains(DBID)) {
                //System.out.println("Not in both: " + DBID);
                NOMATCH++;
            } else {
                MATCH++;
                compounds.add(DBID);
            }


        }
        System.out.println("[INFO] ---------------------------Compounds");
        System.out.println("[INFO] In db2:  " + alldb2.size());
        System.out.println("[INFO] In db3:  " + alldb3.size());
        System.out.println("[INFO] Not in both total:  " + NOMATCH);
        System.out.println("[INFO] In both total:  " + MATCH);


    }

    public void getCommonProteins() throws FileNotFoundException, IOException {
        int NOMATCH = 0;
        int MATCH = 0;

        Set<String> alldb2 = pi.getallProtiensDATASET();
        Set<String> alldb3 = pi.getallProteinsDB();


        for (String DBID : alldb3) {
            if (!alldb2.contains(DBID)) {
                //System.out.println("Not in both: " + DBID);
                NOMATCH++;
            } else {
                MATCH++;
                proteins.add(DBID);
            }


        }
        System.out.println("[INFO]---------------------------Targets");
        System.out.println("[INFO] In db2:  " + alldb2.size());
        System.out.println("[INFO] In db3:  " + alldb3.size());
        System.out.println("[INFO] Not in both total:  " + NOMATCH);
        System.out.println("[INFO] In both total:  " + MATCH);

    }

    public void ExtractMissingElements() throws FileNotFoundException, IOException {

        int validPairs = 0;
        int invalidPairs = 0;
        String DBdirPath  = op.getDrugBank3Path();
        BufferedReader br = new BufferedReader(new FileReader(DBdirPath+"nomatchesdb3.txt"));
        BufferedWriter bw = new BufferedWriter(new FileWriter(DBdirPath+"VALIDnomatchesdb3.txt"));

        String line;
        while ((line = br.readLine()) != null) {
            String[] split = line.split("\t");
            if (compounds.contains(split[0]) && proteins.contains(split[1])) {
                validPairs++;
                  bw.append(line + "\n");
            } else {

                invalidPairs++;
            }

        }

        bw.close();

        System.out.println("[INFO]------------------Summary");
        System.out.println("[INFO] Valid pairs: " + validPairs);
        System.out.println("[INFO] Invalid pairs: " + invalidPairs);
    }
}
