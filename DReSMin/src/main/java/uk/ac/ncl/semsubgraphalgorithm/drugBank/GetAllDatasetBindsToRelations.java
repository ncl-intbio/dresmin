/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.drugBank;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.semsubgraphalgorithm.DrugTargetWork.Pair;
import uk.ac.ncl.semsubgraphalgorithm.DrugTargetWork.SearchShortestPaths;
import uk.ac.ncl.semsubgraphalgorithm.SemSearch.SearchManager;
import uk.ac.ncl.semsubgraphalgorithm.drugBank.DrugBankDataManager.DrugBankVersion;
import uk.ac.ncl.semsubgraphalgorithm.management.OutPutManagement;
import uk.ac.ncl.semsubgraphalgorithm.management.SerialisedGraph;
import uk.ac.ncl.semsubgraphalgorithm.queryGraphs.QueryGraph;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.SourceGraphNEW;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Class uses the semsub algorithm to extract all drug-target associations from
 * the network.
 *
 */
public class GetAllDatasetBindsToRelations {
    
    private final static Logger LOGGER = Logger.getLogger(GetAllDatasetBindsToRelations.class.getName());
    private OutPutManagement op;
    private String opFile;
    private DrugBankDataManager dbdata;
    private Set<String> uniqueDrugs;
    private Set<String> uniqueTargets;
    
    public static void main(String[] args) {
        
        String file = "ALLdatasetpairs.txt";
        GetAllDatasetBindsToRelations rels = new GetAllDatasetBindsToRelations(file);
        //rels.searchGraph();
        rels.getAccessions();
        
    }
    
    public GetAllDatasetBindsToRelations(String opFile) {
        this.op = new OutPutManagement();
        this.opFile = op.getDatasetDrugTargetFolder() + opFile;
        this.dbdata = new DrugBankDataManager(DrugBankVersion.THREE);
        this.uniqueDrugs = new HashSet<>();
        this.uniqueTargets = new HashSet<>();
        try {
            dbdata.getDatassetInfo();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GetAllDatasetBindsToRelations.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GetAllDatasetBindsToRelations.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    /**
     * Method takes the output from the search and creates a file containing
     * only the accessions [DBID "\t" UNIID].
     */
    public void getAccessions() {
        
        Set<Pair> datasetPairs = new HashSet<>();
        
        BufferedWriter bw = null;
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(new File(op.getDatasetDrugTargetFolder() + "[binds_To]_ST1.0.txt")));
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GetAllDatasetBindsToRelations.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            bw = new BufferedWriter(new FileWriter(new File(opFile)));
        } catch (IOException ex) {
            Logger.getLogger(GetAllDatasetBindsToRelations.class.getName()).log(Level.SEVERE, null, ex);
        }
          
        String line;
        try {
            while ((line = br.readLine()) != null) {
                if (line.startsWith("SR:")) {
                    line = line.substring(3);
                    String[] split = line.split(":");
                    String from = dbdata.getDBIDfromDatasetID(split[0]);
                    uniqueDrugs.add(from);
                    String to = dbdata.getUNIIDfromDatasetID(split[1].trim());
                    uniqueTargets.add(to);
                    datasetPairs.add(new Pair(from, to));         
                }                
                
            }
        } catch (IOException ex) {
            Logger.getLogger(GetAllDatasetBindsToRelations.class.getName()).log(Level.SEVERE, null, ex);
        }
              
        for(Pair p: datasetPairs){
            try {
                bw.append(p.toString()+"\n");
            } catch (IOException ex) {
                Logger.getLogger(GetAllDatasetBindsToRelations.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        }    
        
        try {
            br.close();
        } catch (IOException ex) {
            Logger.getLogger(GetAllDatasetBindsToRelations.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(GetAllDatasetBindsToRelations.class.getName()).log(Level.SEVERE, null, ex);
        }       
        LOGGER.log(Level.INFO, "Added {0} drug-target associations from the dataset to {1}", new Object[]{datasetPairs.size(), opFile});
        LOGGER.log(Level.INFO, "With {0} uniqueDrugs", uniqueDrugs.size());
        LOGGER.log(Level.INFO, "With {0} uniqueTargets", uniqueTargets.size());
        
    }

    /**
     * Method completes a search for all the associations
     */
    public void searchGraph() {
        
        QueryGraph q = new QueryGraph(QueryGraph.SemanticSub.BINDS_TO);
        
        try {
            SerialisedGraph sg = new SerialisedGraph();
            SourceGraphNEW source = null;
            try {
                //source = sg.useSerializedNEW(args[0]);
                source = sg.useSerializedNEW(op.getGraphFolder() + "graphNEW.data");
            } catch (FileNotFoundException ex) {
                Logger.getLogger(SearchShortestPaths.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(SearchShortestPaths.class.getName()).log(Level.SEVERE, null, ex);
            }
            //SearchShortestPaths search = new SearchShortestPaths(source, args[1], args[4], args[2], args[3], Integer.parseInt(args[5]), Double.parseDouble(args[6]));
            SearchManager search = new SearchManager(source, q, null, "Compound", true, 1.0, true, true, true, op.getDatasetDrugTargetFolder(), "SDC/node_distance.txt", "SDC/edge_distance.txt", 2);
            search.run();
        } catch (IOException ex) {
            Logger.getLogger(SearchShortestPaths.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
