/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.drugBankWork.version4;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.semsubgraphalgorithm.DrugTargetWork.GoScore;
import uk.ac.ncl.semsubgraphalgorithm.drugBank.DrugBankDataManager;
import uk.ac.ncl.semsubgraphalgorithm.drugBank.DrugBankDataManager.DrugBankVersion;
import uk.ac.ncl.semsubgraphalgorithm.management.OutPutManagement;

/**
 *
 * @author joemullen
 */
public class GetValidDB4Pairs {

    private final static Logger LOGGER = Logger.getLogger(GetValidDB4Pairs.class.getName());
    private Set<String> proteins = new HashSet<>();
    private Set<String> compounds = new HashSet<>();
    private DrugBankDataManager pi;
    private OutPutManagement op;

    public GetValidDB4Pairs() throws FileNotFoundException, IOException {
        this.pi = new DrugBankDataManager(DrugBankVersion.FOUR);
        pi.getDatassetInfo();
        pi.parseDrugBankRels();
        this.op = new OutPutManagement();

    }

    public static void main(String[] args) throws FileNotFoundException, IOException {

        GetValidDB4Pairs el = new GetValidDB4Pairs();
        el.getCommonCompounds();
        el.getCommonProteins();
        el.ExtractMissingElements();
    }

    public void getCommonCompounds() throws FileNotFoundException, IOException {

        int NOMATCH = 0;
        int MATCH = 0;

        Set<String> alldb2 = pi.getallCompoundsDATASET();
        Set<String> alldb3 = pi.getallCompoundsDB();

        for (String DBID : alldb3) {
            if (!alldb2.contains(DBID)) {
                NOMATCH++;
            } else {
                MATCH++;
                compounds.add(DBID);
            }


        }
        LOGGER.log(Level.INFO,"---------------------------Compounds");
        LOGGER.log(Level.INFO, "In db2:  {0}", alldb2.size());
        LOGGER.log(Level.INFO, "In db4:  {0}", alldb3.size());
        LOGGER.log(Level.INFO, "Not in both total:  {0}", NOMATCH);
        LOGGER.log(Level.INFO, "In both total:  {0}", MATCH);


    }

    public void getCommonProteins() throws FileNotFoundException, IOException {
        int NOMATCH = 0;
        int MATCH = 0;

        Set<String> alldb2 = pi.getallProtiensDATASET();
        Set<String> alldb3 = pi.getallProteinsDB();


        for (String DBID : alldb3) {
            if (!alldb2.contains(DBID)) {
                //System.out.println("Not in both: " + DBID);
                NOMATCH++;
            } else {
                MATCH++;
                proteins.add(DBID);
            }


        }
        LOGGER.info("---------------------------Targets");
        LOGGER.log(Level.INFO, "In db2:  {0}", alldb2.size());
        LOGGER.log(Level.INFO, "In db4:  {0}", alldb3.size());
        LOGGER.log(Level.INFO, "Not in both total:  {0}", NOMATCH);
        LOGGER.log(Level.INFO, "In both total:  {0}", MATCH);

    }

    public void ExtractMissingElements() throws FileNotFoundException, IOException {

        int validPairs = 0;
        int invalidPairs = 0;
        String DBdirPath  = op.getDrugBank4Path();
        BufferedReader br = new BufferedReader(new FileReader(DBdirPath+"nomatchesdb4.txt"));
        BufferedWriter bw = new BufferedWriter(new FileWriter(DBdirPath+"VALIDnomatchesdb4.txt"));

        String line;
        while ((line = br.readLine()) != null) {
            String[] split = line.split("\t");
            if (compounds.contains(split[0]) && proteins.contains(split[1])) {
                validPairs++;
                  bw.append(line + "\n");
            } else {

                invalidPairs++;
            }
        }

        bw.close();

        LOGGER.info("------------------Summary");
        LOGGER.log(Level.INFO, "{0} valid pairs added too {1}", new Object[]{validPairs, DBdirPath+"VALIDnomatchesdb4.txt"});
        LOGGER.log(Level.INFO, "{0} invalid pairs added too {1}", new Object[]{invalidPairs, DBdirPath+"nomatchesdb4.txt"});
        LOGGER.log(Level.INFO, "Valid pairs: {0}", validPairs);
        LOGGER.log(Level.INFO, "Invalid pairs: {0}", invalidPairs);
    }
}
