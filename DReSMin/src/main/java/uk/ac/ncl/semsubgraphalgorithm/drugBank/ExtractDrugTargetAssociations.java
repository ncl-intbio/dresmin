/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.drugBank;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import uk.ac.ncl.semsubgraphalgorithm.drugBank.DrugBankDataManager.DrugBankVersion;
import uk.ac.ncl.semsubgraphalgorithm.management.OutPutManagement;

/**
 *
 * @author joemullen
 *
 * Class goes through all files in the search directory, extracts the inferred
 * edge, uses the DrugBankDataManager to map the DrugBank and UniProt accessions.
 */
public class ExtractDrugTargetAssociations {

    private Map<String, String> DRUG;
    private Map<String, String> PROTEIN;
    private OutPutManagement op;
    private DrugBankDataManager pi;
    private String sourceDirectory;
    private String targetDirectory;
    private BufferedWriter summaryFile;
    

    public static void main(String[] args) throws FileNotFoundException, IOException {
        ExtractDrugTargetAssociations mm = new ExtractDrugTargetAssociations(DrugBankVersion.THREE);
        mm.mapp();
    }

    public ExtractDrugTargetAssociations(DrugBankVersion dbv) throws FileNotFoundException, IOException {
        this.pi = new DrugBankDataManager(dbv);
        pi.getDatassetInfo();
        pi.parseDrugBankRels();
        this.DRUG = pi.getDrugBank2NAMEsDATASET();
        this.PROTEIN = pi.getUniprot2NAMEsDATASET();
        this.op = new OutPutManagement();
        this.sourceDirectory = op.getSearchesFolder();
        this.targetDirectory = op.getDrugTargetFolder();
        this.summaryFile = new BufferedWriter(new FileWriter(op.getDrugTargetFolder() + "DT_SUMMARY.txt"));

    }

    public void mapp() throws FileNotFoundException, IOException {

        summaryFile.append("SubNam" + "\t" + "TotalPairs" + "\t" + "ReplicatedPairs" + "\t" + "NullCount" + "\t" + "Unique" + "\n");
        File folder = new File(sourceDirectory);
        if (folder.isDirectory()) {
            for (File child : folder.listFiles()) {
                if (!child.getName().contains("DS_Store") && !child.getName().contains("SUMMARY")) {
                    BufferedReader br = new BufferedReader(new FileReader(child));
                    BufferedWriter bw = new BufferedWriter(new FileWriter(targetDirectory + "DT_" + child.getName()));
                    Set<String> pairs = new HashSet<String>();
                    int count = 0;
                    int replication = 0;
                    int nullcount = 0;
                    String line;
                    while ((line = br.readLine()) != null) {
                        if (line.startsWith("IN:")) {
                            String check = line.substring(3, line.length());
                            String[] associations = check.split("\t");
                            String MATCH = DRUG.get(associations[0].replaceAll("[^A-Za-z0-9]", "").toLowerCase().trim()) + "\t" + PROTEIN.get(associations[1].replaceAll("[^A-Za-z0-9]", "").toLowerCase().trim()) + "\t" + associations[0] + "\t" + associations[1];
                            if (MATCH.contains("null")) {
                                nullcount++;
                            } else {
                                if (pairs.contains(MATCH)) {
                                    replication++;
                                    bw.append(MATCH + "\n");
                                } else {
                                    pairs.add(MATCH);
                                    count++;
                                    bw.append(MATCH + "\n");
                                }
                            }
                        }
                    }
                    bw.close();
                    br.close();
                    summaryFile.append(child.getName() + "\t" + (count + replication + nullcount) + "\t" + replication + "\t" + nullcount + "\t" + pairs.size() + "\n");
                }
            }
            summaryFile.close();
        }
    }
}
