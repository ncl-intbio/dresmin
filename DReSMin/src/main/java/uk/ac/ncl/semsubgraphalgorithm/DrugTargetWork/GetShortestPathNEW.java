/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.DrugTargetWork;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jgrapht.DirectedGraph;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.SimpleGraph;
import uk.ac.ncl.semsubgraphalgorithm.management.OutPutManagement;
import uk.ac.ncl.semsubgraphalgorithm.queryGraphs.QueryGraph;
import uk.ac.ncl.semsubgraphalgorithm.management.SerialisedGraph;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.SourceGraphNEW;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedEdge;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedNode;
import java.util.UUID;
import uk.ac.ncl.semsubgraphalgorithm.graphSplitting.GraphSplit;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.GraphMethodsNEW;

/**
 *
 * @author joemullen
 */
public class GetShortestPathNEW extends GraphMethodsNEW {

    private DirectedGraph<TypedNode, TypedEdge> targ;
    private SourceGraphNEW sp;
    private Map<String, String> DRUGBANKID;
    private Map<String, String> UNIPROTID;
    private ArrayList<String> PAIRS;
    private int found = 0;
    private int nullpath = 0;
    private int greaterthan10 = 0;
    private String fileCollated;
    private String fileIndividual;
    private String visualisationFolder;
    private Map<String, Integer> commonPaths;
    private Map<String, Integer> closedWorld;
    private int noIDs = 0;
    private SimpleGraph<TypedNode, TypedEdge> UDorigi;
    private int failedClosedWorld;
    private int occurringOnce;
    private OutPutManagement op;
    private Map<String, String> graph2name;
    private BufferedWriter brCollated;
    private BufferedWriter brIndividual;
    private final static Logger LOGGER = Logger.getLogger(GetShortestPathNEW.class.getName());

    public static void main(String[] args) throws IOException {

        OutPutManagement op = new OutPutManagement();
        SerialisedGraph sg = new SerialisedGraph();
        SourceGraphNEW p = null;
        try {
            p = sg.useSerializedNEW("graphNEW.data");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GetShortestPathNEW.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(GetShortestPathNEW.class.getName()).log(Level.SEVERE, null, ex);
        }

        GetShortestPathNEW sp = new GetShortestPathNEW(p);
        sp.Run();

        System.out.println("[INFO] Got the shortest path betwen all valid DB3 pairs");

    }

    public GetShortestPathNEW(SourceGraphNEW sg) throws IOException {

        this.sp = sg;
        this.targ = sg.getSourceGraph();
        this.PAIRS = new ArrayList<String>();
        this.DRUGBANKID = new HashMap<String, String>();
        this.UNIPROTID = new HashMap<String, String>();
        this.op = new OutPutManagement();
        this.fileCollated = op.getDrugBankShortestPathPath() + "paths_Collated.txt";
        this.fileIndividual = op.getDrugBankShortestPathPath() + "paths_Individual.txt";
        this.visualisationFolder = op.getDrugBankVisualisationPath();
        this.commonPaths = new HashMap<String, Integer>();
        this.closedWorld = new HashMap<String, Integer>();
        this.UDorigi = new SimpleGraph<TypedNode, TypedEdge>(TypedEdge.class);
        this.graph2name = new HashMap<String, String>();
        this.brCollated = new BufferedWriter(new FileWriter(fileCollated));
        this.brIndividual = new BufferedWriter(new FileWriter(fileIndividual));

    }

    public void Run() {
        try {
            String DBfile = op.getDrugBank3Path();
            getPairs(DBfile + "VALIDnomatchesdb3.txt");
            getAccessions(op.getGraphFolder() + "con_listOP_55e0c.tsv");
            convertToUndirected();
            getAllShortestPaths();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GetShortestPathNEW.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GetShortestPathNEW.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Extract the drug- target pairs from file, in the form DBID+"\t"+ UniProt
     * ID
     *
     * @param file
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void getPairs(String file) throws FileNotFoundException, IOException {
        BufferedReader br = new BufferedReader(new FileReader(file));
        String line;
        while ((line = br.readLine()) != null) {
            PAIRS.add(line);
        }
    }

    /**
     * Get the accessions for the vertices (the typed Nodes don't have drugBank
     * id and UniProt ids attached to them)
     *
     * @param file
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void getAccessions(String file) throws FileNotFoundException, IOException {
        BufferedReader br = new BufferedReader(new FileReader(file));
        String line;
        br.readLine();
        while ((line = br.readLine()) != null) {
            String[] split = line.split("\t");
            if (!split[4].equals("NO_DRUGBANK")) {
                DRUGBANKID.put(split[4], split[0]);
                //System.out.println("Added DB: " + split[0] + ":" + split[4]);
            }

            if (!split[5].equals("NO_UNIPROT")) {
                UNIPROTID.put(split[5], split[0]);
                //System.out.println("Added UNi: " + split[0] + ":" + split[5]);
            }

        }
    }

    /**
     * Get the individual shortest path.
     *
     * @param node1
     * @param node2
     * @return
     */
    public String getShortestPath(String node1, String node2) {

        TypedNode from = getNode("ID", node1, UDorigi.vertexSet());
        TypedNode to = getNode("ID", node2, UDorigi.vertexSet());

       
        LOGGER.log(Level.INFO, "[INFO] Calling getShortestPath between>>>\n {0}\n{1}\n-------------------------", new Object[]{from.toString(), to.toString()});

        if (from != null && to != null) {
            DijkstraShortestPath tr2;
            tr2 = new DijkstraShortestPath(UDorigi, from, to);
            TypedNode[] nodes = null;
            String graph = "";
            if (tr2 == null) {
                nullpath++;
            } else if (tr2.getPathLength() > 10) {
                greaterthan10++;
            } else if (tr2 != null && tr2.getPathLength() < 10) {
                TypedNode start = (TypedNode) tr2.getPath().getStartVertex();
                TypedNode end = (TypedNode) tr2.getPath().getEndVertex();
                List<TypedEdge> edges = tr2.getPath().getEdgeList();
                graph = sortPath(start, end, edges);
                return graph;
            }
        }
        return "Could Not Get Shortest Path- too large (>10)";
    }

    /**
     *
     * @throws IOException
     */
    public void getAllShortestPaths() throws IOException {


        int test = 0;
        for (String pair : PAIRS) {
//            if (test == 300) {
//                break;
//            }
            test++;
            String DB = null;
            String UNIPROT = null;
            String[] split = pair.split("\t");
            if (split.length < 2) {
                //   System.out.println("MISSING INFO: " + DB + "  " + UNIPROT);
            } else {
                if (split[0].equals("Not Available") || split[1].equals("Not Available")) {
                    // System.out.println("COULD NOT PARSE PAIR- one or more elements are missing");
                    noIDs++;
                } else {
                    if (DRUGBANKID.containsKey(split[0])) {
                        DB = DRUGBANKID.get(split[0]);
                    }
                    if (UNIPROTID.containsKey(split[1])) {
                        UNIPROT = UNIPROTID.get(split[1]);
                    }
                    if (DB != null && UNIPROT != null) {
                        String graph = getShortestPath(DB, UNIPROT);
                        //  System.out.println("Searching for path between db" + DB + " uni" + UNIPROT);
                        brIndividual.append(">>>" + graph2name.get(graph) + "\t" + DB + "\t" + UNIPROT + "\n" + graph2name.get(graph) + "\n" + graph + "\n");
                        found++;
                    } else {
                        //  System.out.println("COULD NOT FIND IDs FOR: " + split[0] + "  or: " + split[1]);
                        noIDs++;
                    }
                }
            }
        }

        brCollated.append("\n");
        brCollated.append("[INFO] SUMMARY" + "\n");
        brCollated.append("[INFO]------------------------------" + "\n");
        brCollated.append("[INFO] Searched for " + PAIRS.size() + " drug target interactions" + "\n");
        brCollated.append("[INFO]------------------------------" + "\n");
        brCollated.append("[INFO] No of different graphs found: " + commonPaths.size() + "\n");
        brCollated.append("[INFO] Total sucessful finds: " + found + "\n");
        brCollated.append("[INFO]------------------------------" + "\n");
        brCollated.append("[INFO] No IDs (failed at parsing pairs): " + noIDs + "\n");
        brCollated.append("[INFO] Nullpaths (unsuccesful): " + nullpath + "\n");
        brCollated.append("[INFO] Paths greater than 10 (not included): " + greaterthan10 + "\n");
        brCollated.append("[INFO] ------------------------------" + "\n");
        Set<String> graphs = commonPaths.keySet();
        for (int y = 1900; y > 0; y--) {
            //System.out.println("Y: "+ y);
            for (String graph : graphs) {
                if (commonPaths.get(graph) == y) {
                    getSemanticSubs(graph, y);
                }
            }
        }
        LOGGER.log(Level.INFO, "Finished calculating shortest path between {0} D-T pairs", PAIRS.size());
        brCollated.close();
        brIndividual.close();
    }

    /**
     * Creates semantic subgraphs from given String subgraphs.
     *
     * @param graph
     * @param y
     */
    public void getSemanticSubs(String graph, int y) {

        String[] split = graph.split("\n");
        String[] nodes = split[0].split("\t");
        String[] fromRels = split[1].split("\t");
        String[] toRels = split[2].split("\t");

        boolean onePoss = true;
        int mayTreatFrom = -1;
        int hasSSIsaFrom = -1;
        int mayTreatTo = -1;
        int hasSSIsaTo = -1;
        //check that there is only one possible subgraph
        for (int i = 0; i < fromRels.length; i++) {
            if (fromRels[i].contains(" ")) {
                if (fromRels[i].equals("may_prevent may_treat")) {
                    mayTreatFrom = i;
                }
                if (fromRels[i].equals("is_a has_similar_sequence")) {
                    hasSSIsaFrom = i;
                }
                onePoss = false;
            }
        }

        for (int i = 0; i < toRels.length; i++) {
            if (toRels[i].contains(" ")) {
                if (toRels[i].equals("may_prevent may_treat")) {
                    mayTreatTo = i;
                }
                if (toRels[i].equals("is_a has_similar_sequence")) {
                    hasSSIsaTo = i;
                }
                onePoss = false;
            }
        }
        //if there is only one possible subgraph create it
        if (onePoss) {
            try {
                brCollated.append("------------------------------------------------" + y + "\n");
                brCollated.append(graph2name.get(graph) + "\n" + graph + "\n");
            } catch (IOException ex) {
                Logger.getLogger(GetShortestPathNEW.class.getName()).log(Level.SEVERE, null, ex);
            }
            QueryGraph q = new QueryGraph(graph2name.get(graph) + "\n" + graph);
            q.draw(op.getDrugBankVisualisationPath());
        } else {

            //only ever two possible graphs from the same shortest path
            //either have a single "may_prevent may_treat" relation or 
            //symmetrical "is_a has_similar_sequence"
            if (mayTreatFrom >= 0) {
                //make 2 graphs
            //    System.out.println(graph);

                String[] fromRels2 = getArrayCopy(fromRels);
                String[] fromRels3 = getArrayCopy(fromRels);
           //     System.out.println("1:" + fromRels[mayTreatFrom]);
                String[] split2 = fromRels[mayTreatFrom].split("\\s");
            //    System.out.println(split2.length);
             //   System.out.println(split2[0]);
             //   System.out.println(split2[1]);
                fromRels2[mayTreatFrom] = split2[0];
                fromRels3[mayTreatFrom] = split2[1];

                String graph1 = getGraph(nodes, fromRels2, toRels);
            //    System.out.println(graph1);
                String graph2 = getGraph(nodes, fromRels3, toRels);
            //    System.out.println(graph2);

                try {
                    brCollated.append("------------------------------------------------" + y + "\n");
                    brCollated.append(graph2name.get(graph) + "=1" + "\n" + graph1 + "\n");
                    QueryGraph q1 = new QueryGraph(graph2name.get(graph) + "=1" + "\n" + graph1);
                    q1.draw(op.getDrugBankVisualisationPath());
                    brCollated.append("------------------------------------------------" + y + "\n");
                    brCollated.append(graph2name.get(graph) + "=2" + "\n" + graph2 + "\n");
                    QueryGraph q2 = new QueryGraph(graph2name.get(graph) + "=2" + "\n" + graph2);
                    q2.draw(op.getDrugBankVisualisationPath());
                } catch (IOException ex) {
                    Logger.getLogger(GetShortestPathNEW.class.getName()).log(Level.SEVERE, null, ex);
                }


            } else if (mayTreatTo >= 0) {
            //    System.out.println(graph);
                //make 2 graphs
                String[] toRels2 = getArrayCopy(toRels);
                String[] toRels3 = getArrayCopy(toRels);
            //    System.out.println("2:" + toRels[mayTreatTo]);
                String[] split2 = toRels[mayTreatTo].split("\\s");
              //  System.out.println(split2[0]);
              //  System.out.println(split2[1]);
                toRels2[mayTreatTo] = split2[0];
                toRels3[mayTreatTo] = split2[1];

                String graph1 = getGraph(nodes, fromRels, toRels2);
              //  System.out.println(graph1);
                String graph2 = getGraph(nodes, fromRels, toRels3);
              //  System.out.println(graph2);

                try {
                    brCollated.append("------------------------------------------------" + y + "\n");
                    brCollated.append(graph2name.get(graph) + "=1" + "\n" + graph1 + "\n");
                    QueryGraph q1 = new QueryGraph(graph2name.get(graph) + "=1" + "\n" + graph1);
                    q1.draw(op.getDrugBankVisualisationPath());
                    brCollated.append("------------------------------------------------" + y + "\n");
                    brCollated.append(graph2name.get(graph) + "=2" + "\n" + graph2 + "\n");
                    QueryGraph q2 = new QueryGraph(graph2name.get(graph) + "=2" + "\n" + graph2);
                    q2.draw(op.getDrugBankVisualisationPath());
                } catch (IOException ex) {
                    Logger.getLogger(GetShortestPathNEW.class.getName()).log(Level.SEVERE, null, ex);
                }


            } else if (hasSSIsaFrom >= 0) {

             //   System.out.println(graph);
                //make 2 graphs
                String[] fromRels2 = getArrayCopy(fromRels);
                String[] fromRels3 = getArrayCopy(fromRels);
                String[] split3 = fromRels[hasSSIsaFrom].split("\\s");
             //   System.out.println(split3[0]);
             //   System.out.println(split3[1]);
             //   System.out.println("3:" + fromRels[hasSSIsaFrom]);
             //   System.out.println("3:" + toRels[hasSSIsaFrom]);
                fromRels2[hasSSIsaFrom] = split3[0];
                fromRels3[hasSSIsaFrom] = split3[1];
                String[] toRels2 = getArrayCopy(toRels);
                String[] toRels3 = getArrayCopy(toRels);
                String[] split4 = toRels[hasSSIsaFrom].split("\\s");
             //   System.out.println(split4[0]);
             //   System.out.println(split4[1]);
                toRels2[hasSSIsaFrom] = split4[0];
                toRels3[hasSSIsaFrom] = split4[1];

                String graph1 = getGraph(nodes, fromRels2, toRels2);
             //   System.out.println(graph1);
                String graph2 = getGraph(nodes, fromRels3, toRels3);
             //   System.out.println(graph2);

                try {
                    brCollated.append("------------------------------------------------" + y + "\n");
                    brCollated.append(graph2name.get(graph) + "=1" + "\n" + graph1 + "\n");
                    QueryGraph q1 = new QueryGraph(graph2name.get(graph) + "=1" + "\n" + graph1);
                    q1.draw(op.getDrugBankVisualisationPath());
                    brCollated.append("------------------------------------------------" + y + "\n");
                    brCollated.append(graph2name.get(graph) + "=2" + "\n" + graph2 + "\n");
                    QueryGraph q2 = new QueryGraph(graph2name.get(graph) + "=2" + "\n" + graph2);
                    q2.draw(op.getDrugBankVisualisationPath());
                } catch (IOException ex) {
                    Logger.getLogger(GetShortestPathNEW.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

    private String getGraph(String[] nodes,
            String[] fromRels,
            String[] toRels) {

        String test = "";
        for (int i = 0; i < nodes.length; i++) {
            if (i == 0) {
                test += nodes[i];
            } else {
                test += "\t" + nodes[i];
            }
        }
        test += "\n";
        for (int i = 0; i < fromRels.length; i++) {
            if (i == 0) {
                test += fromRels[i];
            } else {
                test += "\t" + fromRels[i];
            }
        }
        test += "\n";
        for (int i = 0; i < toRels.length; i++) {
            if (i == 0) {
                test += toRels[i];
            } else {
                test += "\t" + toRels[i];
            }
        }
        return test.trim();
    }

    /**
     * Convert the graph to an undirected graph to search.
     */
    public void convertToUndirected() {

        Set<TypedNode> allNodes = targ.vertexSet();
        for (TypedNode no : allNodes) {
            UDorigi.addVertex(no);
        }
        Set<TypedEdge> allEdges = targ.edgeSet();
        for (TypedEdge ed : allEdges) {
            TypedNode from = ed.getV1();
            TypedNode to = ed.getV2();
            if (!from.getPid().equals(to.getPid())) {
                UDorigi.addEdge(from, to, new TypedEdge(from, to, ed.getType()));
            }
        }

        LOGGER.log(Level.INFO, "Created undirected graph with {0} nodes", UDorigi.vertexSet().size());
        LOGGER.log(Level.INFO, "Created undirected graph with {0} edges", UDorigi.edgeSet().size());
    }

    /**
     * Returns a node from a set of nodes by matching a provided key and value.
     *
     * @param key
     * @param value
     * @param nodes
     * @return
     */
    public TypedNode getNode(String key, String value, Set<TypedNode> nodes) {

        String lookAt = "";
        for (TypedNode n : nodes) {
            if (key.toUpperCase().equals("NAME")) {
                lookAt = n.getName();
            } else if (key.toUpperCase().equals("ID")) {
                lookAt = Integer.toString(n.getId());
            } else if (key.toUpperCase().equals("PID")) {
                lookAt = n.getPid();
            } //must be yype
            else {
                lookAt = n.getType();
            }
            if (value.toLowerCase().equals(lookAt.toLowerCase())) {
                return n;
            }
        }
        return null;

    }

    private String[] getArrayCopy(String[] orig) {
        String[] newArray = new String[orig.length];
        for (int i = 0; i < orig.length; i++) {
            newArray[i] = orig[i];
        }
        return newArray;
    }

    /**
     * Sorts the shortest path.
     *
     * @param start
     * @param end
     * @param edges
     * @return
     */
    public String sortPath(TypedNode start, TypedNode end, List<TypedEdge> edges) {
        String graph = "";
        String name = "";

        TypedNode[] nodes = new TypedNode[edges.size() + 1];
        nodes[0] = start;
        nodes[edges.size()] = end;


        //sort the nodes
        List<TypedEdge> duplicate = new ArrayList<TypedEdge>();
        duplicate.addAll(edges);

        int count = 1;
        while (count < edges.size()) {
            TypedEdge delete = null;
            TypedNode t = nodes[count - 1];
            for (TypedEdge e : duplicate) {
                TypedNode from = e.getV1();
                TypedNode to = e.getV2();
                if (from == t) {
                    nodes[count] = e.getV2();
                    delete = e;
                    break;
                } else if (to == t) {
                    nodes[count] = e.getV1();
                    delete = e;
                    break;
                }
            }

            duplicate.remove(delete);
            count++;
        }

        //get the edge types
        String fromRels = "";
        String toRels = "";
        //get the relation types 
        for (int i = 0; i < nodes.length - 1; i++) {
            List<String> fromLocal = new ArrayList<String>();
            List<String> toLocal = new ArrayList<String>();
            Set<TypedEdge> typeRelFROM = sp.getSourceGraph().getAllEdges(nodes[i], nodes[i + 1]);
            if (typeRelFROM.isEmpty()) {
                fromLocal.add("NONE");
            } else {
                for (TypedEdge t : typeRelFROM) {
                    fromLocal.add(t.getType());
                }
            }
            Set<TypedEdge> typeRelTO = sp.getSourceGraph().getAllEdges(nodes[i + 1], nodes[i]);
            if (typeRelTO.isEmpty()) {
                toLocal.add("NONE");
            } else {
                for (TypedEdge t : typeRelTO) {
                    toLocal.add(t.getType());
                }
            }

            for (int p = 0; p < fromLocal.size(); p++) {
                if (p == fromLocal.size() - 1) {
                    fromRels += fromLocal.get(p) + "\t";
                } else {
                    fromRels += fromLocal.get(p) + "|";
                }
            }
            for (int p = 0; p < toLocal.size(); p++) {
                if (p == toLocal.size() - 1) {
                    toRels += toLocal.get(p) + "\t";
                } else {
                    toRels += toLocal.get(p) + "|";
                }
            }
        }

        name = UUID.randomUUID().toString().substring(0, 8) + "_" + name;
        //then we can go back into the source grpah to get all edges between the nodes!!
        for (int i = 0; i < nodes.length; i++) {
            name += nodes[i].getType().substring(0, 1);
            graph += nodes[i].getType() + "\t";
        }

        graph += "\n" + fromRels + "\n" + toRels;

        //add all the data to the graph
        if (!graph2name.containsKey(graph)) {
            graph2name.put(graph, name);

        }

        if (commonPaths.containsKey(graph)) {
            int pathCount = commonPaths.get(graph);
            commonPaths.put(graph, pathCount + 1);
        } else {
            commonPaths.put(graph, 1);
        }

        return graph;
    }
}
