/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.sourceGraph;

import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedNode;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedEdge;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

/**
 *
 * @author joemullen
 */
public class SourceGraphNEW extends GraphMethodsNEW implements java.io.Serializable, Cloneable {

    private File nodeInfo = null;
    private File relationInfo = null;
    private DirectedGraph<TypedNode, TypedEdge> source = null;
    private HashMap<Integer, TypedNode> NodeCache;
    private static final long serialVersionUID = 33326404614661473L;

    public static void main(String[] args) throws IOException {
        //SourceGraph p = new SourceGraph();

        SourceGraphNEW p = new SourceGraphNEW(new File("/Users/joemullen/Desktop/OP_tab_exporter/Small Dataset/con_listOP_32bad.tsv"), new File("/Users/joemullen/Desktop/OP_tab_exporter/Small Dataset/rel_listOP_32bad.tsv"));
        DirectedGraph<TypedNode, TypedEdge> source = p.getSourceGraph();
    }

    public SourceGraphNEW(File nodeInfo, File relationInfo) throws IOException {
        this.nodeInfo = nodeInfo;
        this.relationInfo = relationInfo;
        this.NodeCache = new HashMap<Integer, TypedNode>();
        this.source = new DefaultDirectedGraph<TypedNode, TypedEdge>(
                TypedEdge.class);
        createSourceGraph();

    }

    public SourceGraphNEW(DirectedGraph<TypedNode, TypedEdge> source) throws IOException {
        this.source = source;
    }

    public DirectedGraph<TypedNode, TypedEdge> getSourceGraph() {
        return source;

    }

    public void createSourceGraph() throws IOException {

        //if no files have been passed then we create the test graph
        if (nodeInfo != null && relationInfo != null) {
            try {
                addNodes(nodeInfo);
                addRelations();

            } catch (FileNotFoundException fe) {
                System.out.println(fe);
            }
        }
    }

    /**
     * Add nodes to the graph.
     *
     * @param nodeInfo
     * @throws FileNotFoundException
     */
    public void addNodes(File nodeInfo) throws FileNotFoundException {

        String concep = "";
        String concep2 = "";
        String concep3 = "";
        String concep4 = "";

        Scanner x = new Scanner(nodeInfo);
        x.nextLine();
        int count = 0;
        while (x.hasNextLine()) {
            String line = x.nextLine();
            String[] temp = line.split("\t");
            String ID = temp[0];
            String PID = temp[1];
            String ConceptType = temp[2];
            String name = "";
            if (temp.length > 3) {
                name = temp[3];
            } else {
                name = "NONE_ASSIGNED";
            }
            concep += (ID + ":");
            concep2 += (PID + ":");
            concep3 += (ConceptType + ":");
            concep4 += (name + ":");
            count++;
            if (count % 5000 == 0) {
                System.out.println("[INFO] Vertices Parsed: " + count);
            }
        }

        String[] ids = concep.trim().split(":");
        String[] pids = concep2.trim().split(":");
        String[] contype = concep3.trim().split(":");
        String[] names = concep4.trim().split(":");

        int size = ids.length;

        for (int f = 0; f < size; f++) {
            int id = Integer.parseInt(ids[f]);
            String pid = pids[f];
            String type = contype[f];
            String name = names[f];
            TypedNode vertex = new TypedNode(name, type, pid, id);
            source.addVertex(vertex);
            NodeCache.put(id, vertex);
        }

    }

    /**
     * Returns a given node from the graph.
     */
    public TypedNode getNode(String key, String value) {

        Set<TypedNode> nodes = source.vertexSet();
        String lookAt = "";
        for (TypedNode n : nodes) {
            if (key.toUpperCase().equals("NAME")) {
                lookAt = n.getName();
            } else if (key.toUpperCase().equals("ID")) {
                //  System.out.println("getting here looking for ::"+ Integer.toString(n.getId()));
                lookAt = Integer.toString(n.getId());
            } else if (key.toUpperCase().equals("PID")) {
                lookAt = n.getPid();
            } //must be yype
            else {
                lookAt = n.getType();
            }

            if (value.toLowerCase().equals(lookAt.toLowerCase())) {
                return n;
            }

        }
        return null;
    }

    /**
     * Add relations to the graph.
     *
     * @throws IOException
     */
    public void addRelations() throws IOException {
        HashMap<String, String> relationCache = new HashMap<String, String>();

        BufferedReader br = new BufferedReader(new FileReader(relationInfo));
        String relType = "";
        String line;

        String sourceNode;
        String targetNode;
        int count = 0;

        //extract all the relations from our file and add them to the hashmap
        // hashmap entries are <key>:v1ID\tv2id:<value>:reltype1\trelype2...:
        while ((line = br.readLine()) != null) {
            if (line.startsWith(">>>")) {
                relType = line.substring(3, line.length());
            } else {
                String[] allRelations = line.trim().split("\t");
                for (int y = 0; y < allRelations.length; y++) {
                    String localRel = allRelations[y].trim();
                    if (count % 10000 == 0 && count > 0) {
                        System.out.println("[INFO] Edges Parsed: " + count);
                    }
                    //if we haven't come across a relation between the same nodes before
                    if (!relationCache.containsKey(localRel)
                            && !localRel.equals("")) {
                        relationCache.put(localRel, relType);
                        count++;
                        //if we have come across a relation between the 2 nodes before
                    } else if (!localRel.equals("")) {
                        String add = relationCache.get(localRel);
                        add = add + " " + relType;
                        relationCache.put(localRel, add);
                        count++;
                    }
                }
            }
        }

        //add relations to the graph
        for (String nodes : relationCache.keySet()) {
            String[] local = nodes.split(":");
            sourceNode = local[0];
            targetNode = local[1];
            String[] allTypes = relationCache.get(nodes).split("\t");
            for (int i = 0; i < allTypes.length; i++) {
                if (NodeCache.containsKey(Integer.parseInt(sourceNode)) && NodeCache.containsKey(Integer.parseInt(targetNode))) {
                    TypedEdge t = new TypedEdge(NodeCache.get(Integer.parseInt(sourceNode)), NodeCache.get(Integer.parseInt(targetNode)), allTypes[i].trim());
                    source.addEdge(NodeCache.get(Integer.parseInt(sourceNode)), NodeCache.get(Integer.parseInt(targetNode)), t);
                }
            }
        }
        //no longer require the hashmap
        NodeCache.clear();

    }

    /**
     *
     * @param initialNodes
     * @param conceptClass
     * @return
     * @throws IOException
     *
     * <CHECKED>
     */
    public HashMap<TypedNode, Set<TypedNode>> firstSemanticMatch(HashMap<TypedNode, Set<TypedNode>> initialNodes,
            String conceptClass) throws IOException {

        HashMap<TypedNode, Set<TypedNode>> matchesr = new HashMap<>();

        Set<TypedNode> matches = new HashSet();

        TypedNode sub = null;
        if (conceptClass.equals("Comp") || conceptClass.equals("Compound")) {
            for (TypedNode s : initialNodes.keySet()) {
                sub = s;
                for (TypedNode t : initialNodes.get(s)) {
                    if (t.getType().equals("Comp") || t.getType().equals("Compound")) {
                        //matches.put(s, initialNodes.get(s));
                        matches.add(t);
                    }
                }
            }
        } else {
            for (TypedNode s : initialNodes.keySet()) {
                sub = s;
                for (TypedNode t : initialNodes.get(s)) {

                    if (t.getType().equals(conceptClass)) {
                        //matches.put(s, initialNodes.get(s));
                        matches.add(t);
                    }
                }
            }
            FileWriter fstream = new FileWriter("infoOnInitalNodes.txt");
            try (BufferedWriter out = new BufferedWriter(fstream)) {
                out.append("Initial Nodes with degree >= most connected subgraph node: "
                        + initialNodes.size() + "\n");
                out.append("After applying the Semantic match (" + conceptClass + "): "
                        + matches.size() + "\n");
            }
        }
        matchesr.put(sub, matches);
        return matchesr;

    }

    public static DirectedGraph<String, DefaultEdge> randomDirectedGraph(
            Set<String> nodes, ArrayList<String> relations) {

        DirectedGraph<String, DefaultEdge> graph = new DefaultDirectedGraph<String, DefaultEdge>(
                DefaultEdge.class);


        for (String node : nodes) {
            graph.addVertex(node);

        }
        for (String rel : relations) {
            if (rel.equals("")) {
            } else {
                String[] local = rel.split(":");
                String source = local[0];
                String target = local[1];
                //System.out.println("sour: " + source + " tar:" + target);
                graph.addEdge(source, target);
            }

        }

        //System.out.println("EDGESET : " + graph.edgeSet().toString());
        //System.out.println(
        //    "Network produced: " + graph.toString());
        //return network2;
        return graph;
    }

    public HashMap<String, Integer> getCCFrequency() {
        HashMap<String, Integer> freq = new HashMap<String, Integer>();
        Set<TypedNode> allNodes = source.vertexSet();
        //get the frequency of each concept class
        int total = 0;
        for (TypedNode t : allNodes) {
            String cc = t.getType();
            if (freq.containsKey(cc)) {
                int fre = freq.get(cc);
                fre += 1;
                freq.put(cc, fre);
                total++;

            } else {
                freq.put(cc, 1);
                total++;
            }
        }

        //then work this out as a percentage of the total

        int totalpercentage = 0;
        int count = 0;
        Set<String> ccnames = freq.keySet();
        for (String cc : ccnames) {
            count++;
            int fre = freq.get(cc);
            float percent = (fre / (float) total) * 100;
            int per = (int) Math.floor(percent);

            //some are less than 1; want them present in the graph
            if (per == 0) {
                per = 1;
            }
            //don't want more than 100 percent!!!
            if (count == ccnames.size()) {

                per = 100 - totalpercentage;
            }
            totalpercentage += per;
            freq.put(cc, per);

        }
        // System.out.println(freq.toString());
        return freq;

    }

    public Set<String> getAllCCs() {

        Set<TypedNode> allNodes = source.vertexSet();
        Set<String> allConceptClasses = new HashSet<String>();
        for (TypedNode t : allNodes) {
            allConceptClasses.add(t.getType());
        }
        //System.out.println("All Concept Classes: " + allConceptClasses.toString());
        return allConceptClasses;

    }

    public int getGraphEdgesetSize() {
        return source.edgeSet().size();

    }

    public int getGraphNodeSize() {
        return source.vertexSet().size();

    }

    public Map<String, Set<String>> getRelationTypeRules() {

        Map<String, Integer> relTypesFreq = new HashMap<String, Integer>();
        Map<String, Set<String>> relTypesRules = new HashMap<String, Set<String>>();
        Map<String, Set<String>> relTypesRules2 = new HashMap<String, Set<String>>();

        Set<TypedEdge> edges = source.edgeSet();

        for (TypedEdge rel : edges) {

            String fromCC = rel.getV1().getType();
            String toCC = rel.getV2().getType();

            String ccRel = fromCC + ":" + toCC;
            String relTypes = rel.getType();

            if (!relTypesRules2.containsKey(ccRel)) {
                Set<String> relTypesH = new HashSet<String>();
                if (relTypes.contains(" ")) {
                    String[] split = relTypes.split(" ");
                    for (String rela : split) {
                        relTypesH.add(rela);
                    }
                } else {
                    relTypesH.add(relTypes);
                }
                relTypesRules2.put(ccRel, relTypesH);
            }

            if (relTypesRules2.containsKey(ccRel)) {
                Set<String> relTypesH = relTypesRules2.get(ccRel);
                if (relTypes.contains(" ")) {
                    String[] split = relTypes.split(" ");
                    for (String rela : split) {
                        relTypesH.add(rela);
                    }
                } else {
                    relTypesH.add(relTypes);

                }
                relTypesRules2.put(ccRel, relTypesH);
            }
        }
        return relTypesRules2;

    }

    public String[][] getAverageConnectivityOfEachConceptClass() {

        //all the nodes in the graph
        Set<TypedNode> nodes = source.vertexSet();

        //get the average percentage of each cc that makes up the graph
        HashMap<String, Integer> info = getCCFrequency();
        //get all the conceptClasses that may be present in th graph
        Set<String> concepts = info.keySet();
        //create a new 2d Array to store the info in
        String[][] connectivity = new String[concepts.size()][5];
        int conceptNumber = 0;
        //for every concept class we have in our graph

        for (String cc : concepts) {
            //HashMap to store conceptClasses of possible to nodes
            HashMap<String, Integer> ccto = new HashMap<String, Integer>();
            Set<String> symmetricalCCs = new HashSet<String>();

            int connectivities = 0;
            int number = 0;
            int symCount = 0;
            int simFreq = 0;
            //go through the semantic info of the graph
            for (TypedNode n : nodes) {
                //if the node = the concept class of interest
                if (n.getType().equals(cc)) {
                    number++;
                    //get all edges
                    Set<TypedEdge> local = source.outgoingEdgesOf(n);
                    int outgoing = local.size();
                    connectivities += outgoing;
                    for (TypedEdge de : local) {
                        TypedNode ed = de.getV2();
                        if (ed.getType().equals(cc)) {
                            if (source.containsEdge(ed, de.getV1())) {
                                symmetricalCCs.add(cc);
                                simFreq++;
                                symCount++;
                            } else {
                                symCount++;
                            }

                        }

                        if (ccto.containsKey(ed.getType())) {
                            Integer count = ccto.get(ed.getType());
                            ccto.put(ed.getType(), count + 1);
                            //if the target node has the same concept class as the target node 
                            //lets see if there is a symmetrical relation
                        } else {
                            ccto.put(ed.getType(), 1);
                        }

                    }
                }
            }

            String rules = "";

            //work out how many of the rels for each concept class go to which concept class


            if (ccto.size() == 0) {
                //we have no relations coming from this conceptclass,
                //no need to calculate the frequency 
            } else if (ccto.size() == 1) {
                //there is only one concept class- nothing to work out
                Set<String> cc2 = ccto.keySet();
                for (String con : cc2) {
                    //don't want rules that will not be used!
                    if (connectivities / number != 0) {
                        rules = con + "=" + Integer.toString(connectivities / number);
                    }
                }

            } else if (ccto.size() > 1) {

                //String[][] inf = new String[ccto.size()][2];

                Set<String> cc3 = ccto.keySet();
                //total number of outgoing rels
                int total = 0;
                for (String concl : cc3) {
                    total = total + ccto.get(concl);

                }

                //now we have the total lets work out the average frequency
                int numberOfrels = connectivities / number;
                int aver = total / numberOfrels;

                String[][] averages = new String[ccto.size()][3];

                int totalAllocated = 0;
                int count = 0;
                for (String concl : cc3) {
                    //total = total + ccto.get(concl);
                    averages[count][0] = concl;
                    averages[count][1] = Double.toString((double) ccto.get(concl) / aver);
                    averages[count][2] = Integer.toString(ccto.get(concl) / aver);
                    //rules = rules + concl + "=" + ccto.get(concl) / aver + ",";
                    totalAllocated += ccto.get(concl) / aver;
                    count++;

                }

                // System.out.println("BEFORE ANY ADDIION: "+Arrays.deepToString(averages));

                int leftWith = (numberOfrels - totalAllocated);

                //allocate to the conceptclasses that have 0 in first
                if (leftWith > 0) {

                    for (int p = 0; p < averages.length; p++) {
                        if (leftWith < 1) {

                            //System.out.println("ALLALLOCATED");
                            break;
                        }
                        if (Integer.parseInt(averages[p][2]) == 0) {
                            //Integer.parseInt(cc);averages[p][1]
                            averages[p][2] = "1";
                            leftWith = leftWith - 1;

                        }

                    }

                }

                if (leftWith > 0) {
                    for (int p = 0; p < averages.length; p++) {
                        if (leftWith < 1) {
                            break;
                        }

                        if (Double.parseDouble(averages[p][1]) - Integer.parseInt(averages[p][2]) > 0.5) {
                            averages[p][2] = Integer.toString(Integer.parseInt(averages[p][2]) + 1);
                            leftWith = leftWith - 1;
                        }
                    }

                }

                for (int y = 0; y < averages.length; y++) {
                    if (Integer.parseInt(averages[y][2]) != 0) {

                        rules = rules + averages[y][0] + "=" + averages[y][2] + ",";
                    }


                }
                rules = rules.substring(0, rules.length() - 1);

            }


            connectivity[conceptNumber][0] = cc;
            //System.out.println(cc);
            connectivity[conceptNumber][1] = Integer.toString(info.get(cc));
            //  System.out.println(Integer.toString(info.get(cc)));
            connectivity[conceptNumber][2] = Integer.toString(connectivities / number);
            // System.out.println(Integer.toString(connectivities / number));
            connectivity[conceptNumber][3] = rules;
            // System.out.println(rules);

            // System.out.println("there are>>> "+ number);

            // System.out.println("------------------------------------------");
            if (symmetricalCCs.size() == 0) {
            } else {
                connectivity[conceptNumber][4] = symmetricalCCs.toString().substring(1, symmetricalCCs.toString().length() - 1) + "=" + (double) simFreq / symCount;
            }
            conceptNumber++;


        }
        // System.out.println(Arrays.deepToString(connectivity));
        return connectivity;

    }

    public String getHighestConnectivity() {
        int highest = 0;
        TypedNode high = null;
        for (TypedNode node : source.vertexSet()) {
            if (source.edgesOf(node).size() > highest) {
                highest = source.edgesOf(node).size();
                high = node;
            }

        }

        System.out.println("[INFO]" + source.edgesOf(high).toString());

        return highest + " " + high.getName() + "\t" + high.getType();

    }

    public String[][] addSemanticNode(String id, String pid, String conceptClass, String name, String[][] newSemInfo) {
        String[][] newdata = new String[newSemInfo.length + 1][4];

        for (int i = 0; i < newSemInfo.length; i++) {
            for (int p = 0; p < 4; p++) {
                newdata[i][p] = newSemInfo[i][p];
            }
        }
        newdata[newSemInfo.length][0] = id;
        newdata[newSemInfo.length][1] = pid;
        newdata[newSemInfo.length][2] = conceptClass;
        newdata[newSemInfo.length][3] = name;
        return newdata;

    }

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }
}
