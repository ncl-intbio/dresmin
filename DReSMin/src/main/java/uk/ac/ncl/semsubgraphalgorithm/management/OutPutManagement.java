/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.management;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

/**
 *
 * @author joemullen
 */
public class OutPutManagement {

    private String edgeDistance = "SDC/edge_distance.txt";
    private String nodeDistance = "SDC/node_distance.txt";

    public void setNodeDistance(String node) {
        this.nodeDistance = node;
    }

    public void setEdgeDistance(String edge) {
        this.edgeDistance = edge;
    }

    
    /**

     */
    public String getDatasetDrugTargetFolder() {
        String dir = "Results/DatasetDrugTarget/";
        String sys = System.getProperty("os.name");

        return dir;
    }
    
    /**
     * Empties the temp folder, this should be done before and after every usage
     * of the temp file store
     */
    public String getDrugTargetFolder() {
        String dir = "Results/DB3WORK/DrugTargetAssociations/";
        String sys = System.getProperty("os.name");

        return dir;
    }

    /**
     * Empties the temp folder, this should be done before and after every usage
     * of the temp file store
     */
    public void emptyTempFolder() {
        String dir = "Results/Temp";
        String sys = System.getProperty("os.name");
//        if (!sys.startsWith("Mac") && !sys.startsWith("Ubun")) {
//            dir = "Results\\Temp";
//        }
        File folder = new File(sys);
        if (folder.isDirectory()) {
            for (File child : folder.listFiles()) {
                child.delete();
            }
        }
    }

    public String getEdgeDistance() {
        //String dir = ;
        //String sys = System.getProperty("os.name");
        return edgeDistance;
    }

    public String getNodeDistance() {
        //String dir = "SDC/node_distance.txt";
        //String sys = System.getProperty("os.name");
        return nodeDistance;
    }

    public String getCSSFile() {
        String dir = "Graph/graphstreamStyle.css";
        String sys = System.getProperty("os.name");
//        if (!sys.startsWith("Mac") && !sys.startsWith("Ubun")) {
//            dir = "Graph\\";
//        }
        return dir;
    }

    public String getGraphFolder() {
        String dir = "Graph/";
        String sys = System.getProperty("os.name");
//        if (!sys.startsWith("Mac") && !sys.startsWith("Ubun")) {
//            dir = "Graph\\";
//        }
        return dir;
    }

    public void emptySearchesFolder() {
        String dir = "Results/Searches";
        String sys = System.getProperty("os.name");
//        if (!sys.startsWith("Mac") && !sys.startsWith("Ubun")) {
//            dir = "Results\\Searches";
//        }
        File folder = new File(sys);
        if (folder.isDirectory()) {
            for (File child : folder.listFiles()) {
                child.delete();
            }
        }
    }

    public String getSplitVisFolder() {
        String dir = "Results/Visualisation/SplitVis/";
        String sys = System.getProperty("os.name");
//        if (!sys.startsWith("Mac") && !sys.startsWith("Ubun")) {
//            dir = "Results\\Visualisation\\SplitVis\\";
//        }
        File folder = new File(sys);
        if (folder.isDirectory()) {
            for (File child : folder.listFiles()) {
                child.delete();
            }
        }
        return dir;
    }

    public String getSemVisFolder() {
        String dir = "Results/Visualisation/SemVis/";
        String sys = System.getProperty("os.name");
//        if (!sys.startsWith("Mac") && !sys.startsWith("Ubun")) {
//            dir = "Results\\Visualisation\\SemVis\\";
//        }
        File folder = new File(sys);
        if (folder.isDirectory()) {
            for (File child : folder.listFiles()) {
                child.delete();
            }
        }
        return dir;
    }

    /**
     * Empties the searches folder, this should be done before and after every
     * usage of the search file store
     */
    public void emptySearchFolder() {
        String dir = "Results/Searches";
        String sys = System.getProperty("os.name");
//        if (!sys.startsWith("Mac") && !sys.startsWith("Ubun")) {
//            dir = "Results\\Searches";
//        }
        File folder = new File(sys);
        if (folder.isDirectory()) {
            for (File child : folder.listFiles()) {
                child.delete();
            }
        }
    }

    /**
     * Returns the string name of the results directory depending on the OS
     *
     * @return
     */
    public String getResultsName() {

        String dir = "Results/";
        String sys = System.getProperty("os.name");
//        if (!sys.startsWith("Mac") && !sys.startsWith("Ubun")) {
//            dir = "Results\\";
//        }

        return dir;

    }

    /**
     * Returns the string name of the results directory depending on the OS
     *
     * @return
     */
    public String getScalabilityName() {

        String dir = "Results/Scalability/";
        String sys = System.getProperty("os.name");
//        if (!sys.startsWith("Mac") && !sys.startsWith("Ubun")) {
//            dir = "Results\\Scalabilty\\";
//        }

        return dir;

    }

    /**
     * Returns the string name of the temp directory depending on the OS
     *
     * @return
     */
    public String getTempDirectoryName() {

        String dir = "Results/Temp";
        String sys = System.getProperty("os.name");
//        if (!sys.startsWith("Mac") && !sys.startsWith("Ubun")) {
//            dir = "Results\\Temp";
//        }

        return dir;

    }

    
     /**
     * Returns the path to the DrugBank4.2 work directory
     *
     * @return
     */
    public String getDrugBank4Path() {

        String dir = "Results/DB4WORK/";
        String sys = System.getProperty("os.name");
//        if (!sys.startsWith("Mac") && !sys.startsWith("Ubun")) {
//            dir = "Result\\DB3WORK\\";
//        }
        return dir;

    }
    
    
    /**
     * Returns the path to the DrugBank work directory
     *
     * @return
     */
    public String getDrugBank3Path() {

        String dir = "Results/DB3WORK/";
        String sys = System.getProperty("os.name");
//        if (!sys.startsWith("Mac") && !sys.startsWith("Ubun")) {
//            dir = "Result\\DB3WORK\\";
//        }

        return dir;

    }

    /**
     * Returns the path to the DrugBank shortest path directory
     *
     * @return
     */
    public String getDrugBankShortestPathPath() {

        String dir = "Results/DB3WORK/ShortestPaths/";
        String sys = System.getProperty("os.name");
//        if (!sys.startsWith("Mac") && !sys.startsWith("Ubun")) {
//            dir = "Results\\DB3WORK\\ShortestPaths\\";
//        }

        return dir;

    }

    /**
     * Returns the path to the DrugBank searches directory
     *
     * @return
     */
    public String getSearchesFolder() {

        String dir = "Results/Searches/";
        String sys = System.getProperty("os.name");
//        if (!sys.startsWith("Mac") && !sys.startsWith("Ubun")) {
//            dir = "Results\\DB3WORK\\SubGraphSearches\\";
//        }

        return dir;

    }

    /**
     * Returns the path to the DrugBank searches directory
     *
     * @return
     */
    public String getDrugBankSearchesPath() {

        String dir = "Results/DB3WORK/SubGraphSearches/";
        String sys = System.getProperty("os.name");
//        if (!sys.startsWith("Mac") && !sys.startsWith("Ubun")) {
//            dir = "Results\\DB3WORK\\SubGraphSearches\\";
//        }

        return dir;

    }

    /**
     * Returns the path to the DrugBank visualisation directory
     *
     * @return
     */
    public String getDrugBankVisualisationPath() {

        String dir = "Results/DB3WORK/SubGraphVisualisation/";
        String sys = System.getProperty("os.name");
//        if (!sys.startsWith("Mac") && !sys.startsWith("Ubun")) {
//            dir = "Results\\DB3WORK\\SubGraphVisualisation\\";
//        }

        return dir;

    }

    /**
     * Returns the path to the searches directory
     *
     * @return
     */
    public String getSearhchesDirectoryPath() {

        String dir = "Results/Searches/";
        String sys = System.getProperty("os.name");
//        if (!sys.startsWith("Mac") && !sys.startsWith("Ubun")) {
//            dir = "Results\\Searches\\";
//        }

        return dir;

    }

    /**
     * Returns the path to the unique pairs directory
     *
     * @return
     */
    public String getuniquePairsDirectoryPath() {

        String dir = "Results/DB3WORK/UniquePairs/";
        String sys = System.getProperty("os.name");
//        if (!sys.startsWith("Mac") && !sys.startsWith("Ubun")) {
//            dir = "Results\\Db3WORK\\UniquePairs\\";
//        }

        return dir;

    }

    /**
     * Returns the path to all the pairs directory
     *
     * @return
     */
    public String getALLPairsDirectoryPath() {

        String dir = "Results/DB3WORK/AllPairs/";
        String sys = System.getProperty("os.name");
//        if (!sys.startsWith("Mac") && !sys.startsWith("Ubun")) {
//            dir = "Results\\Db3WORK\\AllPairs\\";
//        }

        return dir;

    }

    /**
     * Returns the path to the visualization directory
     *
     * @return
     */
    public String getVisualisationDirectoryPath() {

        String dir = "Results/Visualisation/";
        String sys = System.getProperty("os.name");
//        if (!sys.startsWith("Mac") && !sys.startsWith("Ubun")) {
//            dir = "Results\\Visualisation\\";
//        }

        return dir;

    }
}
