/*
 * Class takes a sub graph
 * If it has |V|  > 4 then the graph is split into graphs containing nodes of !> 4
 */
package uk.ac.ncl.semsubgraphalgorithm.graphSplitting;
/*
 * How can we quantify splitting the subgraphs?s
 * Based on connectivity?!?!
 * Graph measure 
 */

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jgrapht.DirectedGraph;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import uk.ac.ncl.semsubgraphalgorithm.management.OutPutManagement;
import uk.ac.ncl.semsubgraphalgorithm.queryGraphs.QueryGraph;
import uk.ac.ncl.semsubgraphalgorithm.queryGraphs.QueryGraph.SemanticSub;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.GraphMethodsNEW;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedEdge;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedNode;

/**
 *
 * @author joemullen this is the implemtation of the GraphSplit
 * algorithm described in the paper. It takes a query graph and returns a set of
 * query graphs whereby every graph in the set has a nodeset of less than 4.
 */
public class GraphSplit extends GraphMethodsNEW {

    private QueryGraph q;
    private DirectedGraph<TypedNode, TypedEdge> original;
    private UndirectedGraph<TypedNode, TypedEdge> UDorigi;
    private Set<GraphSplitInstance> newSubs;
    private HashMap<String, ArrayList<TypedNode>> nodeForeachGraph;
    private Set<DefaultEdge> extraEdges;
    private HashMap<TypedNode, int[]> edgset;
    private TypedNode mostConnected;
    private int subsizes = 3;
    private GraphSplitInstance splitGraph;
    private QueryGraph orig;
    private boolean draw;
    private static int random = 0;
    private OutPutManagement op;
    private final static Logger LOGGER = Logger.getLogger(GraphSplit.class.getName());
    private String graphOne;
    private String graphTwo;

    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException {
        //use random graph
        //SerialisedGraph ser = new SerialisedGraph();
        //SourceGraph source = ser.useSerialized("ran_1000_graph.data");
        // SourceGraph source = ser.useSerialized("graph.data");
        //RandomQuery ran = new RandomQuery(source, 7, "Compound");

        QueryGraph six = new QueryGraph(SemanticSub.SIXNODE);

        //split query
        GraphSplit g = new GraphSplit(six, true);
    }

    public GraphSplit(QueryGraph s, boolean draw) {

        this.op = new OutPutManagement();
        this.orig = s;
        this.q = s;
        this.original = s.getSub();
        this.newSubs = new HashSet<GraphSplitInstance>();
        this.extraEdges = new HashSet<DefaultEdge>();
        this.edgset = nameEdgeset(original);
        this.mostConnected = mostConnectedTop(edgset);
        this.UDorigi = new SimpleGraph<TypedNode, TypedEdge>(TypedEdge.class);
        this.nodeForeachGraph = new HashMap<String, ArrayList<TypedNode>>();
        this.splitGraph = new GraphSplitInstance();
        this.draw = draw;
        this.graphOne = UUID.randomUUID().toString().substring(0, 8);
        this.graphTwo = UUID.randomUUID().toString().substring(0, 8);
        long startTime = System.currentTimeMillis();
        run();
        long endTime = System.currentTimeMillis();
        LOGGER.log(Level.INFO, "Splitting large subgraph took {0} seconds", (endTime - startTime) / 1000);


    }

    public void run() {
        // System.out.println("[INFO] Starting to split sub of size " + original.vertexSet().size());
        convertToUndirected();
        //if the subgraph is a clique
        if (cliqueTest()) {
            splitClique();
        } else {
            calcShortestPath();
            createSubs();
        }
    }

    public GraphSplitInstance getSplit() {

        return splitGraph;
    }
    
    public String getGraphOneUID(){
        return graphOne;
    }
    
     public String getGraphTwoUID(){
        return graphTwo;
    }

    /**
     * Test to see if the subgraph is a clique
     *
     * @return
     *
     * <CHECKED>
     */
    public boolean cliqueTest() {
        Set<TypedNode> allNodes = UDorigi.vertexSet();
        for (TypedNode s : allNodes) {
            Set<TypedEdge> edges = UDorigi.edgesOf(s);
            for (TypedNode s2 : allNodes) {
                if (!s.equals(s2)) {
                    if (!UDorigi.containsEdge(s, s2)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * If the original graph is a clique then split randomly.
     *
     * <CHECKED>
     */
    public void splitClique() {

        QueryGraph q1 = null;
        QueryGraph q2 = null;
        List<TypedNode> one = new ArrayList<>();
        List<TypedNode> two = new ArrayList<>();
        Set<TypedNode> allNodes = UDorigi.vertexSet();
        int count = 0;
        for (TypedNode s : allNodes) {
            if (count == 0) {
                mostConnected = s;
                one.add(s);
                two.add(s);
            } else if (one.size() == two.size()) {
                one.add(s);
            } else if (one.size() > two.size()) {
                two.add(s);
            }
            count++;
        }
        nodeForeachGraph.put(graphOne, (ArrayList<TypedNode>) one);
        nodeForeachGraph.put(graphTwo, (ArrayList<TypedNode>) two);
        createSubs();


    }

    /**
     * Converts the graph to an undirected graph for the shortest path search.
     *
     * <CHEC>
     */
    public void convertToUndirected() {
        Set<TypedNode> allNodes = original.vertexSet();
        for (TypedNode no : allNodes) {
            UDorigi.addVertex(no);
        }
        Set<TypedEdge> allEdges = original.edgeSet();
        for (TypedEdge ed : allEdges) {
            TypedNode from = ed.getV1();
            //System.out.println("From: "+ from.toString());
            TypedNode to = ed.getV2();
            //System.out.println("To: "+ to.toString());
            UDorigi.addEdge(from, to, new TypedEdge(from, to, ed.getType()));
        }
    }

    /**
     * Get the individual shortest path.
     *
     * @param node1
     * @param node2
     * @return
     */
    public void calcShortestPath() {

        Set<TypedNode> allNodes = UDorigi.vertexSet();
        //System.out.println("allNodes: " + allNodes.toString());
        TypedNode from = null;
        TypedNode to = null;
        int distance = 0;
        for (TypedNode no : allNodes) {
            for (TypedNode no2 : allNodes) {
                DijkstraShortestPath tr2 = new DijkstraShortestPath(UDorigi, no, no2);
                int dis = tr2.getPathEdgeList().size();
                if (dis > distance) {
                    distance = dis;
                    from = no;
                    to = no2;
                }

            }

        }

        DijkstraShortestPath tr2 = new DijkstraShortestPath(UDorigi, mostConnected, from);
        int disOne = tr2.getPathEdgeList().size();
        ArrayList<TypedNode> nodes1 = nodesFromEdgeList(tr2.getPathEdgeList());
        nodeForeachGraph.put(graphOne, nodes1);


        DijkstraShortestPath tr3 = new DijkstraShortestPath(UDorigi, mostConnected, to);
        int distwo = tr3.getPathEdgeList().size();
        ArrayList<TypedNode> nodes2 = nodesFromEdgeList(tr3.getPathEdgeList());
        // System.out.println("Nodes 2 initial: " + nodes2.toString());
        nodeForeachGraph.put(graphTwo, nodes2);

        // System.out.println("FirstCheck: " + nodeForeachGraph.toString());
        ArrayList<TypedNode> leftowa = nodesNotIncluded(nodesFromEdgeList(tr2.getPathEdgeList()), nodesFromEdgeList(tr3.getPathEdgeList()));
        // System.out.println("leftowa: " + leftowa.toString());
        allocatingLefOverNodes(leftowa);

    }

    public void createSubs() {
        Set<String> graphs = nodeForeachGraph.keySet();
        //System.out.println("create Subs: " + nodeForeachGraph.toString());
        String addtoo = "";
        int count = 0;
        Set<TypedEdge> allEdgeFromOrig = original.edgeSet();
        Set<DefaultEdge> added = new HashSet<DefaultEdge>();
        Set<DefaultEdge> NotADDED = new HashSet<DefaultEdge>();
        QueryGraph q1 = null;
        QueryGraph q2 = null;

        for (String gr : graphs) {
            count++;
            ArrayList<TypedNode> thisgraph = nodeForeachGraph.get(gr);
            DirectedGraph<TypedNode, TypedEdge> name = new DefaultDirectedGraph<TypedNode, TypedEdge>(TypedEdge.class);
            for (TypedNode node : thisgraph) {
                name.addVertex(node);
            }

            for (TypedEdge loc : allEdgeFromOrig) {
                TypedNode from = original.getEdgeSource(loc);
                TypedNode to = original.getEdgeTarget(loc);
                if (name.containsVertex(from) && name.containsVertex(to)) {
                    name.addEdge(from, to, loc);
                    added.add(loc);
                }
            }

            if (count == 1) {
                q1 = new QueryGraph(name, gr);

                if (draw == true) {
                    q1.draw(op.getVisualisationDirectoryPath());
                }

                // System.out.println("[INFO] Created the sub: " + name.toString());
                // System.out.println("[INFO]  " + q1.toString());
            }

            if (count == 2) {
                q2 = new QueryGraph(name, gr);
                if (draw == true) {
                    q2.draw(op.getVisualisationDirectoryPath());
                }

                // System.out.println("[INFO] Created the sub: " + name.toString());
                // System.out.println("[INFO]  " + q2.toString());
            }

        }

        for (TypedEdge ed : allEdgeFromOrig) {
            if (!added.contains(ed)) {
                NotADDED.add(ed);
            }

        }

        if (draw == true) {
            q.draw(op.getVisualisationDirectoryPath());
        }


        // System.out.println("[INFO] Edges left over: " + NotADDED.toString());
        // System.out.println("[INFO] Crossover node " + getCrossoverNode() + " with edgeset " + Arrays.toString(getCrossoverNodeEdgeset()));

        splitGraph = new GraphSplitInstance(q1, q2, orig, mostConnected);

    }

    public void allocatingLefOverNodes(ArrayList<TypedNode> notMatched) {

        for (TypedNode nod : notMatched) {
            // System.out.println("Allocating: " + nod);
            //if the node is only connected to one other node add it to that graph
            if (rule1(nod) == true) {
                addRule1(nod);
                //     System.out.println("Added rule_1");
                //if it has a greater number of edges between one of the graphs
            } else if (!rule2(nod).equals("equal")) {
                String moreRelsInGraph = rule2(nod);
                addRule2(nod, moreRelsInGraph);
                //    System.out.println("Added rule_2");
                //if it has an equal number of edges between the 2 graphs add it to the smallest graph
                //if the graphs are of different node set sizes
            } else if (rule3(nod) == true) {
                addRule3(nod);
                //  System.out.println("Added rule_3");
            } //else just add it to graph 1
            else {
                //alternate adding nodes to either of the graphs
                if (random % 2 == 0) {
                    //      System.out.println("Added rule_4");
                    ArrayList<TypedNode> h = nodeForeachGraph.get(graphOne);
                    h.add(nod);
                    nodeForeachGraph.put(graphOne, h);
                } else {
                    //       System.out.println("Added rule_4");
                    ArrayList<TypedNode> h = nodeForeachGraph.get(graphTwo);
                    h.add(nod);
                    nodeForeachGraph.put(graphTwo, h);
                }
                random++;
            }
        }
    }

    public ArrayList<TypedNode> nodesNotIncluded(ArrayList<TypedNode> one, ArrayList<TypedNode> two) {
        ArrayList<TypedNode> local = new ArrayList<TypedNode>(one);
        local.addAll(two);

        ArrayList<TypedNode> noMatch = new ArrayList<TypedNode>();
        Set<TypedNode> all = original.vertexSet();
        for (TypedNode node : all) {
            if (!local.contains(node)) {
                noMatch.add(node);
            }
        }
        return noMatch;
    }

    public ArrayList<TypedNode> nodesFromEdgeList(List<TypedEdge> edgelist) {
        ArrayList<TypedNode> nodes = new ArrayList<TypedNode>();

        int count = 0;
        for (TypedEdge rel : edgelist) {
            TypedNode source = UDorigi.getEdgeSource(rel);
            TypedNode targ = UDorigi.getEdgeTarget(rel);
            count++;

            if (count == 1) {
                nodes.add(source);
                nodes.add(targ);

            } else {
                if (!nodes.contains(targ)) {
                    nodes.add(targ);
                } else {
                    nodes.add(source);
                }
            }

        }

        return nodes;
    }

    //calculates how many new subgraphs will (ideally) be created
    public int getNumberOfnewSubs(int graphSize) {

        int g = graphSize + 1;
        int newSubs = 1;
        for (int i = 0; i < 10; i++) {

            if (g < subsizes && g > 0) {
                newSubs = i + 1;
                break;
            }
            if (g == 0) {
                newSubs = i;
                break;
            } else {
                g = g - subsizes;
            }

        }

        //System.out.println("There will be this many new graphs " + newSubs);
        return newSubs;
    }

    public ArrayList<Integer> getSizeOfNewSubs(int numberofsubs, int graphsize) {
        ArrayList<Integer> insubsizes = new ArrayList<Integer>();
        int withOverlap = graphsize + (numberofsubs - 1);
        for (int i = 0; i < 10; i++) {

            if (withOverlap > subsizes) {
                insubsizes.add(subsizes);
            } else if (withOverlap < subsizes || withOverlap == subsizes) {
                insubsizes.add(withOverlap);
                break;

            }
            withOverlap = withOverlap - subsizes;
        }
        //System.out.println("Of these sizes " + insubsizes.toString());

        return insubsizes;

    }

    /*
     * Below we have all the rules by which the remaining nodes are allocated to 
     * the smaller graphs. This is detailed in the Supplementary Material of the 
     * accompanying paper.
     */
    public boolean rule1(TypedNode node) {
        boolean rule1 = false;
        if (UDorigi.degreeOf(node) == 1) {
            rule1 = true;
        }
        return rule1;
    }

    public void addRule1(TypedNode node) {
        Set<TypedEdge> edgis = UDorigi.edgesOf(node);
        TypedNode targ = null;
        for (TypedEdge s : edgis) {
            TypedNode src = UDorigi.getEdgeSource(s);
            TypedNode tar = UDorigi.getEdgeTarget(s);
            if (!node.equals(src)) {
                targ = src;
            } else {
                targ = tar;
            }
        }

        Set<String> graphs = nodeForeachGraph.keySet();
        String addtoo = "";

        for (String gr : graphs) {
            ArrayList<TypedNode> thisgraph = nodeForeachGraph.get(gr);
            if (thisgraph.contains(targ)) {
                addtoo = gr;
            }

        }
        //if the ON is the node that the node connects too that means
        //it can be connected to EITHER graph- in that case we want to
        //add it to the smallest
        if (targ.equals(getCrossoverNode())) {
            for (String gr : graphs) {
                //if it can be connected to both graphs than add it to the smallest
                if (nodeForeachGraph.get(gr).size() < nodeForeachGraph.get(addtoo).size()) {
                    addtoo = gr;

                }
            }
        }

        ArrayList<TypedNode> h = nodeForeachGraph.get(addtoo);
        h.add(node);
        nodeForeachGraph.put(addtoo, h);

    }

    public String rule2(TypedNode node) {
        String moreRels = "equal";
        //get the edges
        Set<TypedEdge> ed = original.edgesOf(node);
        //see if how many edges attach to each graph
        int d1 = 0;
        int d2 = 0;
        String d1name = "";
        String d2name = "";

        //store the names for each graph
        int count = 0;
        for (String gr : nodeForeachGraph.keySet()) {
            if (count == 0) {
                d1name = gr;
            } else {
                d2name = gr;
            }
            count++;

        }

        //how many edges are there between the node to be allocated and the nodes in the first graph?
        for (TypedNode nod : nodeForeachGraph.get(d1name)) {

            if (original.containsEdge(nod, node)) {
                d1++;
            }

            if (original.containsEdge(node, nod)) {
                d1++;
            }

        }

        //how many edges are there between the node to be allocated and the nodes in the second graph?
        for (TypedNode nod : nodeForeachGraph.get(d2name)) {

            if (original.containsEdge(nod, node)) {
                d2++;
            }

            if (original.containsEdge(node, nod)) {
                d2++;
            }
        }

        if (d1 > d2) {
            moreRels = d1name;
        }

        if (d1 < d2) {
            moreRels = d2name;
        }

        // System.out.println("More rels: " + moreRels);
        return moreRels;
    }

    public void addRule2(TypedNode node, String graphName) {
        ArrayList<TypedNode> h = nodeForeachGraph.get(graphName);
        h.add(node);
        nodeForeachGraph.put(graphName, h);
    }

    public boolean rule3(TypedNode node) {
        boolean equal = false;
        //get the edges
        Set<TypedEdge> ed = original.edgesOf(node);
        //see if how many edges attach to each graph
        int d1 = 0;
        int d2 = 0;
        String d1name = "";
        String d2name = "";

        //store the names for each graph
        int count = 0;
        for (String gr : nodeForeachGraph.keySet()) {
            if (count == 0) {
                d1name = gr;
            } else {
                d2name = gr;
            }
            count++;

        }



        //how many edges are there between the node to be allocated and the nodes in the first graph?
        for (TypedNode nod : nodeForeachGraph.get(d1name)) {

            if (original.containsEdge(nod, node)) {
                d1++;
            }

            if (original.containsEdge(node, nod)) {
                d1++;
            }

        }

        //how many edges are there between the node to be allocated and the nodes in the second graph?
        for (TypedNode nod : nodeForeachGraph.get(d2name)) {

            if (original.containsEdge(nod, node)) {
                d2++;
            }

            if (original.containsEdge(node, nod)) {
                d2++;
            }

        }

        //if the node contains the same numbe of edges between the graphs &&& the graphs are different sizes
        if (d1 == d2 && (nodeForeachGraph.get(graphOne).size() != nodeForeachGraph.get(graphTwo).size())) {
            equal = true;

        }
        return equal;

    }

    public void addRule3(TypedNode node) {


        String smallestGraph = "";
        int size = 100;
        for (String gr : nodeForeachGraph.keySet()) {
            if (nodeForeachGraph.get(gr).size() < size) {
                smallestGraph = gr;
                size = nodeForeachGraph.get(gr).size();

            }
        }
        ArrayList<TypedNode> h = nodeForeachGraph.get(smallestGraph);
        h.add(node);
        nodeForeachGraph.put(smallestGraph, h);

    }
    /*
     * Get set methods
     */

    public TypedNode getCrossoverNode() {
        return mostConnected;
    }
    //get the edgeset of the most connected node as it is in the original subgraph

    public int[] getCrossoverNodeEdgeset() {
        return edgset.get(mostConnected);
    }

    public Set<GraphSplitInstance> getNewSubs() {
        return newSubs;
    }

    public Set<DefaultEdge> getExtraEdges() {
        return extraEdges;
    }

    public QueryGraph getOrignalQueryGraph() {
        return q;
    }
}
