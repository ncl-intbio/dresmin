/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.DrugTargetWork;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.semsubgraphalgorithm.SemSearch.SearchManager;
import uk.ac.ncl.semsubgraphalgorithm.management.OutPutManagement;
import uk.ac.ncl.semsubgraphalgorithm.management.SerialisedGraph;
import uk.ac.ncl.semsubgraphalgorithm.queryGraphs.QueryGraph;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.SourceGraphNEW;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 *
 * Class takes all the shortest paths, creates semantic subgraphs from them and
 * then does a search for them.
 *
 */
public class SearchShortestPaths {

    private SourceGraphNEW source;
    private OutPutManagement op;
    private String file;
    private Set<String> alreadySearched;
    private int subId;
    private String opDir;
    private String nodeDistance;
    private String edgeDistance;
    private final static Logger LOGGER = Logger.getLogger(SearchShortestPaths.class.getName());
    private double threshold;

    /**
     * Run a search for each of the semantic subgraphs.
     *
     * @param args
     */
    public static void main(String[] args) {

        LOGGER.log(Level.INFO, "Graph {0}", args[0]);
        LOGGER.log(Level.INFO, "Collated paths {0}", args[1]);
        LOGGER.log(Level.INFO, "Node distance {0}", args[2]);
        LOGGER.log(Level.INFO, "Edge distance {0}", args[3]);
        LOGGER.log(Level.INFO, "Output {0}", args[4]);
        LOGGER.log(Level.INFO, "Searching for subID {0}", args[5]);
        LOGGER.log(Level.INFO, "Using threshold {0}", args[6]);

        //e.g
        //java -jar SemSubgraphAlgorithm-jar-with-dependencies.jar /Users/joemullen/Desktop/SemSubNEW/DReSMin/Graph/graphNEW.data /Users/joemullen/Desktop/SemSubNEW/DReSMin/Results/DB3WORK/ShortestPaths/paths_Collated.txt /Users/joemullen/Desktop/SemSubNEW/DReSMin/SDC/node_distance.txt /Users/joemullen/Desktop/SemSubNEW/DReSMin/SDC/edge_distance.txt /Users/joemullen/Desktop/ 1.0 1

        OutPutManagement op = new OutPutManagement();

        try {
            SerialisedGraph sg = new SerialisedGraph();
            SourceGraphNEW source = null;
            try {
                source = sg.useSerializedNEW(args[0]);
                //source =sg.useSerializedNEW(op.getGraphFolder()+"graphNEW.data");
            } catch (FileNotFoundException ex) {
                Logger.getLogger(SearchShortestPaths.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(SearchShortestPaths.class.getName()).log(Level.SEVERE, null, ex);
            }
            SearchShortestPaths search = new SearchShortestPaths(source, args[1], args[4], args[2], args[3], Integer.parseInt(args[5]), Double.parseDouble(args[6]));
            //SearchShortestPaths search = new SearchShortestPaths(source, "Results/DB3WORK/ShortestPaths/paths_Collated.txt", "/Users/joemullen/Desktop/", "SDC/node_distance.txt", "SDC/edge_distance.txt", 181, 1.0);
            search.run();
        } catch (IOException ex) {
            Logger.getLogger(SearchShortestPaths.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Constructors.
     *
     * @param source
     * @param start
     */
    public SearchShortestPaths(SourceGraphNEW source, String collated, String outPutDir, String nodeDistance, String edgeDistance, int sub, double ST) {
        this.source = source;
        this.op = new OutPutManagement();
        this.file = collated;
        this.alreadySearched = new HashSet<String>();
        this.subId = sub;
        this.nodeDistance = nodeDistance;
        this.edgeDistance = edgeDistance;
        this.opDir = outPutDir;
        this.threshold = ST;
    }

    /**
     * Run the split search for the block of 50.
     */
    public void run() {
        BufferedReader br = null;
        QueryGraph query = null;
        boolean first = false;
        int count = 1;
        try {
            br = new BufferedReader(new FileReader(file));
            String line;
            try {
                StringBuilder sub = new StringBuilder();
                while ((line = br.readLine()) != null) {
                    if (line.startsWith("-----")) {
                        if (!sub.toString().isEmpty()) {
                            query = new QueryGraph(sub.toString());
                            sub = new StringBuilder();
                            if (count == subId) {
                                SearchManager search = new SearchManager(source, query, null, "Compound", true, threshold, true, true, true, opDir, nodeDistance, edgeDistance, 2);
                                search.setOutputDeets(false);
                                search.run();
                                break;
                            }
                            count++;
                        }
                    }
                    if (!line.startsWith("[INFO]") && !line.isEmpty() && !line.startsWith("--")) {
                        sub.append(line).append("\n");
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(SearchShortestPaths.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SearchShortestPaths.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(SearchShortestPaths.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public String getSubName(String fileName) {
        return fileName.replace("[", "").split("_")[0] + "_" + fileName.replace("]", "").split("_")[1];
    }
}
