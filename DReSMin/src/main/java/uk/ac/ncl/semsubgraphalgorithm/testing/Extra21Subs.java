///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package uk.ac.ncl.semsubgraphalgorithm.testing;
//
//import java.io.BufferedReader;
//import java.io.BufferedWriter;
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.FileReader;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.Map;
//import java.util.Set;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import uk.ac.ncl.semsubgraphalgorithm.management.OutPutManagament;
//import uk.ac.ncl.semsubgraphalgorithm.QueryGraph;
//import uk.ac.ncl.semsubgraphalgorithm.SemSearch;
//import uk.ac.ncl.semsubgraphalgorithm.management.SerialisedGraph;
//import uk.ac.ncl.semsubgraphalgorithm.SourceGraph;
//import uk.ac.ncl.semsubgraphalgorithm.drugBankWork.SubGraphMatches2Accession;
//
///**
// *
// * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
// */
//public class Extra21Subs {
//
//    private OutPutManagament op;
//    private Set<String> db3Pairs;
//    private Set<String> subgraphmatchedPairs;
//    private Set<String> validatedPairs;
//
//    public Extra21Subs() {
//        this.op = new OutPutManagament();
//        this.db3Pairs = new HashSet<String>();
//        this.subgraphmatchedPairs = new HashSet<String>();
//        this.validatedPairs = new HashSet<String>();
//    }
//
//    public static void main(String[] args) {
//        Extra21Subs esubs = new Extra21Subs();
//        QueryGraph q = new QueryGraph(QueryGraph.SemanticSub.SUB37);
//        // esubs.search(q);
//        //esubs.getUniqueRelations();
//        //    esubs.validateUniqueRelations("Results/DB3WORK/UniquePairs/CTCT_Q3.txt");
////        Set<String> files = new HashSet<String>();
////        files.add("Results/DB3WORK/UniquePairs/CHLOR.txt");
////        files.add("Results/ExtraSearches/Unique/subNumber6.txt");
////        files.add("Results/DB3WORK/UniquePairs/CTCT.txt");
////        files.add("Results/DB3WORK/UniquePairs/NEWTargetSMALL.txt");
////        files.add("Results/ExtraSearches/Unique/subNumber12.txt");
//        esubs.goScore();
//
//        // esubs.getInteractionsInAll(files);
//    }
//
//    public void getInteractionsInAll(Set<String> files) {
//
//        Set<String> all = new HashSet<String>();
//        BufferedReader br = null;
//        int count = 0;
//        for (String file : files) {
//            try {
//                Set<String> temp = new HashSet<String>();
//                br = new BufferedReader(new FileReader(new File(file)));
//                String line;
//                try {
//                    while ((line = br.readLine()) != null) {
//                        all.add(line);
//                        //if its first file just add to all
//                        if (count == 0) {
//                            all.add(line.split("\t")[0] + "\t" + line.split("\t")[1]);
//                        } else {
//                            if (all.contains(line.split("\t")[0] + "\t" + line.split("\t")[1])) {
//                                temp.add(line.split("\t")[0] + "\t" + line.split("\t")[1]);
//                            }
//
//
//                        }
//                    }
//                } catch (IOException ex) {
//                    Logger.getLogger(Extra21Subs.class.getName()).log(Level.SEVERE, null, ex);
//                }
//                if (count > 0) {
//                    all = temp;
//                }
//                count++;
//            } catch (FileNotFoundException ex) {
//                Logger.getLogger(Extra21Subs.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//
//        System.out.println("In all " + all.size());
//        for (String interaction : all) {
//            System.out.println(interaction);
//        }
//    }
//
//    public void validateUniqueRelations(String subFile) {
//
//        String fileRead = subFile;//"Results/ExtraSearches/Unique/" + subFile;
//        String fileWrite = "Results/ExtraSearches/ValidatedPairs/Q3"; //+ subFile;
//
//        BufferedWriter bw = null;
//        try {
//            bw = new BufferedWriter(new FileWriter(new File(fileWrite)));
//        } catch (IOException ex) {
//            Logger.getLogger(Extra21Subs.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        try {
//            try {
//                //get the valid db3 interations
//                BufferedReader db3valid = null;
//                try {
//                    db3valid = new BufferedReader(new FileReader(op.getDrugBankDirectoryPath() + "all_Valid.txt"));
//                } catch (FileNotFoundException ex) {
//                    Logger.getLogger(Extra21Subs.class.getName()).log(Level.SEVERE, null, ex);
//                }
//
//                String line;
//
//                while ((line = db3valid.readLine()) != null) {
//                    db3Pairs.add(line);
//
//                }
//            } catch (IOException ex) {
//                Logger.getLogger(Extra21Subs.class.getName()).log(Level.SEVERE, null, ex);
//            }
//
//            //get the interactions inferred by the sub
//            BufferedReader subgraphIdentified = null;
//            try {
//                subgraphIdentified = new BufferedReader(new FileReader(new File(fileRead)));
//            } catch (FileNotFoundException ex) {
//                Logger.getLogger(Extra21Subs.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            String line2;
//            while ((line2 = subgraphIdentified.readLine()) != null) {
//                subgraphmatchedPairs.add(line2.split("\t")[0] + "\t" + line2.split("\t")[1]);
//            }
//        } catch (IOException ex) {
//            Logger.getLogger(Extra21Subs.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        //go Compare!!!!
//        for (String p : db3Pairs) {
//            // System.out.println(p);
//            if (subgraphmatchedPairs.contains(p)) {
//                validatedPairs.add(p);
//            }
//        }
//
//        //write validated relations to file
//        for (String p : validatedPairs) {
//            try {
//                bw.append(p + "\n");
//            } catch (IOException ex) {
//                Logger.getLogger(Extra21Subs.class.getName()).log(Level.SEVERE, null, ex);
//            }
//
//        }
//        try {
//            bw.close();
//        } catch (IOException ex) {
//            Logger.getLogger(Extra21Subs.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        System.out.println("[INFO] DB3 pairs: " + db3Pairs.size());
//        System.out.println("[INFO] Validated DB3 Pairs: " + validatedPairs.size());
//    }
//
//    /**
//     * Identify the number of unique numbers. Need to specify the location of
//     * the file that was output after the search as well as the query graph that
//     * was used during the search. (all of these are stored in the ExtraSearches
//     * folder)
//     */
//    public void getUniqueRelations() {
//        String file = "Results/ExtraSearches/[subNumber37]_ST0.8_14188416953.txt";
//        QueryGraph query = new QueryGraph(QueryGraph.SemanticSub.SUB37);
//
//        try {
//            SubGraphMatches2Accession mm = new SubGraphMatches2Accession(file, query, true, true);
//            mm.mapp();
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(Extra21Subs.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (IOException ex) {
//            Logger.getLogger(Extra21Subs.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//
//    }
//    /*
//     * Variables used for search.
//     */
//
//    public void search(QueryGraph q) {
//        try {
//            SerialisedGraph ser = new SerialisedGraph();
//            SourceGraph source = null;
//            try {
//                source = ser.useSerialized("graph.data");
//            } catch (FileNotFoundException ex) {
//                Logger.getLogger(Extra21Subs.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (ClassNotFoundException ex) {
//                Logger.getLogger(Extra21Subs.class.getName()).log(Level.SEVERE, null, ex);
//            }
//
//            SemSearch sem = new SemSearch(source, q, null, null, 8, "Compound", true, 0.8, true, true, true);
//            sem.completeSearch();
//        } catch (IOException ex) {
//            Logger.getLogger(Extra21Subs.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
//    /**
//     * Rank the mappings
//     */
//    public void goScore() {
//
//        Set<String> mapped = new HashSet<String>();
//
//        Map<String, Double> scored = new HashMap<String, Double>();
//
//        Map<String, Double> fileAndScores = new HashMap<>();
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber1.txt", 40.702);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber3.txt", 7.06);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber4.txt", 3.462);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber6.txt", 1.93);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber12.txt", 0.907);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber5.txt", 0.833);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber2.txt", 0.737);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber16.txt", 0.466);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber18.txt", 0.466);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber15.txt", 0.412);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber21.txt", 0.256);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber9.txt", 0.251);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber19.txt", 0.244);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber27.txt", 0.231);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber8.txt", 0.224);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber34.txt", 0.191);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber14.txt", 0.19);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber20.txt", 0.188);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber11.txt", 0.12);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber22.txt", 0.102);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber10.txt", 0.086);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber24.txt", 0.068);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber25.txt", 0.053);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber13.txt", 0.043);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber7.txt", 0.037);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber29.txt", 0.028);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber23.txt", 0.018);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber30.txt", 0.015);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber31.txt", 0.012);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber42.txt", 0.007);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber26.txt", 0.004);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber37.txt", 0.003);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber36.txt", 0.008);
//        fileAndScores.put("Results/ExtraSearches/Unique/subNumber32.txt", 0.002);
//
//
//        //go through and score all interactions
//        for (String file : fileAndScores.keySet()) {
//            BufferedReader br = null;
//            try {
//                br = new BufferedReader(new FileReader(new File(file)));
//                String line2;
//                double score = fileAndScores.get(file);
//                try {
//                    while ((line2 = br.readLine()) != null) {
//                        String interaction = line2.split("\t")[0] + "\t" + line2.split("\t")[1];
//                        if (scored.containsKey(interaction)) {
//                            double localScore = scored.get(interaction);
//                            localScore = localScore + score;
//                            scored.put(interaction, localScore);
//                        } else {
//                            scored.put(interaction, score);
//
//                        }
//                    }
//                } catch (IOException ex) {
//                    Logger.getLogger(Extra21Subs.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            } catch (FileNotFoundException ex) {
//                Logger.getLogger(Extra21Subs.class.getName()).log(Level.SEVERE, null, ex);
//            } finally {
//
//                try {
//                    br.close();
//                } catch (IOException ex) {
//                    Logger.getLogger(Extra21Subs.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//
//        }
//
//        int count = 0;
//        //print out the top 20
//        double topScore = 0.0;
//
//        Set<String> alreadyScored = new HashSet<String>();
//        String inter = "";
//        while (count < 20) {
//
//            for (String interaction : scored.keySet()) {
//                if (scored.get(interaction) == 54.061) {
//                    mapped.add(inter);
//                }
//                if (!alreadyScored.contains(interaction)) {
//                    if (scored.get(interaction) >= topScore && !interaction.equals(inter)) {
//                        topScore = scored.get(interaction);
//                        inter = interaction;
//                    }
//                }
//            }
//            System.out.println(inter + "\t" + topScore);
//            topScore = 0.0;
//            alreadyScored.add(inter);
//            count++;
//        }
//
//        for (String mapp : mapped) {
//            System.out.println(mapp);
//
//        }
//
//
//    }
//}
