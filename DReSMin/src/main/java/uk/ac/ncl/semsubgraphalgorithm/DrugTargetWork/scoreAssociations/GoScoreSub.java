/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.DrugTargetWork.scoreAssociations;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import uk.ac.ncl.semsubgraphalgorithm.DrugTargetWork.Pair;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Class takes a file of mappings from a sub search and scores the sub depending
 * on how many of the inferred associations are captured in the known
 * associations.
 *
 *
 */
public class GoScoreSub {

    private Set<Pair> knownAssociations;
    private Set<Pair> inferred;
    private File mappings;
    private String summary;
    private int verified;
    private int duplicates;
    private String subName;
    private String scriptLoc;

    public GoScoreSub(Set<Pair> knownAssociations, File mappings) {
        this.knownAssociations = knownAssociations;
        this.inferred = new HashSet<>();
        this.duplicates = 0;
        this.subName = mappings.getName().split("]")[0].substring(1);
        this.duplicates = 0;
        this.mappings = mappings;
    }

    /**
     * Scores the sub and returns details.
     */
    public void goScore() {

        int validated = 0;
        for (Pair p : inferred) {
            if (knownAssociations.contains(p)) {
                validated++;
            }
        }

        /**
         * Score the sub...
         */
        double subScore = (validated * 1.0) / inferred.size();

        /**
         * Get the size of all the relations inferred not just the NR ones
         */
        int nonRedundant = inferred.size() + duplicates;

        /**
         * Create summary
         */
        StringBuilder str = new StringBuilder();


        str.append(subName).append("\t").append(nonRedundant).append("\t").append(inferred.size()).append("\t").append(validated).append("\t").append(subScore);

        summary = str.toString();
    }

    /**
     * Extracts all associations from the .txt file provided.
     */
    public void goGetInferredAssociationsTXT() {
        GZIPInputStream gzis = null;
        boolean zip = false;
        if (mappings.getName().endsWith(".gz")) {
            zip = true;
        }

        if (zip) {
            try {
                gzis = new GZIPInputStream(new FileInputStream(mappings));
                BufferedReader br = new BufferedReader(new InputStreamReader(gzis));
                String thisLine;
                String fromName = null;
                String toName = null;
                Pair p = null;
                // Now read lines of text: the BufferedReader puts them in lines,
                // the InputStreamReader does Unicode conversion, and the
                // GZipInputStream "gunzip"s the data from the FileInputStream.
                try {
                    while ((thisLine = br.readLine()) != null) {
                        if (thisLine.startsWith("IN:")) {
                            fromName = thisLine.substring(3).split("\t")[0];
                            toName = thisLine.substring(3).split("\t")[1];
                            p = new Pair(fromName, toName);
                            if (!inferred.contains(p)) {
                                inferred.add(p);
                            } else {
                                duplicates++;
                            }
                        }
                    }
                } catch (IOException ex) {
                    Logger.getLogger(GoScoreSub.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (IOException ex) {
                Logger.getLogger(GoScoreSub.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    gzis.close();
                } catch (IOException ex) {
                    Logger.getLogger(GoScoreSub.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {

            BufferedReader br = null;

            try {
                br = new BufferedReader(new FileReader(mappings));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(GoScoreSub.class.getName()).log(Level.SEVERE, null, ex);
            }
            String thisLine;
            String fromName = null;
            String toName = null;
            Pair p = null;

            try {
                while ((thisLine = br.readLine()) != null) {
                    if (thisLine.startsWith("IN:")) {
                        fromName = thisLine.substring(3).split("\t")[0];
                        toName = thisLine.substring(3).split("\t")[1];
                        p = new Pair(fromName, toName);
                        if (!inferred.contains(p)) {
                            inferred.add(p);
                        } else {
                            duplicates++;
                        }
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(GoScoreSub.class.getName()).log(Level.SEVERE, null, ex);
            }

            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(GoScoreSub.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    public String getSummary() {
        return summary;
    }

    public void setKnownAssociations(Set<Pair> knownAssociations) {
        this.knownAssociations = knownAssociations;
    }
}
