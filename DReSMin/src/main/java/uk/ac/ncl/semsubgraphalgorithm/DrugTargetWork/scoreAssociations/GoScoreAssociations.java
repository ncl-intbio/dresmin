/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.DrugTargetWork.scoreAssociations;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import uk.ac.ncl.semsubgraphalgorithm.DrugTargetWork.Pair;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class GoScoreAssociations {

    private final static Logger LOGGER = Logger.getLogger(GoScoreAssociations.class.getName());
    private HashMap<String, Double> subScores;
    private HashMap<Pair, Double> associationScoresFALSE;
    private HashMap<Pair, Double> associationScoresTRUE;
    private String directory;
    private Map<String, String> DrugBank2NAMEsDATASET;
    private Map<String, String> Uniprot2NAMEsDATASET;
    private HashMap<Pair, Integer> numberOfSubsFALSE;
    private HashMap<Pair, Integer> numberOfSubsTRUE;

    public GoScoreAssociations(HashMap<String, Double> subScores, String directory, Map<String, String> DrugBank2NAMEsDATASET, Map<String, String> Uniprot2NAMEsDATASET) {
        this.subScores = subScores;
        this.associationScoresFALSE = new HashMap<>();
        this.directory = directory;
        this.DrugBank2NAMEsDATASET = DrugBank2NAMEsDATASET;
        this.Uniprot2NAMEsDATASET = Uniprot2NAMEsDATASET;
        this.numberOfSubsFALSE = new HashMap<>();
        this.associationScoresTRUE = new HashMap();
        this.numberOfSubsTRUE = new HashMap<>();
    }

    public void scoreAssociations() {

        double subScore = 0.0;
        File dir = new File(directory);
        File[] directoryListing = dir.listFiles();
        if (directoryListing != null) {
            for (File child : directoryListing) {
                if (!child.getName().startsWith(".")) {
                    String subName = child.getName().split("]")[0].substring(1);
                    subScore = subScores.get(subName);
                    //if (child.getName().endsWith(".txt")) {
                    checkSubAssociationsTXT(subScore, child);
                    //} else if (child.getName().endsWith(".gz")) {
                    //  checkSubAssociationsGZIP(subScore, child, chunkSize);
                    //}
                    LOGGER.log(Level.INFO, "Scoring all associations in {0}", child);
                }
            }
        } else {
            LOGGER.info("Errr nahhhh....");
        }
    }

    /**
     * Extracts all associations from the .txt file provided.
     */
    public void checkSubAssociationsTXT(double subScore, File child) {
        Map<Pair, Integer> inferred = new HashMap<>();
        GZIPInputStream gzis = null;
        boolean zip = false;
        if (child.getName().endsWith(".gz")) {
            zip = true;
        }

        if (zip) {
            try {
                gzis = new GZIPInputStream(new FileInputStream(child));
                BufferedReader br = new BufferedReader(new InputStreamReader(gzis));
                String thisLine;
                String fromName = null;
                String toName = null;
                Pair p = null;
                // Now read lines of text: the BufferedReader puts them in lines,
                // the InputStreamReader does Unicode conversion, and the
                // GZipInputStream "gunzip"s the data from the FileInputStream.
                try {
                    while ((thisLine = br.readLine()) != null) {
                        if (thisLine.startsWith("IN:")) {
                            fromName = thisLine.substring(3).split("\t")[0];
                            toName = thisLine.substring(3).split("\t")[1];
                            p = new Pair(fromName, toName);
                            if (!inferred.containsKey(p)) {
                                inferred.put(p, 1);
                            } else {
                                int count = inferred.get(p);
                                count += 1;
                                inferred.put(p, count);
                            }
                        }
                    }
                } catch (IOException ex) {
                    Logger.getLogger(GoScoreSub.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (IOException ex) {
                Logger.getLogger(GoScoreSub.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    gzis.close();
                } catch (IOException ex) {
                    Logger.getLogger(GoScoreSub.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {

            BufferedReader br = null;

            try {
                br = new BufferedReader(new FileReader(child));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(GoScoreSub.class.getName()).log(Level.SEVERE, null, ex);
            }
            String thisLine;
            String fromName = null;
            String toName = null;
            Pair p = null;

            try {
                while ((thisLine = br.readLine()) != null) {
                    if (thisLine.startsWith("IN:")) {
                        fromName = thisLine.substring(3).split("\t")[0];
                        toName = thisLine.substring(3).split("\t")[1];
                        p = new Pair(fromName, toName);
                        if (!inferred.containsKey(p)) {
                            inferred.put(p, 1);
                        } else {
                            int count = inferred.get(p);
                            count += 1;
                            inferred.put(p, count);
                        }
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(GoScoreSub.class.getName()).log(Level.SEVERE, null, ex);
            }

            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(GoScoreSub.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        for (Pair pa : inferred.keySet()) {
            int inferrredNum = 0;
            //count ass score
            if (associationScoresTRUE.containsKey(pa)) {
                double score = associationScoresTRUE.get(pa);
                score = score + (subScore * inferred.get(pa));
                associationScoresTRUE.put(pa, score);
            } else {
                associationScoresTRUE.put(pa, subScore * inferred.get(pa));
            }
            if (associationScoresFALSE.containsKey(pa)) {
                double score = associationScoresFALSE.get(pa);
                score = score + subScore;
                associationScoresFALSE.put(pa, score);
            } else {
                associationScoresFALSE.put(pa, subScore);
            }
            //check the number of subs inferring the association
            if (numberOfSubsFALSE.containsKey(pa)) {
                int numberofSubs = numberOfSubsFALSE.get(pa);
                numberOfSubsFALSE.put(pa, numberofSubs + 1);
            } else {
                numberOfSubsFALSE.put(pa, 1);
            }

            if (numberOfSubsTRUE.containsKey(pa)) {
                int numberofSubs = numberOfSubsTRUE.get(pa);
                numberOfSubsTRUE.put(pa, numberofSubs + inferred.get(pa));
            } else {
                numberOfSubsTRUE.put(pa, inferred.get(pa));
            }

        }

    }

    public void addToFile() {

        String opFileINSTANCE = "scored_Associations_TRUE.txt";
        String opFileFALSE = "scored_Associations_FALSE.txt";

        BufferedWriter bwTRUE = null;
        try {
            bwTRUE = new BufferedWriter(new FileWriter(opFileINSTANCE));
        } catch (IOException ex) {
            Logger.getLogger(GoScoreAssociations.class.getName()).log(Level.SEVERE, null, ex);
        }

        BufferedWriter bwFALSE = null;
        try {
            bwFALSE = new BufferedWriter(new FileWriter(opFileFALSE));
        } catch (IOException ex) {
            Logger.getLogger(GoScoreAssociations.class.getName()).log(Level.SEVERE, null, ex);
        }

        /**
         * Sort the scored associations
         */
        HashMap<Pair, Double> sorted = (HashMap<Pair, Double>) sortByValue(associationScoresFALSE);
        /**
         * Add them to file with the score first
         */
        for (Pair sor : sorted.keySet()) {
            try {
                bwFALSE.append(sorted.get(sor) + "\t" + DrugBank2NAMEsDATASET.get(sor.getDrug()) + "\t" + Uniprot2NAMEsDATASET.get(sor.getTarget()) + "\t" + numberOfSubsFALSE.get(sor) +"\n");
                
            } catch (IOException ex) {
                Logger.getLogger(GoScoreAssociations.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try {
            bwFALSE.close();
        } catch (IOException ex) {
            Logger.getLogger(GoScoreAssociations.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.INFO, "Added all ranked associations INSTANCE FALSE to {0}", opFileFALSE);
        LOGGER.log(Level.INFO, "Ranked {0} unique associations to ", associationScoresFALSE.size());

        /**
         * Sort the scored associations
         */
        sorted = (HashMap<Pair, Double>) sortByValue(associationScoresTRUE);
        /**
         * Add them to file with the score first
         */
        for (Pair sor : sorted.keySet()) {
            try {
                bwTRUE.append(sorted.get(sor) + "\t" + DrugBank2NAMEsDATASET.get(sor.getDrug()) + "\t" + Uniprot2NAMEsDATASET.get(sor.getTarget()) + "\t" + numberOfSubsTRUE.get(sor) + "\n");
                // System.out.println(sorted.get(sor)+"\t"+ sor.getDrug()+"\t"+ sor.getTarget());
            } catch (IOException ex) {
                Logger.getLogger(GoScoreAssociations.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try {
            bwTRUE.close();
        } catch (IOException ex) {
            Logger.getLogger(GoScoreAssociations.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.INFO, "Added all ranked associations INSTANCE TRUE to {0}", opFileFALSE);
        LOGGER.log(Level.INFO, "Ranked {0} unique associations to ", associationScoresTRUE.size());

    }

    /**
     * Allows for a hashmap to be ordered on the value. Used when we order the
     * datasets by their LLS score.
     *
     * @param <K>
     * @param <V>
     * @param map
     * @return
     */
    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list =
                new LinkedList<>(map.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
            @Override
            public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });

        Map<K, V> result = new LinkedHashMap<>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }
}
