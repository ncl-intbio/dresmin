/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.management;

import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.SourceGraphNEW;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author joemullen
 */
public class SerialisedGraph implements java.io.Serializable {

    private static final long serialVersionUID = 33326404614661473L;
    private String summary = "";
    private long startTime;
    private long endTime;
    private OutPutManagement op;

    public SerialisedGraph() throws IOException {
        super();
        this.op = new OutPutManagement();
    }

    public static void main(String[] args) throws IOException, Exception {
        OutPutManagement op = new OutPutManagement();    
        try {
            //SourceGraphNEW s = new SourceGraphNEW(new File(op.getGraphFolder()+"con_listOP_55e0c.tsv"), new File(op.getGraphFolder()+"rel_listOP_55e0c.tsv"));
            //SourceGraphNEW p = new SourceGraphNEW(new File("/Users/joemullen/Desktop/OP_tab_exporter/Small Dataset/con_listOP_32bad.tsv"), new File("/Users/joemullen/Desktop/OP_tab_exporter/Small Dataset/rel_listOP_32bad.tsv"));
            SerialisedGraph sg = new SerialisedGraph();
            //sg.SerializeGraphNEW(s, "graphNEW");
            SourceGraphNEW p = sg.useSerializedNEW("/Users/joemullen/Desktop/SemSubNEW/DReSMin/Graph/graphNEW.data");
            System.out.println(p.getGraphNodeSize());
            //System.out.println(p.getRelationTypeRules());

        } catch (FileNotFoundException fe) {
            System.out.println(fe);
        }

    }

    public void SerializeGraphNEW(SourceGraphNEW s, String fileName) throws FileNotFoundException, IOException {
        startTime = System.currentTimeMillis();
        try (FileOutputStream f_out = new FileOutputStream(op.getGraphFolder() + fileName + ".data")) {
            ObjectOutputStream obj_out = new ObjectOutputStream(f_out);
            obj_out.writeObject(s);
            obj_out.close();
        }

        endTime = System.currentTimeMillis();

        System.out.println("[INFO] Serialized graph is saved as..." + op.getGraphFolder() + fileName);

    }

    public SourceGraphNEW useSerializedNEW(String file) throws FileNotFoundException, IOException, ClassNotFoundException {
        SourceGraphNEW s = null;
        startTime = System.currentTimeMillis();
        try {
            FileInputStream f_in = new FileInputStream( file);
            ObjectInputStream obj_in = new ObjectInputStream(f_in);
            s = (SourceGraphNEW) obj_in.readObject();
            f_in.close();
            obj_in.close();
            endTime = System.currentTimeMillis();
            return s;

        } catch (IOException i) {
            i.printStackTrace();
        }
        return s;
    }

    public String getSummary() {
        return "[INFO] Serialized Graph Parsed in: " + (endTime - startTime) / 1000 + " seconds";
    }
}
