/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.SemSearch;

/**
 * @author joemullen
 */
import uk.ac.ncl.semsubgraphalgorithm.management.OutPutManagement;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.SourceGraphNEW;
import uk.ac.ncl.semsubgraphalgorithm.queryGraphs.QueryGraph;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedNode;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedNodeUID;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.UUID;
import java.util.logging.Logger;
import uk.ac.ncl.semsubgraphalgorithm.semanticDistanceSubgraphExclusion.SemanticDistanceCalculator;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import static uk.ac.ncl.semsubgraphalgorithm.SemSearch.SearchManager.printProgBar;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.GraphMethodsNEW;

/**
 * Class takes an initial node pair and returns the mappings for that node pair.
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class SemSearch extends GraphMethodsNEW {

    private DataStructureSub subTree;
    private DataStructureMapping mapTree;
    private double threshold;
    private SemanticDistanceCalculator sdc;
    private boolean useSDC;
    public String summary = "";
    private int subGraphSize;
    private int tarGraphSize;
    private TypedNode[] sub_core;
    private TypedNode[] tar_core;
    private TypedNode[] sub_order;
    private TypedNodeUID[] UID_core;
    private int subOrderCount = 0;
    private TypedNode[] sub_out;
    private TypedNode[] sub_in;
    private TypedNodeUID[] tar_out;
    private TypedNodeUID[] tar_in;
    private int allMatchesExclSmallWorldAss = 0;
    private HashMap<TypedNodeUID, TypedNode> potentialNodesPairsP;
    //stores all matches
    private List<TypedNode[]> allMatches;
    private TypedNodeUID rootNodeUID = null;
    private boolean closedWorld;
    private boolean noElementsBelowST;
    private OutPutManagement op;
    private final static Logger LOGGER = Logger.getLogger(SemSearch.class.getName());
    private String visDir;
    private SourceGraphNEW source;
    private QueryGraph query;
    private HashMap<TypedNode, Set<TypedNode>> initialNodes;
    private AdditionCheck add;
    private String nodeDistance;
    private String edgeDistance;

    public String getSummary() {
        return summary;
    }

    /**
     * Class performs a search
     *
     * @param s
     * @param q
     * @param initialNodes
     * @param semDepth
     * @param startClass
     * @param SDC
     * @param threshold
     * @param closedWorld
     * @param outPut
     * @param noElementsBelowST
     * @throws IOException
     */
    public SemSearch(SourceGraphNEW s, QueryGraph q, HashMap<TypedNode, Set<TypedNode>> initialNodes, String startClass, AdditionCheck add, boolean SDC, double threshold, boolean closedWorld, boolean outPut, boolean noElementsBelowST, String nodeDistance, String edgeDistance) {

        this.source = s;
        this.query = q;
        this.add = add;
        this.initialNodes = initialNodes;
        this.subTree = null;
        this.closedWorld = closedWorld;
        this.threshold = threshold;
        this.useSDC = SDC;
        this.subGraphSize = s.getGraphSize(query.getSub());
        this.tarGraphSize = s.getGraphSize(source.getSourceGraph());
        this.noElementsBelowST = noElementsBelowST;
        this.sub_core = new TypedNode[subGraphSize];
        this.tar_core = new TypedNode[subGraphSize];
        this.sub_out = new TypedNode[subGraphSize];
        this.sub_in = new TypedNode[subGraphSize];
        this.tar_out = new TypedNodeUID[tarGraphSize];
        this.tar_in = new TypedNodeUID[tarGraphSize];
        this.potentialNodesPairsP = new HashMap<>();
        this.allMatches = new ArrayList<>();
        this.op = new OutPutManagement();
        this.visDir = op.getSemVisFolder();
        this.edgeDistance = edgeDistance;
        this.nodeDistance = nodeDistance;
        if (useSDC == true) {
            this.sdc = new SemanticDistanceCalculator(this.nodeDistance, this.edgeDistance);
        }
    }

    /**
     * Performs a semantic search using parameters passed to the constructor
     *
     * @throws IOException
     */
    public void completeSearch() throws IOException {
        try {
            runSearch(initialNodes);
        } catch (Exception ex) {
            Logger.getLogger(SemSearch.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Method performs a search for each initial pair identified
     *
     * @param initialPairs
     * @throws Exception
     */
    public void runSearch(HashMap<TypedNode, Set<TypedNode>> initialPairs) throws Exception {

        resetDataStructures();
        double count = 1.0;
       // System.out.println("INITIAL NODES:: " + count);
        for (TypedNode node : initialPairs.keySet()) {
            TypedNode sub = node;
            for (TypedNode tarTN : initialNodes.get(node)) {
               // System.out.println(node.getName() + "\t" + count);
                long checktime = System.currentTimeMillis();

                TypedNodeUID tar = new TypedNodeUID(tarTN, UUID.randomUUID().toString());
                subTree = new DataStructureSub(sub);
                mapTree = new DataStructureMapping(tar);
                resetDataStructures();

                HashMap<TypedNodeUID, TypedNode> resultsSTATE = getNextNodesPair(tar, sub, 1);
                getNextNodesMap(resultsSTATE, 1);
                
                if (count % 100 == 0.0) {
                    gc();
                }
                printProgBar(((count/ initialNodes.get(node).size()) *100), "SP_SRCH");
                count++;     
            }
        }
    }

    /**
     * Method performs a search for each initial pair identified
     *
     * @param initialPairs
     * @throws Exception
     */
    public void runSearchSinglePair(TypedNode sub, TypedNode tar) throws Exception {

        resetDataStructures();
        allMatches = new ArrayList<TypedNode[]>();
        int count = 0;
        TypedNodeUID tarUID = new TypedNodeUID(tar, UUID.randomUUID().toString());
        subTree = new DataStructureSub(sub);
        mapTree = new DataStructureMapping(tarUID);
        resetDataStructures();
        HashMap<TypedNodeUID, TypedNode> resultsSTATE = getNextNodesPair(tarUID, sub, 1);
        getNextNodesMap(resultsSTATE, 1);
        count++;

    }

    /**
     * Returns the next nodes to be considered during the search
     *
     * @param otherNodes
     * @return
     * @throws Exception
     *
     * <CHECKED>
     */
    public void getNextNodesMap(HashMap<TypedNodeUID, TypedNode> otherNodes, int state) throws Exception {
        HashMap<TypedNodeUID, TypedNode> newAdditions = new HashMap<>();
        for (TypedNodeUID tar : otherNodes.keySet()) {
            TypedNode sub = otherNodes.get(tar);
            if (sub != null) {
                newAdditions.putAll(getNextNodesPair(tar, sub, state));
            }
        }
        state++;
        //System.out.println("New addditions:: "+ newAdditions.size()+" STATE: "+ state);
        if (state < subGraphSize
                && newAdditions.size() > 1) {
            getNextNodesMap(newAdditions, state);
        }
    }

    /**
     * Method returns all possible nodes that may be included in a match in the
     * next state of the search, if the search has reached the final state it
     * returns all matches
     *
     * @param subNode
     * @param tarNode
     * @return
     * @throws Exception
     */
    public HashMap<TypedNodeUID, TypedNode> getNextNodesPair(TypedNodeUID tarNode, TypedNode subNode, int depth) throws Exception {

        resetDataStructures();
        UID_core = mapTree.popUIDCore(tarNode);
        sub_core = subTree.popSubCore(subNode);
        tar_core = mapTree.removeUIDs((UID_core));

        HashMap<TypedNodeUID, TypedNode> allNodesAdded = new HashMap<>();
        populateArrays(sub_core, tar_core);
        calculateP();

        for (TypedNodeUID target : potentialNodesPairsP.keySet()) {

            if (target == null) {
                break;
            } else {
                TypedNode sub = potentialNodesPairsP.get(target);
                TypedNode tar = target.getTypedNode();
                int count = 0;
                //can we add this node tot he mapping??
                if (add.addToMapping(sub_core.length + 1, sub_core, tar_core, tar, sub, closedWorld, subOrderCount, useSDC, noElementsBelowST, threshold)) {
                    allNodesAdded.put(target, sub);
                    mapTree.addNode(target, UID_core);
                    subTree.addNode(sub, sub_core);
                    TypedNode[] mapping = tar_core;
                    //  System.out.println(sub_core.length+" compared to "+ subGraphSize);

                    if ((sub_core.length) == (subGraphSize - 1)) {
                        mapping = append(tar_core, tar);
                        if (subOrderCount == 0) {
                            sub_order = append(sub_core, sub);
                            setSubOrder();
                        }

                        if (closedWorldCHECK(mapping, sub_order, source.getSourceGraph(), query.getSub(), closedWorld, subOrderCount) == true && !allMatches.contains(mapping) && (scoreMatchBooleanCHECK(query,
                                source,
                                sub_order,
                                mapping,
                                sdc,
                                sdc.getAllConceptClasses(),
                                useSDC,
                                noElementsBelowST,
                                threshold,
                                subOrderCount) == true)) {
                            {

                                if (!allMatches.contains(mapping)) {
                                    allMatches.add(mapping);
                                }

                                //System.out.println(allMatches.size());
                            }

                        } else {
                            //System.out.println("-----" + allMatchesExclSmallWorldAss);
                            allMatchesExclSmallWorldAss++;
                        }
                    }
                    count++;

                }
            }
        }
        return allNodesAdded;
    }

    /**
     * Populates the arrays that contain information that is used to see whether
     * or not a node passes the rules and may be considered for the next state
     * of the search
     *
     * @param subnode
     * @param tarnode
     */
    public void populateArrays(TypedNode[] subnode, TypedNode[] tarnode) {

        TypedNode[] tarOut1;
        tarOut1 = tOut(source.getSourceGraph(), tarnode);

        for (int i = 0; i < tarOut1.length; i++) {
            tar_out[i] = new TypedNodeUID(tarOut1[i], UUID.randomUUID().toString());
        }

        TypedNode[] tarIn1;
        tarIn1 = tIn(source.getSourceGraph(), tarnode);
        for (int i = 0; i < tarIn1.length; i++) {
            tar_in[i] = new TypedNodeUID(tarIn1[i], UUID.randomUUID().toString());
        }

        TypedNode[] subOut1;
        subOut1 = tOut(query.getSub(), subnode);
        for (int i = 0; i < subOut1.length; i++) {
            sub_out[i] = subOut1[i];
        }

        TypedNode[] subIn1;
        subIn1 = tIn(query.getSub(), subnode);
        for (int i = 0; i < subIn1.length; i++) {
            sub_in[i] = subIn1[i];
        }
    }

    /**
     * Calculates which set of nodes will be used for the next state of the
     * search
     *
     * @return
     *
     * <CHECKED>
     */
    public void calculateP() {
        TypedNode mostConnectednode = null;
        int highest = 0;

        if (tar_out[0] != null && sub_out[0] != null) {
            for (int y = 0; y < sub_out.length; y++) {
                TypedNode local = sub_out[y];
                if (sub_out[y] != null) {
                    int[] local2 = add.getSubMap().get(local);
                    if ((local2[0] >= highest)
                            && (repeatabiltyCHECK(local, sub_core, getState()))) {
                        highest = local2[0];
                        mostConnectednode = local;

                    }
                }
            }

            for (int i = 0; i < tar_out.length; i++) {
                if (tar_out[i] != null) {
                    potentialNodesPairsP.put(tar_out[i], mostConnectednode);
                }
            }
        }

        if (mostConnectednode == null && tar_in[0] != null
                && sub_in[0] != null) {
            int highestIn = 0;
            for (int y = 0; y < sub_in.length; y++) {
                TypedNode local = sub_in[y];
                if (sub_in[y] != null) {
                    int[] local2 = add.getSubMap().get(local);
                    if ((local2[0] >= highestIn)
                            && (repeatabiltyCHECK(local, sub_core, getState()))) {
                        highestIn = local2[0];
                        mostConnectednode = local;
                    }
                }
            }

            for (int i = 0; i < tar_in.length; i++) {
                if (tar_in[i] != null) {
                    potentialNodesPairsP.put(tar_in[i], mostConnectednode);
                }
            }

        }
    }

    /**
     * Check to see if the graph we are searching for is a complete match of the
     * target graph
     *
     * @return
     */
    public boolean completeMatch() {
        boolean completeMatch = false;

        if (source.getGraphSize(source.getSourceGraph()) == source.getGraphSize(query.getSub())) {
            completeMatch = true;
        }
        return completeMatch;

    }

    /**
     * Resets the data structures prior to a new search or a new state
     */
    public void resetDataStructures() {
        //System.out.println("resetting data structures");
        this.sub_out = new TypedNode[subGraphSize];
        this.sub_in = new TypedNode[subGraphSize];
        this.tar_out = new TypedNodeUID[tarGraphSize];
        this.tar_in = new TypedNodeUID[tarGraphSize];
        this.potentialNodesPairsP = new HashMap<>();

    }

    /**
     * Returns the current state of the search for that particular mapping
     *
     * @return
     *
     * <CHECKED>
     */
    public int getState() {

        int county = 0;
        for (int y = 0; y < sub_core.length; y++) {
            if (sub_core[y] == null) {
                county++;
            }

        }
        int myPredictedState = (sub_core.length - county);
        return myPredictedState;
    }

    /**
     * Sorts the nodes of match in ascending order using their id's
     *
     * @param match1
     * @return
     * <CHECK>
     */
    public String orderMatch(TypedNode[] match1) {

        ArrayList<Integer> temp = new ArrayList<Integer>();
        for (int y = 0; y < match1.length; y++) {
            temp.add(match1[y].getId());
        }
        Collections.sort(temp);
        return temp.toString();
    }

    /**
     * Get the number of matches that were identified by the search
     *
     * @return
     */
    public int numberOfMathces() {

        return allMatches.size();
    }

    /**
     * Set the order in which nodes from the subgraph will be searched for
     *
     * @param order
     */
    public void setSubOrder() {
        this.subOrderCount = 1;
    }

    /**
     * Return the order in which the subgraph nodes were mapped
     *
     * @return
     */
    public TypedNode[] getSubOrder() {
        return sub_order;
    }

    /**
     * Return all the matches from the search.
     *
     * @return
     */
    public List<TypedNode[]> getallMatches() {
        return allMatches;
    }

    /**
     * Progress bar.
     *
     * @param percent
     */
   public static void printProgBar(double percent, String header) {
        StringBuilder bar = new StringBuilder(header + "[");

        for (int i = 0; i < 50.0; i++) {
            if (i < (percent / 2)) {
                bar.append("=");
            } else if (i == (percent / 2)) {
                bar.append(">");
            } else {
                bar.append(" ");
            }
        }

        bar.append("]   ").append(percent).append("%     ");
        System.out.print("\r" + bar.toString());
    }

    /**
     * Append an array.
     *
     * @param <TypedNode>
     * @param arr
     * @param element
     * @return
     */
    static <TypedNode> TypedNode[] append(TypedNode[] arr, TypedNode element) {

        final int N = arr.length;
        arr = Arrays.copyOf(arr, N + 1);
        arr[N] = element;
        return arr;

    }

    public static void gc() {
        Object obj = new Object();
        WeakReference ref = new WeakReference<>(obj);
        obj = null;
        while (ref.get() != null) {
            System.gc();
        }
    }
}
