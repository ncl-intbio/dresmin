/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.SemSearch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.semsubgraphalgorithm.queryGraphs.QueryGraph;
import uk.ac.ncl.semsubgraphalgorithm.semanticDistanceSubgraphExclusion.SemanticDistanceCalculator;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.GraphMethodsNEW;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.SourceGraphNEW;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedNode;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Class checks to see if nodes provided can be added to a potential mapping
 * during a semantic search.
 */
public class AdditionCheck extends GraphMethodsNEW {

    private HashMap<TypedNode, int[]> subMap;
    private HashMap<TypedNode, int[]> targeyMap;
    private HashMap<TypedNode, int[]> origisubMap;
    private SourceGraphNEW source;
    private QueryGraph query;
    private QueryGraph originalSub;
    private SemanticDistanceCalculator sdc;

    public HashMap<TypedNode, int[]> getSubMap() {
        return subMap;
    }

    public void setSubMap(HashMap<TypedNode, int[]> subMap) {
        this.subMap = subMap;
    }

    public HashMap<TypedNode, int[]> getTargeyMap() {
        return targeyMap;
    }

    public void setTargeyMap(HashMap<TypedNode, int[]> targeyMap) {
        this.targeyMap = targeyMap;
    }

    public HashMap<TypedNode, int[]> getOrigisubMap() {
        return origisubMap;
    }

    public void setOrigisubMap(HashMap<TypedNode, int[]> origisubMap) {
        this.origisubMap = origisubMap;
    }

    public AdditionCheck(SourceGraphNEW sourceg, QueryGraph sub, QueryGraph origi, SemanticDistanceCalculator sdc) {
        this.source = sourceg;
        this.originalSub = origi;
        this.query = sub;
        this.targeyMap = nameEdgeset(source.getSourceGraph());
        if (originalSub != null) {
            this.origisubMap = nameEdgeset(originalSub.getSub());
        }
        this.subMap = nameEdgeset(sub.getSub());
        this.sdc = sdc;
    }

    public boolean addToMapping(int state, TypedNode[] sub_core, TypedNode[] tar_core, TypedNode tar, TypedNode sub, boolean closedWorld, int subOrderCount, boolean useSDC, boolean noElementsBelowST, double threshold) {
      
        try {
            if ((feas2(sub, tar, state))
                    && (repeatabiltyCHECK(tar, tar_core, state))
                    && (repeatabiltyCHECK(
                    sub, sub_core, state)
                    && (feas1(sub, tar, sub_core, tar_core, state))
                    && (closedWorldCHECK(tar_core, sub_core, source.getSourceGraph(), query.getSub(), closedWorld, subOrderCount))
                    && (scoreMatchBooleanCHECK(query,
                    source,
                    sub_core,
                    tar_core,
                    sdc,
                    sdc.getAllConceptClasses(),
                    useSDC,
                    noElementsBelowST,
                    threshold,
                    state)))) {
                  //System.out.println("comparing: "+ sub.toString() +" abd "+ tar.toString()+"  TRUUUUUUUEEE");
                return true;
            } else {
                //System.out.println("comparing: "+ sub.toString() +" abd "+ tar.toString()+"  FALLLLLSSSSSEE");
                return false;
            }
        } catch (Exception ex) {
            Logger.getLogger(AdditionCheck.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * Checks to see if a node passes the first feasibility rule
     *
     * @param subNode
     * @param tarNode
     * @return
     *
     * <CHECKED>
     */
    public boolean feas1(TypedNode subNode, TypedNode tarNode, TypedNode[] sub_core, TypedNode[] tar_core, int State) {

        boolean well = false;

        if (State == 0) {
            return true;
        } else {
            if ((rPred(subNode, tarNode, sub_core, tar_core) == true)
                    && (rSucc(subNode, tarNode, sub_core, tar_core) == true)) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Check if the pairs match in the same way to the previous nodes of the
     * partial mapping
     *
     * @param sub
     * @param tar
     * @return
     *
     * <CHECKED>
     */
    public boolean rPred(TypedNode sub, TypedNode tar, TypedNode[] sub_core, TypedNode[] tar_core) {

        boolean well = true;
        TypedNode[] inComingNodes = source.tIn(query.getSub(), sub);
        if (inComingNodes.length > 0) {
            ArrayList<Integer> location_in_mapping = new ArrayList<>();
            for (TypedNode node : inComingNodes) {
                for (int i = 0; i < sub_core.length; i++) {
                    if (sub_core[i] == node) {
                        location_in_mapping.add(i);
                    }
                }
            }
            for (int look : location_in_mapping) {
                if (!source.getSourceGraph().containsEdge(tar_core[look], tar)) {  
                    return false;
                }
            }
        }
        return well;
    }

    /**
     * Check if the pairs match in the same way to the next nodes of the partial
     * mapping
     *
     * @param sub
     * @param tar
     * @return
     */
    public boolean rSucc(TypedNode sub, TypedNode tar, TypedNode[] sub_core, TypedNode[] tar_core) {
        boolean well = true;
        TypedNode[] outGoingNodes = source.tOut(query.getSub(), sub);

        if (outGoingNodes.length > 0) {
            ArrayList<Integer> location_in_mapping = new ArrayList<>();
            for (TypedNode node : outGoingNodes) {
                for (int i = 0; i < sub_core.length; i++) {
                    if (sub_core[i] == node) {
                        location_in_mapping.add(i);
                    }

                }
            }
            for (int look : location_in_mapping) {
                if (!source.getSourceGraph().containsEdge(tar, tar_core[look])) {
                    return false;
                }
            }
        }
        return well;
    }

    /**
     * Checks to see if a potential addition passes feasibility rule 2
     *
     * @param subNode
     * @param tarNode
     * @return
     *
     * <CHECKED>
     */
    public boolean feas2(TypedNode subNode, TypedNode tarNode, int state) {
        if (state == 0) {
            return true;
        } else {
            int[] subEdgeset = subMap.get(subNode);
            int[] tarEdgeset = targeyMap.get(tarNode);
            return (rTermin(subEdgeset, tarEdgeset));
        }
    }

    /**
     * Check the node in the mapping has the same edgeset as its equivalent in
     * the subgraph
     *
     * @param sub
     * @param tar
     * @return
     */
    public boolean rTermin(int[] sub, int[] tar) {
        boolean well = true;
        for (int p = 0; p < sub.length; p++) {
            if (tar[p] < sub[p]) {
                return false;
            }
        }
        return well;
    }
}
