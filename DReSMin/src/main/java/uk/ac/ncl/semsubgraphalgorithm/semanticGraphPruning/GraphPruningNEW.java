/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.semanticGraphPruning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;
import uk.ac.ncl.semsubgraphalgorithm.queryGraphs.QueryGraph;
import uk.ac.ncl.semsubgraphalgorithm.management.SerialisedGraph;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.SourceGraphNEW;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedEdge;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedNode;
import uk.ac.ncl.semsubgraphalgorithm.semanticDistanceSubgraphExclusion.SemanticDistanceCalculator;

/**
 *
 * @author joemullen
 */
public class GraphPruningNEW {

    private QueryGraph q;
    private SourceGraphNEW s;
    private SemanticDistanceCalculator sdc;
    private DirectedGraph<TypedNode, TypedEdge> target;
    private DirectedGraph<TypedNode, TypedEdge> newGraph;
    private DirectedGraph<TypedNode, TypedEdge> query;
    private Set<String> ConceptClassesToInclude = new HashSet<String>();
    private float threshold;
    //counts for testing
    private int deletedNodes = 0;
    private int deletedEdges = 0;
    private String summary;
    private long startTime;
    private long endTime;
    private final static Logger LOGGER = Logger.getLogger(GraphPruningNEW.class.getName());

    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {
        SerialisedGraph ser = new SerialisedGraph();
        SourceGraphNEW source = ser.useSerializedNEW("graphNEW.data");

        //RandomGraph ran = new RandomGraph(source, 1000, 0.5);
        QueryGraph query = new QueryGraph(QueryGraph.SemanticSub.CTT);

        GraphPruningNEW gp = new GraphPruningNEW(source, query, 0.8);
    }

    // do we need to update the Semantics?? Think not, would just take time.
    public GraphPruningNEW(SourceGraphNEW graph, QueryGraph q, double th) throws IOException, Exception {

        this.q = q;
        this.query = q.getSub();
        this.sdc = new SemanticDistanceCalculator(null, null);
        this.threshold = (float) th;
        this.s = graph;
        this.target = s.getSourceGraph();
        startTime = System.currentTimeMillis();
        getConceptClasses();
        Prune();
        checkAnyLoneNodes();
        endTime = System.currentTimeMillis();
        summary = summary + "Pruning Took " + (endTime - startTime) / 1000 + " seconds)";
        System.out.println(getSummary());

    }

    private void getConceptClasses() throws Exception {
        Set<TypedNode> allNodes = query.vertexSet();
        Set<String> alloriginal = sdc.getAllConceptClasses();
        //for every concept class in the subgraph
        for (TypedNode n : allNodes) {
            String type = n.getType();
            //score against every conceptclass in the graph
            //if greater than the threshold then include cc in graph
            for (String cc : alloriginal) {
                if (sdc.getNodeDistance(type, cc) >= threshold) {
                    ConceptClassesToInclude.add(cc);
                }
            }
        }
        LOGGER.info("Concept classes to be included: " + ConceptClassesToInclude.toString());
    }

    private void Prune() {
        newGraph = new DefaultDirectedGraph<TypedNode, TypedEdge>(
                TypedEdge.class);
        Set<TypedNode> allnodes = target.vertexSet();

        //add nodes to the new graph

        for (TypedNode t : allnodes) {
            String type = t.getType();
            //if the conceptclass of the node is not to be included then we delete the node
            if (ConceptClassesToInclude.contains(type)) {
                newGraph.addVertex(t);
            }
        }


        //think there is a problem here
        Set<TypedNode> newNodes = newGraph.vertexSet();

        for (TypedNode t : newNodes) {
            Set<TypedEdge> allEdges = target.edgesOf(t);
            for (TypedEdge e : allEdges) {
                if (newNodes.contains(e.getV1()) && newNodes.contains(e.getV2())) {
                    newGraph.addEdge(e.getV1(), e.getV2(), e);
                }
            }
        }

        LOGGER.log(Level.INFO, "(Semantic Pruning) Deleted: {0}/{1} (nodes/edges),  New graph size : {2}/{3} (nodes/edges) ", new Object[]{target.vertexSet().size() - newGraph.vertexSet().size(), target.edgeSet().size() - newGraph.edgeSet().size(), newGraph.vertexSet().size(), newGraph.edgeSet().size()});

    }

    private void checkAnyLoneNodes() {

        Set<TypedNode> allnodes = newGraph.vertexSet();
        Set<TypedNode> toDelete = new HashSet<TypedNode>();
        int deleted = 0;
        for (TypedNode n : allnodes) {
            if (newGraph.edgesOf(n).size() == 0) {
                toDelete.add(n);
                deleted++;
            }
        }
        
        for(TypedNode t: toDelete){
            newGraph.removeVertex(t);  
        }
        LOGGER.log(Level.INFO, " (No Relations) Deleted: {0} (nodes), New graph size : {1}/{2} (nodes/edges)", new Object[]{deleted, newGraph.vertexSet().size(), newGraph.edgeSet().size()});
    }

    public DirectedGraph<TypedNode, TypedEdge> getPrunedGraph() throws IOException {
        return newGraph;
        //return new SourceGraph(target2, newsource.getAllCOnceptInfo());
    }

    public SourceGraphNEW getPrunedSource() throws IOException {
        //SourceGraph sg = (SourceGraph) s.clone();
        if (threshold == 0.0) {
            return s;
        } else {
            SourceGraphNEW sg = new SourceGraphNEW(newGraph);
            return sg;
        }
        //return new SourceGraph(target2, newsource.getAllCOnceptInfo());
    }

    public String getSummary() {
        return summary;

    }

    public String getTime() {
        String time = (" " + (double) (endTime - startTime) / 1000);
        return time.trim();
    }
}
