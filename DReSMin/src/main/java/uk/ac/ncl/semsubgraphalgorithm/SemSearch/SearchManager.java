/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.SemSearch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.semsubgraphalgorithm.DrugTargetWork.SearchShortestPaths;
import static uk.ac.ncl.semsubgraphalgorithm.SemSearch.SemSearch.gc;
import uk.ac.ncl.semsubgraphalgorithm.graphSplitting.GraphSplitInstance;
import uk.ac.ncl.semsubgraphalgorithm.graphSplitting.RunSplit;
import uk.ac.ncl.semsubgraphalgorithm.graphSplitting.splitDataStructure.DataStructureSplit;
import uk.ac.ncl.semsubgraphalgorithm.graphSplitting.splitDataStructure.SplitNode;
import uk.ac.ncl.semsubgraphalgorithm.management.OutPutManagement;
import uk.ac.ncl.semsubgraphalgorithm.management.SerialisedGraph;
import uk.ac.ncl.semsubgraphalgorithm.queryGraphs.QueryGraph;
import uk.ac.ncl.semsubgraphalgorithm.semanticDistanceSubgraphExclusion.SemanticDistanceCalculator;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.GraphMethodsNEW;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.SourceGraphNEW;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedEdge;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedNode;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class SearchManager extends GraphMethodsNEW {

    //if we want to write the output to file?
    private boolean writeToFile;
    //source graph for the search
    private SourceGraphNEW source;
    //queryGraph for the search
    private QueryGraph query;
    // If we are running a split search
    private QueryGraph originalSub;
    //inferred edge of a sub
    private TypedEdge inferredEdge;
    //most connected node info
    private TypedNode mostConnectedTopNode;
    private TypedNode mostConnectedSemNode;
    //initial nodes to be used during a search
    private HashMap<TypedNode, Set<TypedNode>> initialNodes;
    //initial nodes to be used during a split search when it is one node at a time
    private HashMap<TypedNode, TypedNode> initialNodesSplit;
    //addition check
    private AdditionCheck add;
    private FileWriter fstream;
    private BufferedWriter out;
    private long startTime2;
    private long endTime2;
    private String startingClass;
    private OutPutManagement op;
    private double threshold;
    private boolean SDC;
    private boolean closedWorld;
    private boolean outPut;
    private boolean noElementsBelow;
    private int numberOfMatches = 1;
    private SemanticDistanceCalculator sdc;
    private TypedNode[] sub_order;
    private final static Logger LOGGER = Logger.getLogger(SearchManager.class.getName());
    private String edgeDistance;
    private String nodeDistance;
    private boolean outputAllSubDeets;
    private int splitMax;

    public static void main(String[] args) {

        OutPutManagement op = new OutPutManagement();
        QueryGraph q = new QueryGraph(QueryGraph.SemanticSub.TWONODE);

        try {
            SerialisedGraph sg = new SerialisedGraph();
            SourceGraphNEW source = null;
            try {
                //source = sg.useSerializedNEW(args[0]);
                source = sg.useSerializedNEW(op.getGraphFolder() + "graphNEW.data");
            } catch (FileNotFoundException ex) {
                Logger.getLogger(SearchShortestPaths.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(SearchShortestPaths.class.getName()).log(Level.SEVERE, null, ex);
            }
            //SearchShortestPaths search = new SearchShortestPaths(source, args[1], args[4], args[2], args[3], Integer.parseInt(args[5]), Double.parseDouble(args[6]));
            SearchManager search = new SearchManager(source, q, null, "Publication", true, 1.0, true, true, true, "/Users/joemullen/Desktop/", "SDC/node_distance.txt", "SDC/edge_distance.txt", 2);
            search.run();
        } catch (IOException ex) {
            Logger.getLogger(SearchShortestPaths.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public SearchManager(SourceGraphNEW s, QueryGraph q, HashMap<TypedNode, Set<TypedNode>> initialNodes, String startClass, boolean SDC, double threshold, boolean closedWorld, boolean outPut, boolean noElementsBelowST, String opDir, String nodeDistance, String edgeDistance, int splitMax) {
        this.outPut = outPut;
        this.nodeDistance = nodeDistance;
        this.edgeDistance = edgeDistance;
        this.closedWorld = closedWorld;
        this.noElementsBelow = noElementsBelowST;
        this.SDC = SDC;
        this.writeToFile = outPut;
        this.source = s;
        this.query = q;
        this.inferredEdge = q.getInferredEdge();
        this.startingClass = startClass;
        this.threshold = threshold;
        this.splitMax = splitMax;
        this.sdc = new SemanticDistanceCalculator(nodeDistance, edgeDistance);
        this.op = new OutPutManagement();
        if (outPut) {
            try {
                this.fstream = new FileWriter(new File(opDir + "[" + q.getSubgraphName()
                        + "]_ST" + threshold + "_" + System.currentTimeMillis() / 100 + ".txt"));
                this.out = new BufferedWriter(fstream);
            } catch (IOException ex) {
                Logger.getLogger(SemSearch.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        this.outputAllSubDeets = true;
        if (query.getSub().vertexSet().size() > 4) {
            this.outputAllSubDeets = false;
        }
    }

    public void run() {
        try {
            if (query.getSub().vertexSet().size() < 4) {
                runIndividual();
            } else {
                //runSplitOneAtATime();
                runSplit(null);
            }
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(SearchManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Run an individual search in one go.
     */
    public void runIndividual() {

        this.add = new AdditionCheck(source, query, null, sdc);
        this.mostConnectedSemNode = mostConnectedSem(add.getSubMap(), startingClass);
        this.mostConnectedTopNode = mostConnectedTop(add.getSubMap());
        if (threshold == 0.0) {//just do it topologically
            this.initialNodes = startingNodes(mostConnectedTopNode,
                    add.getSubMap(), add.getTargeyMap());
        } else {
            try {
                //use the starting CC
                this.initialNodes = source.firstSemanticMatch(startingNodes(mostConnectedSemNode,
                        add.getSubMap(), add.getTargeyMap()), getStartingCC());
            } catch (IOException ex) {
                Logger.getLogger(SemSearch.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        SemSearch sem = null;
        startTime2 = System.currentTimeMillis();
        try {
            sem = new SemSearch(source, query, initialNodes, startingClass, add, SDC, threshold, closedWorld, outPut, noElementsBelow, nodeDistance, edgeDistance);

            double count = 1.0;
            System.out.println("INITIAL NODES:: " + initialNodes.size());
            TypedNode sub = null;
            for (TypedNode init : initialNodes.keySet()) {
                sub = init;
                for (TypedNode tar : initialNodes.get(sub)) {
                    printProgBar(((count / initialNodes.get(sub).size()) * 100), "IN_SRCH");
                    sem.runSearchSinglePair(sub, tar);
                    List<TypedNode[]> matches = sem.getallMatches();
                    if (matches.size() > 0) {
                        this.sub_order = sem.getSubOrder();
                        if (outPut) {
                            addToFile(matches, outputAllSubDeets);
                        }
                    }
                    if (count % 100 == 0.0) {
                        gc();
                    }
                    count++;
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(SearchManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        endTime2 = System.currentTimeMillis();

        LOGGER.log(Level.INFO, "[SEARCH_TOOK]{0}", getTime());
        LOGGER.log(Level.INFO, "[MAPPINGS]{0}", (numberOfMatches - 1));
        LOGGER.log(Level.INFO, "[SUB_SIZE]{0}/{1}", new Object[]{query.getSub().vertexSet().size(), query.getSub().edgeSet().size()});
        LOGGER.log(Level.INFO, "[TAR_SIZE]{0}/{1}", new Object[]{source.getSourceGraph().vertexSet().size(), source.getSourceGraph().edgeSet().size()});
        LOGGER.log(Level.INFO, "[SUB_ORDER]{0}", getSubOrderToString());
    }

    /**
     * Run a split search one crossover node at a time.
     */
    public void runSplit(RunSplit run) {

        RunSplit split = null;
        if (run != null) {
            split = run;
        } else {
            split = new RunSplit(query);
            split.setSplitMax(splitMax);
            split.split();
        }

        startTime2 = System.currentTimeMillis();
        SplitNode focus = split.getData().getRootNode();
        int count = 0;
        boolean mappings = true;
        while (focus != null) {
            if (count > 0) {
                if (mappings) {
                    mappings = semSearch(focus, split.getData());
                }
            }
            focus = split.getData().getNextNode();
            count++;
        }
        endTime2 = System.currentTimeMillis();

        LOGGER.log(Level.INFO, "[SPLIT_SEARCH_TOOK]{0}", getTime());
        LOGGER.log(Level.INFO, "[MAPPINGS]{0}", (numberOfMatches - 1));
        LOGGER.log(Level.INFO, "[SUB_SIZE]{0}/{1}", new Object[]{query.getSub().vertexSet().size(), query.getSub().edgeSet().size()});
        LOGGER.log(Level.INFO, "[TAR_SIZE]{0}/{1}", new Object[]{source.getSourceGraph().vertexSet().size(), source.getSourceGraph().edgeSet().size()});

    }

    public boolean semSearch(SplitNode node, DataStructureSplit data) {

        boolean mappingsCheck = true;

        TypedNode[] sub_order_1 = null;
        TypedNode[] sub_order_2 = null;
        List<TypedNode[]> matches_1 = null;
        List<TypedNode[]> matches_2 = null;
        boolean outPut = false;
        SemSearch sem1 = null;
        SemSearch sem2 = null;

        //to delete nodes after using
        boolean deleteOne = false;
        boolean deleteTwo = false;

        LOGGER.log(Level.INFO, " --------------------------- Looking at node ::{0}", node.getUIDs());
        //if it is the lowest node then we do a search...
        GraphSplitInstance gs = node.getSplit();
        AdditionCheck add_1;
        AdditionCheck add_2;
        if (node == data.getRootNode()) {
            //System.out.println("----------------------------- is this the root node???");
            add_1 = new AdditionCheck(source, gs.getQueryGraph1(), query, sdc);
            add_2 = new AdditionCheck(source, gs.getQueryGraph2(), query, sdc);
            outPut = true;

        } else {
            add_1 = new AdditionCheck(source, gs.getQueryGraph1(), gs.getOrig(), sdc);
            add_2 = new AdditionCheck(source, gs.getQueryGraph2(), gs.getOrig(), sdc);
        }

        this.initialNodes = startingNodes(mostConnectedSem(add_1.getOrigisubMap(), "any"), add_1.getOrigisubMap(), add_1.getTargeyMap());
        LOGGER.log(Level.INFO, "We have {0} initial nodes", initialNodes.size());
        LOGGER.log(Level.INFO, "Crossovertype {0} type", gs.getCrossoverNode().getType());
        if (node.isSplits1()) {
            deleteOne = true;
            LOGGER.log(Level.INFO, "S1 already found for {0}", node.getUIDs());
            //if the graph has been split then get the node that represents it
            SplitNode s1Split = data.getNode(node.getSplit().getQueryGraph1().getSubgraphName());
            matches_1 = s1Split.getMergedMappings();
            sub_order_1 = s1Split.getMappedSubOrder();

        } else {
            LOGGER.log(Level.INFO, "Need to search for S1 {0}", node.getUIDs());
            LOGGER.log(Level.INFO, "Sub details: {0}", gs.getQueryGraph1().toString());
            sem1 = new SemSearch(source, gs.getQueryGraph1(), initialNodes, gs.getCrossoverNode().getType(), add_1, SDC, threshold, closedWorld, outPut, noElementsBelow, nodeDistance, edgeDistance);
            try {
                sem1.completeSearch();
            } catch (IOException ex) {
                Logger.getLogger(SearchManager.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
            matches_1 = sem1.getallMatches();
            LOGGER.log(Level.INFO, "[MAPPINGS] sub1 {0}", sem1.getallMatches().size());
            if (sem1.getallMatches().size() > 0) {
                sub_order_1 = sem1.getSubOrder();
                LOGGER.log(Level.INFO, "[SUB_ORDER]{0}", getSubOrderToString(sem1.getSubOrder()));
            }

        }
        if (node.isSplits2()) {
            deleteTwo = true;
            //if the graph has been split then get the node that represents it
            LOGGER.log(Level.INFO, "S2 already found for {0}", node.getUIDs());
            SplitNode s2Split = data.getNode(node.getSplit().getQueryGraph2().getSubgraphName());
            matches_2 = s2Split.getMergedMappings();
            sub_order_2 = s2Split.getMappedSubOrder();

        } else {
            LOGGER.log(Level.INFO, "Need to search for S2 {0}", node.getUIDs());
            LOGGER.log(Level.INFO, "Sub details: {0}", gs.getQueryGraph2().toString());
            pruneInitialNodes(matches_1, getCrossoverNodeLocation(sub_order_1, gs.getCrossoverNode()));
            sem2 = new SemSearch(source, gs.getQueryGraph2(), initialNodes, gs.getCrossoverNode().getType(), add_2, SDC, threshold, closedWorld, outPut, noElementsBelow, nodeDistance, edgeDistance);
            try {
                sem2.completeSearch();
            } catch (IOException ex) {
                Logger.getLogger(SearchManager.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
            LOGGER.log(Level.INFO, "[MAPPINGS] sub2 {0}", sem2.getallMatches().size());
            if (sem2.getallMatches().size() > 0) {
                LOGGER.log(Level.INFO, "[SUB_ORDER]{0}", getSubOrderToString(sem2.getSubOrder()));
                matches_2 = sem2.getallMatches();
                sub_order_2 = sem2.getSubOrder();
            }
        }
        //then map

        List<TypedNode[]> mappings = new ArrayList<>();
        if (matches_1 != null && matches_2 != null) {
            if (matches_1.size() > 0 && matches_2.size() > 0) {
                int sem1Crossover = getCrossoverNodeLocation(sub_order_1, gs.getCrossoverNode());
                int sem2Crossover = getCrossoverNodeLocation(sub_order_2, gs.getCrossoverNode());
                getOrigOrder(sub_order_1, sub_order_2, sem2Crossover, gs.getOrig());
                double count = 1.0;
                LOGGER.info("Indexing mappings....");
                //sort the second mappings into a hashMap for quick access
                Map<TypedNode, Set<Integer>> index = new HashMap<>();
                for (TypedNode[] second : matches_2) {
                    if (index.containsKey(second[sem2Crossover])) {
                        Set<Integer> subMappings = index.get(second[sem2Crossover]);
                        subMappings.add(matches_2.indexOf(second));
                        index.put(second[sem2Crossover], subMappings);
                    } else {
                        Set<Integer> subMappings = new HashSet<>();
                        subMappings.add(matches_2.indexOf(second));
                        index.put(second[sem2Crossover], subMappings);
                    }
                }

                LOGGER.info("Merging mappings....");
                for (TypedNode[] first : matches_1) {
                    if (index.containsKey(first[sem1Crossover])) {
                        Set<Integer> positions = index.get(first[sem1Crossover]);
                        for (int pos : positions) {
                            TypedNode[] merged = null;
                            merged = mergeSubs(first, matches_2.get(pos), sem2Crossover, gs.getOrig(), outPut);
                            if (merged != null) {
                                mappings.add(merged);
                            }
                        }
                    }
                    printProgBar(((count / matches_1.size()) * 100), "MERGING");
                    count++;
                }
            }

            //if the graph has been split then get the node that represents it
            String s1Split = node.getSplit().getQueryGraph1().getSubgraphName();
            data.deleteNode(s1Split);
            LOGGER.log(Level.INFO, "Deleted previous node...{0}", s1Split);
           
            String s2Split = node.getSplit().getQueryGraph2().getSubgraphName();
            data.deleteNode(s2Split);
            LOGGER.log(Level.INFO, "Deleting previous node...{0}", s2Split);

        }


        if (mappings.isEmpty()) {
            mappingsCheck = false;
        } else {
            //then add the merged mappings to the node...          
            node.setMergedMappings(mappings);
            LOGGER.log(Level.INFO, "Adding {0} merged mappings to {1}", new Object[]{mappings.size(), node.getUIDs()});

            node.setMappedSubOrder(sub_order);
            node.setS1SubOrder(sub_order_1);
            node.setS2SubOrder(sub_order_2);
            LOGGER.log(Level.INFO, "Sub-order {0} for {1}", new Object[]{getSubOrderToString(sub_order), node.getUIDs()});
        }
        return mappingsCheck;
    }

    public void pruneInitialNodes(List<TypedNode[]> matches, int crossoverloc) {
        Set<TypedNode> temp = new HashSet<>();
        TypedNode sub = null;
        for (TypedNode s : initialNodes.keySet()) {
            sub = s;
            temp.addAll(initialNodes.get(s));

        }
        // temp.putAll(initialNodes.values());
        for (TypedNode t : temp) {

            boolean succ = false;
            for (TypedNode[] ty : matches) {
                if (ty[crossoverloc] == t) {
                    succ = true;
                    break;
                }
            }
            if (succ == false) {
                Set<TypedNode> tars = initialNodes.get(sub);
                tars.remove(t);
                initialNodes.put(sub, tars);
            }

        }
    }

    public int getCrossoverNodeLocation(TypedNode[] subOrder, TypedNode crossover) {
        int loc = 0;
        for (int i = 0; i < subOrder.length; i++) {
            if (subOrder[i] == crossover) {
                loc = i;
            }
        }
        return loc;
    }

    public boolean isWriteToFile() {
        return writeToFile;
    }

    public SourceGraphNEW getSource() {
        return source;
    }

    public QueryGraph getQuery() {
        return query;
    }

    public QueryGraph getOriginalSub() {
        return originalSub;
    }

    public TypedEdge getInferredEdge() {
        return inferredEdge;
    }

    public TypedNode getMostConnectedTopNode() {
        return mostConnectedTopNode;
    }

    public TypedNode getMostConnectedSemNode() {
        return mostConnectedSemNode;
    }

    public AdditionCheck getAdd() {
        return add;
    }

    public BufferedWriter getOut() {
        return out;
    }

    public long getStartTime2() {
        return startTime2;
    }

    public long getEndTime2() {
        return endTime2;
    }

    public String getStartClass() {
        return startingClass;
    }

    public void addToFile(List<TypedNode[]> matches, boolean allSubDeets) {

        for (TypedNode[] match : matches) {
            try {
                addToFile(match);
            } catch (IOException ex) {
                Logger.getLogger(SearchManager.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Write all of the matches identified to file
     *
     * @param match1
     * @throws IOException
     */
    public void addToFile(TypedNode[] match)
            throws IOException {

        out.append(">>> " + numberOfMatches + "\n");

        if (outputAllSubDeets) {
            out.append("[SUB_INFO]" + "\n");
            try {
                out.append("SS:" + scoreMatchDoubleCHECK(match, query, sub_order, threshold, source, sdc, SDC, sdc.getAllConceptClasses()) + "\n");

            } catch (Exception ex) {
                Logger.getLogger(SemSearch.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
            //node info
            out.append("SN:");
            for (int t = 0; t < match.length; t++) {
                out.append(match[t].getId() + "\t");
            }
            out.append("\n");

            out.append("NN:");
            for (int t = 0; t < match.length; t++) {
                out.append(match[t].getName() + "\t");
            }
            out.append("\n");

            out.append("CC:");
            for (int t = 0; t < match.length; t++) {
                out.append(match[t].getType() + "\t");
            }
            out.append("\n");

            //edge info
            Set<TypedEdge> edges = new HashSet<TypedEdge>();
            for (int t = 0; t < match.length; t++) {
                Set<TypedEdge> edge = source.getSourceGraph().edgesOf(match[t]);
                for (TypedEdge e : edge) {
                    TypedNode from = e.getV1();
                    TypedNode to = e.getV2();
                    boolean toCheck = false;
                    boolean fromCheck = false;
                    for (int p = 0; p < match.length; p++) {
                        if (match[p] == to) {
                            toCheck = true;
                        }
                        if (match[p] == from) {
                            fromCheck = true;
                        }
                    }
                    if (toCheck && fromCheck) {
                        edges.add(e);
                    }
                }
            }

            TypedEdge[] t = new TypedEdge[edges.size()];
            int count = 0;
            for (TypedEdge e : edges) {
                t[count] = e;
                count++;
            }

            out.append("SR:");
            for (int p = 0; p < t.length; p++) {
                TypedEdge o = t[p];
                out.append(o.getV1().getId() + ":" + o.getV2().getId() + "\t");
            }
            out.append("\n");

            out.append("RT:");
            for (int p = 0; p < t.length; p++) {
                TypedEdge o = t[p];
                out.append(o.getType() + "\t");
            }
            out.append("\n");
            out.append("[INFERRED_REL_INFO]" + "\n");
        }

        out.append(getInferredRelInfo(match));

        if (outputAllSubDeets) {
            out.append("\n");
        }

        numberOfMatches++;

    }

    /**
     * Return time taken to complete a search in seconds
     *
     * @return
     */
    public String getTime() {
        String time = (" " + (double) (endTime2 - startTime2) / 1000);
        return time.trim();
    }

    /**
     * If we have run a split search we need to
     */
    public void getOrigOrder(TypedNode[] sub_order_1, TypedNode[] sub_order_2, int sem2COLoc, QueryGraph original) {

        TypedNode[] subOrder2RemovedCO = removeCrossOver(sub_order_2, sem2COLoc);
        sub_order = new TypedNode[original.getSub().vertexSet().size()];
        for (int k = 0; k < sub_order_1.length; k++) {
            sub_order[k] = sub_order_1[k];
        }
        for (int k = 0; k < subOrder2RemovedCO.length; k++) {
            sub_order[k + (sub_order_1.length)] = subOrder2RemovedCO[k];
        }
    }

    public TypedNode[] removeCrossOver(TypedNode[] mapping, int crossoverLoc) {
        TypedNode[] removed = new TypedNode[mapping.length - 1];
        for (int i = 0; i < mapping.length; i++) {
            if (i < crossoverLoc) {
                removed[i] = mapping[i];
            } else if (i > crossoverLoc) {
                removed[i - 1] = mapping[i];
            }
        }
        return removed;
    }

    /**
     * Returns the sub order in String format; used for summary output.
     *
     * @return
     */
    public String getSubOrderToString(TypedNode[] subOrder) {
        StringBuilder order = new StringBuilder();
        for (int i = 0; i < subOrder.length; i++) {
            order.append(subOrder[i].getType()).append("\t");
        }
        return order.toString();
    }

    /**
     * Returns the sub order in String format; used for summary output.
     *
     * @return
     */
    public String getSubOrderToString() {
        StringBuilder order = new StringBuilder();
        for (int i = 0; i < sub_order.length; i++) {
            order.append(sub_order[i].getType()).append("\t");
        }
        return order.toString();
    }

    public TypedNode[] mergeSubs(TypedNode[] m1, TypedNode[] m2, int sem2COLoc, QueryGraph original, boolean outPut) {

        TypedNode[] subOrder2RemovedCO = removeCrossOver(m2, sem2COLoc);

        TypedNode[] mapping = new TypedNode[original.getSub().vertexSet().size()];
        System.arraycopy(m1, 0, mapping, 0, m1.length);
        for (int y = 0; y < subOrder2RemovedCO.length; y++) {
            mapping[y + m1.length] = subOrder2RemovedCO[y];
        }
        boolean check = false;
        try {
            check = checkMappedSub(mapping, original);

        } catch (Exception ex) {
            Logger.getLogger(SearchManager.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        //do we want to include all the subgraph details in the output file or JUST the details for the inferred edge- if the sub is extremely large we just want the inferred edge data.


        if (outPut) {
            if (check) {
                try {
                    addToFile(mapping);
                } catch (IOException ex) {
                    Logger.getLogger(SearchManager.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            if (check) {
                return mapping;
            }
        }
        return null;
    }

    /**
     * Returns the semantics for the inferred relation; if one has been
     * specified in the QueryGraph being searched for
     *
     * @param match
     * @return
     */
    public String getInferredRelInfo(TypedNode[] match) {

        TypedNode from = null;
        TypedNode to = null;

        if (inferredEdge == null) {
            return "no_info_on_inferred_assigned" + "\n";

        } else {
            for (int i = 0; i < sub_order.length; i++) {
                if (sub_order[i] == inferredEdge.getV1()) {
                    from = match[i];
                }
                if (sub_order[i] == inferredEdge.getV2()) {
                    to = match[i];
                }
            }
        }

        if (outputAllSubDeets) {
            return "IR:" + from.getId() + "\t" + to.getId() + "\n" + "IN:" + from.getName() + "\t" + to.getName() + "\n" + "IC:" + from.getType() + "\t" + to.getType() + "\t" + inferredEdge.getType() + "\n";
        } else {
            return "IN:" + from.getName() + "\t" + to.getName() + "\n";
        }
    }

    public boolean checkMappedSub(TypedNode[] mapping, QueryGraph original) throws Exception {

        //check for any repeated nodes
        if (repeatabiltyCHECK(mapping) == false) {
            return false;
        }
        //closedWorldFirst
        if (closedWorldCHECK(mapping, sub_order, source.getSourceGraph(), original.getSub(), true, 2) == false) {
            return false;

        }
        //check SDC
        if (scoreMatchBooleanCHECK(original, source, sub_order, mapping, sdc, sdc.getAllConceptClasses(), SDC, noElementsBelow, threshold, 2) == false) {
            return false;
        }
        return true;
    }

    public void setOutputDeets(boolean outPutAllDeets) {
        this.outputAllSubDeets = outPutAllDeets;
    }

    public static void printProgBar(double percent, String header) {
        StringBuilder bar = new StringBuilder(header + "[");

        for (int i = 0; i < 50; i++) {
            if (i < (percent / 2)) {
                bar.append("=");
            } else if (i == (percent / 2)) {
                bar.append(">");
            } else {
                bar.append(" ");
            }
        }

        bar.append("]   ").append(percent).append("%     ");
        System.out.print("\r");
        System.out.print("\r" + bar.toString());
    }

    public static void gc() {
        Object obj = new Object();
        WeakReference ref = new WeakReference<>(obj);
        obj = null;
        while (ref.get() != null) {
            System.gc();
        }
    }
}
