/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.sourceGraph;

import java.io.Serializable;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class TypedNodeUID {

    private TypedNode nodeID;
    private String UID;

    public TypedNodeUID(TypedNode nodeID, String UID) {
        this.nodeID = nodeID;
        this.UID = UID;

    }

    public TypedNode getTypedNode() {
        return nodeID;
    }

    public String getUID() {
        return UID;
    }

    public void setTypedNode(TypedNode typedNode) {
        this.nodeID = typedNode;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public String toString() {
        return UID + "\t" + nodeID;
    }

  
}
