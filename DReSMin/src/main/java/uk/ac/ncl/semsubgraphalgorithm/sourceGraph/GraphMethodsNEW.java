/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.sourceGraph;

import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.SourceGraphNEW;
import uk.ac.ncl.semsubgraphalgorithm.queryGraphs.QueryGraph;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedNode;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedEdge;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.jgrapht.DirectedGraph;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.DefaultEdge;
import uk.ac.ncl.semsubgraphalgorithm.semanticDistanceSubgraphExclusion.SemanticDistanceCalculator;

/**
 *
 * @author joemullen
 */
public class GraphMethodsNEW {

    String startingConceptClass;

    /**
     * Returns a hashmap of the typed Node and its edgeset [0] total [1]
     * incoming [2] outoging [3] symmetrical.
     *
     * <CHECKED>
     *
     * @param graph2
     * @return
     */
    public HashMap<TypedNode, int[]> nameEdgeset(
            DirectedGraph<TypedNode, TypedEdge> graph2) {
        DirectedGraph<TypedNode, TypedEdge> localSubgraph = graph2;
        Set<TypedNode> conSmall = localSubgraph.vertexSet();
        HashMap<TypedNode, int[]> allInfo = new HashMap<TypedNode, int[]>(
                conSmall.size());

        Iterator<TypedNode> conits = conSmall.iterator();
        while (conits.hasNext()) {
            // for (String node1 : conSmall){
            int[] local = new int[4];
            TypedNode node1 = conits.next();
            Set<TypedEdge> totaledges2 = localSubgraph.edgesOf(node1);
            Set<TypedEdge> incoming2 = localSubgraph.incomingEdgesOf(node1);
            Set<TypedEdge> outgoing2 = localSubgraph.outgoingEdgesOf(node1);
            local[0] = totaledges2.size();
            local[1] = incoming2.size();
            local[2] = outgoing2.size();

            ArrayList<TypedNode> incomingEdgesNodes = new ArrayList<>();
            for (TypedEdge edge1 : incoming2) {
                TypedNode tar = graph2.getEdgeSource(edge1);
                incomingEdgesNodes.add(tar);
            }

            int reverse = 0;
            for (TypedEdge t : outgoing2) {
                TypedNode from = t.getV1();
                TypedNode to = t.getV2();
                if (graph2.containsEdge(to, from)) {
                    reverse++;
                }
            }
            local[3] = reverse;
            allInfo.put(node1, local);
        }
        return allInfo;
    }

    /**
     * Returns the most connected node of a particular semantic class. If the
     * startclass equals none then just retrieve the most highly connected.
     *
     * @param edgeset
     * @param conceptClass
     * @param queryNODEInfo
     * @return
     *
     * <CHECKED>
     */
    public TypedNode mostConnectedSem(HashMap<TypedNode, int[]> edgeset, String conceptClass) {
        TypedNode mostConnected = null;
        int highest = 0;
        System.out.println("edgeset: " + edgeset.keySet().size());
        Iterator<TypedNode> it = edgeset.keySet().iterator();
        while (it.hasNext()) {
            TypedNode node = it.next();
            int[] local = edgeset.get(node);
            if (local[0] > highest) {
                if (conceptClass.equals("any")) {
                    highest = local[0];
                    mostConnected = node;
                } else {
                    if (node.getType().equals(conceptClass)) {
                        highest = local[0];
                        mostConnected = node;
                    }
                }
            }
        }
        if (conceptClass.equals("any")) {
            startingConceptClass = mostConnected.getType();
            System.out.println("STARTING CONCEPT CLASS: " + startingConceptClass);
        } else {
            startingConceptClass = conceptClass;
        }

        return mostConnected;
    }

    /**
     * Returns the most connected node of a subgraph.
     *
     * <CHECKED>
     */
    public TypedNode mostConnectedTop(HashMap<TypedNode, int[]> edgeset) {

        TypedNode mostConnected = null;
        int highest = 0;
        Iterator<TypedNode> it = edgeset.keySet().iterator();
        while (it.hasNext()) {
            TypedNode node = it.next();
            int[] local = edgeset.get(node);
            if (local[0] > highest) {
                highest = local[0];
                mostConnected = node;
            }
        }
        return mostConnected;

    }

    // returns all matches from target graph of the most highly connected node
    // in sub
    public ArrayList<String> startingNodesSem(String highestfromsub,
            HashMap<String, int[]> subnameedgset,
            HashMap<String, int[]> tarnameedgset, String ConceptClass) {
        int[] local = subnameedgset.get(highestfromsub);
        int connetivity = local[0];
        ArrayList<String> matches = new ArrayList<String>();
        Iterator<String> it = tarnameedgset.keySet().iterator();

        while (it.hasNext()) {
            String name = (String) it.next();
            int[] local2 = tarnameedgset.get(name);

            if (local2[0] >= connetivity) {

                matches.add(highestfromsub + ",,," + name);
                // .out.println(name);
            }

        }
        //System.out.println("connectivity matches in large graph "
        //	+ matches.toString());
        return matches;

    }

    /**
     * Returns all nodes from the target graph that matches the connectivity and
     * given semantic class as the highest connected node from the query graph.
     *
     * @param highestfromsub
     * @param subnameedgset
     * @param tarnameedgset
     * @return
     *
     * <CHECKED>
     *
     */
    public HashMap<TypedNode, Set<TypedNode>> startingNodes(TypedNode highestfromsub,
            HashMap<TypedNode, int[]> subnameedgset,
            HashMap<TypedNode, int[]> tarnameedgset) {

        int[] local = subnameedgset.get(highestfromsub);
        int connetivity = local[0];
        int outgoing = local[1];
        int ingoing = local[2];
        int symm = local[3];
        HashMap<TypedNode, Set<TypedNode>> matches = new HashMap<>();
        Set<TypedNode> matchestar = new HashSet<>();
        Iterator<TypedNode> it = tarnameedgset.keySet().iterator();

        while (it.hasNext()) {
            TypedNode node = it.next();
            int[] local2 = tarnameedgset.get(node);
            if ((local2[0] >= connetivity) && (local2[1] >= outgoing) && (local2[2] >= ingoing) && (local2[3] >= symm) && node.getType().equals(highestfromsub.getType())) {
                matchestar.add(node);
            }
        }

        matches.put(highestfromsub, matchestar);
        return matches;
    }

    /**
     * Returns the nodeset size of a graph.
     *
     * @param entry
     * @return
     *
     * <CHECKED>
     *
     */
    public int getGraphSize(DirectedGraph<TypedNode, TypedEdge> entry) {
        int thidid = entry.vertexSet().size();
        // System.out.println("size of graph  " + thidid);
        return thidid;

    }

    // returns all the nodes from a graph in the form of a ArrayList 
    public ArrayList<String> getGetNodes(
            DirectedGraph<String, DefaultEdge> network) {
        String[] conceptsbiggraph = toStringArray(network.vertexSet());
        ArrayList<String> nodesBig = new ArrayList<String>(
                conceptsbiggraph.length);
        for (String s : conceptsbiggraph) {
            nodesBig.add(s);
            // .out.println(s);
        }

        return nodesBig;

    }

    // allows for the conversion of a Set of Strings to an array of Strings
    public String[] toStringArray(Set<String> conLarge1) {
        Set<String> localSet = conLarge1;
        String[] conceptSet = new String[conLarge1.size()];
        Iterator<String> conc = localSet.iterator();
        int count1 = 0;
        while (conc.hasNext()) {
            String thisT = "";
            thisT = conc.next();
            conceptSet[count1] = thisT;
            count1++;

        }
        return conceptSet;
    }

    /**
     * Returns all the outgoing nodes of a state; not including those that are
     * already in the match.
     *
     * @param graph
     * @param originalNodes
     * @return
     *
     * <CHECKED>
     */
    public TypedNode[] tOut(DirectedGraph<TypedNode, TypedEdge> graph,
            TypedNode[] originalNodes) {

        ArrayList<TypedNode> newnodesOutLIST = new ArrayList<TypedNode>();

        for (int i = 0; i < originalNodes.length; i++) {

            if (originalNodes[i] != null) {
                Set<TypedEdge> edges = graph.outgoingEdgesOf(originalNodes[i]);
                for (TypedEdge edgeysys : edges) {
                    TypedNode tar = graph.getEdgeTarget(edgeysys);
                    int matches = 0;
                    int count = 0;
                    //testing it isnt one of the original nodes
                    while (count < originalNodes.length) {
                        TypedNode heree = originalNodes[count];
                        // System.out.println("comparing orig: "+ heree+"  new: "+ tar);
                        if (heree == tar) {
                            matches++;
                        }
                        count++;
                    }
                    //testing it has not already been added to newNodesOutlist
                    for (TypedNode node : newnodesOutLIST) {
                        if (node == tar) {
                            matches++;
                        }

                    }
                    if (matches == 0) {
                        newnodesOutLIST.add(tar);
                    }

                }

            }
        }
        TypedNode[] outNodes = new TypedNode[newnodesOutLIST.size()];
        for (int y = 0; y < newnodesOutLIST.size(); y++) {
            outNodes[y] = newnodesOutLIST.get(y);
        }
        return outNodes;
    }

    /**
     *
     * @param graph
     * @param originalNode
     * @return
     *
     * <CHECKED>
     */
    public TypedNode[] tOut(DirectedGraph<TypedNode, TypedEdge> graph,
            TypedNode originalNode) {

        TypedNode[] single = new TypedNode[1];
        single[0] = originalNode;

        return tOut(graph, single);

    }

    /**
     * Returns all the incoming nodes of a state; not including those that are
     * already in the match.
     *
     * @param graph
     * @param originalNodes
     * @return
     *
     * <CHECKED>
     */
    public TypedNode[] tIn(DirectedGraph<TypedNode, TypedEdge> graph,
            TypedNode[] originalNodes) {

        ArrayList<TypedNode> newnodesInLIST = new ArrayList<TypedNode>();

        for (int i = 0; i < originalNodes.length; i++) {
            if (originalNodes[i] != null) {
                Set<TypedEdge> edges = graph.incomingEdgesOf(originalNodes[i]);
                for (TypedEdge edgeysys : edges) {
                    TypedNode tar = graph.getEdgeSource(edgeysys);
                    int matches = 0;
                    int count = 0;
                    while (count < originalNodes.length) {
                        TypedNode heree = originalNodes[count];
                        if (heree == tar) {
                            matches++;
                        }
                        count++;
                    }

                    for (TypedNode node : newnodesInLIST) {
                        if (node == tar) {
                            matches++;
                        }
                    }
                    if (matches == 0) {
                        newnodesInLIST.add(tar);
                    }
                }

            }
        }
        TypedNode[] inNodes = new TypedNode[newnodesInLIST.size()];
        for (int y = 0; y < newnodesInLIST.size(); y++) {
            //System.out.println("Addding: "+ newnodesInLIST.get(y).toString());
            inNodes[y] = newnodesInLIST.get(y);
        }
        return inNodes;
    }

    /**
     *
     * @param graph
     * @param originalNode
     * @return
     *
     * <CHECKED>
     */
    public TypedNode[] tIn(DirectedGraph<TypedNode, TypedEdge> graph,
            TypedNode originalNode) {

        TypedNode[] single = new TypedNode[1];
        single[0] = originalNode;
        return tIn(graph, single);

    }

    /**
     * Checks to see if a potential match contains the same node twice.
     *
     * @param match
     * @return
     *
     * <CHECKED>
     */
    public boolean repeatabiltyCHECK(TypedNode[] match) {

        boolean check = true;
        for (int y = 0; y < match.length; y++) {
            for (int u = 0; u < match.length; u++) {
                if (u != y) {
                    if (match[u] == match[y]) {
                        return false;
                    }
                }
            }
        }
        return check;
    }

    /**
     * Closed world check.
     *
     * @param match
     * @param sub_order
     * @param targ
     * @param motif
     * @param run
     * @param subOrderCount
     * @return
     *
     * <CHECKED>
     */
    public boolean closedWorldCHECK(TypedNode[] match, TypedNode[] sub_order, DirectedGraph<TypedNode, TypedEdge> targ, DirectedGraph<TypedNode, TypedEdge> motif, boolean run, int subOrderCount) {

        if (subOrderCount == 0) {
            return true;
        }

        boolean test = true;
        //if we aren't using a closed world approach just return true
        if (run == false) {
            //  System.out.println("closedWorld: true");
            return true;

        } else {

            for (int u = 0; u < sub_order.length; u++) {
                if (sub_order[u] == null) {
                    break;
                }
                for (int i = 0; i < sub_order.length; i++) {
                    if (sub_order[i] == null) {
                        break;
                    }
                    if ((motif.containsEdge(sub_order[u], sub_order[i]) == false) && targ
                            .containsEdge(match[u], match[i]) == true) {
                        //                   System.out.println("closedWorld: false");

                        return false;
                    }

                    if ((motif.containsEdge(sub_order[i], sub_order[u]) == false) && (targ
                            .containsEdge(match[i], match[u]) == true)) {
                        //                 System.out.println("closedWorld: false");
                        return false;
                    }

                }

            }
        }
        return test;
    }

    /**
     * Checks to see if a node is already in a match before it is added.
     *
     * @param node
     * @param contained
     * @param STATE
     * @return
     *
     * <CHECKED>
     */
    public boolean repeatabiltyCHECK(TypedNode node, TypedNode[] contained, int STATE) {
        if (STATE > 0) {
            for (int i = 0; i <= contained.length - 1; i++) {
                TypedNode here = contained[i];
                if (node == here) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Score potential matches.
     *
     * @param query
     * @param source
     * @param sub_order
     * @param match
     * @param sub_concept_classes
     * @param sdc
     * @param allCCsINSDC
     * @param useSDC
     * @param noElementsBelowST
     * @param threshold
     * @param subOrderCount
     * @return
     * @throws Exception
     *
     * <CHECKED>
     */
    public boolean scoreMatchBooleanCHECK(QueryGraph query,
            SourceGraphNEW source,
            TypedNode[] sub_order,
            TypedNode[] match,
            SemanticDistanceCalculator sdc,
            Set<String> allCCsINSDC,
            boolean useSDC,
            boolean noElementsBelowST,
            double threshold,
            int subOrderCount) throws Exception {

        if (subOrderCount == 0) {
            // System.out.println("scoreBooleanMatch: true ???? 111");
            return true;
        } else {
            //if we want subgraphs that may contain elements distant from the 
            //ST but cumulatively are no lower than ST
            if (noElementsBelowST == false) {
                if (subOrderCount == 0) {
                    // System.out.println("scoreBooleanMatch: true ????");
                    return true;
                } else {

                    double semanticScore = scoreMatchDoubleCHECK(match, query, sub_order, threshold, source, sdc, useSDC, allCCsINSDC);

                    if (semanticScore >= threshold) {
                        ///   System.out.println("scoreBooleanMatch: true 1");
                        return true;
                    }
                }
            } //if we ONLY want to look at matches where EVERY element is > ST
            else {
                if (subOrderCount == 0) {
                    // System.out.println("scoreBooleanMatch: true n");
                    return true;
                } else if (scoreNodesOfMatchBoolCHECK(match, sub_order, useSDC, source, allCCsINSDC, threshold, sdc) && scoreRelsOfMatchBoolCHECK(match, sub_order, query, source, sdc, threshold)) {
                    // System.out.println("scoreBooleanMatch: true 2nd");
                    return true;
                }
            }
        }

        return false;

    }

    public double scoreMatchDoubleCHECK(TypedNode[] match, QueryGraph query, TypedNode[] sub_order, double threshold, SourceGraphNEW source, SemanticDistanceCalculator sdc, boolean useSDC, Set<String> allCCsINSDC) throws Exception {

        double ccScores = scoreConceptClassesofMatchCHECK(match, sub_order, useSDC, source, allCCsINSDC, threshold, sdc);
        double rtScores = scoreRelationTypesofMatchCHECK(match, query, sub_order, source, sdc);

        int potentialElements = query.getSub().edgeSet().size() + query.getSub().vertexSet().size();

        return (ccScores + rtScores) / potentialElements;

    }

    public double scoreRelationTypesofMatchCHECK(TypedNode[] match, QueryGraph query, TypedNode[] sub_order, SourceGraphNEW source, SemanticDistanceCalculator sdc) throws Exception {

        //if we aren't using a closed world approach just return true
        double score = 0.0;
        int edgesScored = 0;
        int allSubEdges = query.getSub().edgeSet().size();

        for (int u = 0; u < sub_order.length; u++) {

            for (int i = 0; i < sub_order.length; i++) {

                if (query.getSub().containsEdge(sub_order[u], sub_order[i])) {

                    String QUERYrelType = query.getSub().getEdge(sub_order[u], sub_order[i]).getType();
                    //query.getQueryRelInfo().get(sub_order[u] + ":" + sub_order[i]);
                    //System.out.println(QUERYrelType);

                    if (match[i] != null && match[u] != null) {
                        //x.out.println("Looking for edge type" + match[u] + ":" + match[i]);
                        Set<TypedEdge> MATCHrelTypes = source.getSourceGraph().getAllEdges(match[u], match[i]);

                        //if there is >1 relationType between the two concepts get the SDC of all of them
                        //but only keep the highest scoring one
                        double highestScoringRelType = 0.0;
                        for (TypedEdge e : MATCHrelTypes) {
                            double local = sdc.getEdgeDistance(QUERYrelType, e.getType());
                            if (local > highestScoringRelType) {
                                highestScoringRelType = local;
                            }
                        }
                        score += highestScoringRelType;
                        edgesScored++;

                    }

                }

            }
        }
        double remaining = (double) allSubEdges - edgesScored;
        return score + remaining;
    }

    public double scoreConceptClassesofMatchCHECK(TypedNode[] match, TypedNode[] sub_order, boolean useSDC, SourceGraphNEW s, Set<String> allCCsINSDC, double threshold, SemanticDistanceCalculator sdc) throws Exception {

        //if we aren't using a closed world approach just return true
        double score = 0.0;

        for (int i = 0; i < match.length; i++) {
            if (match[i] == null) {
                //must assume that all other concept_classes will give an exact match
                score += 1.0;
            } else {
                score += getNodeSDCCHECK(sub_order[i], match[i], useSDC, s, allCCsINSDC, threshold, sdc);
            }
        }

        return score;
    }

    private double getNodeSDCCHECK(TypedNode subNode, TypedNode tarNode, boolean useSDC, SourceGraphNEW s, Set<String> allCCsINSDC, double threshold, SemanticDistanceCalculator sdc) throws Exception {

        double score = 0.0;

        if (useSDC == false) {

            return 1.0;

        } else {
            String QueryCC = subNode.getType();
            String targetConceptClass = tarNode.getType();

            if (allCCsINSDC.contains(QueryCC) && (allCCsINSDC.contains(targetConceptClass))) {

                if (sdc.getNodeDistance(QueryCC, targetConceptClass) >= threshold) {

                    return sdc.getNodeDistance(QueryCC, targetConceptClass);
                }

            } else {

                return 0.0;
            }
        }
        return 0.0;

    }

    private boolean scoreNodesOfMatchBoolCHECK(TypedNode[] match, TypedNode[] sub_order, boolean useSDC, SourceGraphNEW s, Set<String> allCCsINSDC, double threshold, SemanticDistanceCalculator sdc) throws Exception {
        boolean check = true;

        for (int i = 0; i < match.length; i++) {
            if (match[i] != null) {
                if ((getNodeSDCCHECK(sub_order[i], match[i], useSDC, s, allCCsINSDC, threshold, sdc)) < threshold) {
                    return false;
                }
            }
        }
        return check;


    }

    private boolean scoreRelsOfMatchBoolCHECK(TypedNode[] match, TypedNode[] sub_order, QueryGraph query, SourceGraphNEW source, SemanticDistanceCalculator sdc, double threshold) throws Exception {

        boolean check = true;

        for (int u = 0; u < sub_order.length; u++) {

            for (int i = 0; i < sub_order.length; i++) {

                if (query.getSub().containsEdge(sub_order[u], sub_order[i])) {
                    String QUERYrelType = query.getSub().getEdge(sub_order[u], sub_order[i]).getType();
                    //System.out.println(QUERYrelType);

                    if (match[i] != null && match[u] != null) {
                        //System.out.println("Looking for edge type" + match[u] + ":" + match[i]);
                        Set<TypedEdge> MATCHrelType = source.getSourceGraph().getAllEdges(match[u], match[i]);

                        //if there is >1 relationType between the two concepts get the SDC of all of them
                        double highestScoringRelType = 0.0;
                        //but only keep the highest scoring one
                        for (TypedEdge edge : MATCHrelType) {
                            String type = edge.getType();

                            double local = sdc.getEdgeDistance(QUERYrelType, type);
                            if (local > highestScoringRelType) {
                                highestScoringRelType = local;

                            }
                        }

                        if (highestScoringRelType < threshold) {
                            return false;

                        }

                    }

                }

            }


        }
        return check;
    }

    public String getStartingCC() {
        return startingConceptClass;
    }
}
