/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.DrugTargetWork;

import java.io.Serializable;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class Pair implements Serializable{
    private static final long serialVersionUID = 1L;

    private String Drug;
    private String Target;

    public Pair(String Drug, String Target) {
        this.Drug = Drug; 
        this.Target = Target;
    }

    public String getTarget() {
        return Target;
    }

    public String getDrug() {
        return Drug;
    }

    public void setTarget(String target) {
        this.Target = target;
    }

    public void setDrug(String drug) {
        this.Drug = drug;
    }

    @Override
    public String toString() {
        return Drug + "\t" + Target;
    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((Drug == null) ? 0 : Drug.hashCode());
        result = prime * result + ((Target == null) ? 0 : Target.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        Pair other = (Pair) obj;

        return (this.Drug == other.Drug || (this.Drug != null && this.Drug.equals(other.Drug))) && (this.Target == other.Target || (this.Target != null && this.Target.equals(other.Target)));

    }
}
