/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.graphSplitting;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import uk.ac.ncl.semsubgraphalgorithm.management.SerialisedGraph;
import uk.ac.ncl.semsubgraphalgorithm.queryGraphs.QueryGraph;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedNode;

/**
 *
 * @author joemullen
 */
public class GraphSplitInstance {

    private QueryGraph q1;
    private QueryGraph q2;
    private QueryGraph orig;
    private TypedNode crossover;
    boolean check = false;
    private Set<String> graphNames;

    public GraphSplitInstance() {
    }

    public GraphSplitInstance(QueryGraph q1, QueryGraph q2, QueryGraph orig, TypedNode crossoverNode) {

        if (q2.getSub().vertexSet().size() > q1.getSub().vertexSet().size()) {
            this.q1 = q2;
            this.q2 = q1;

        } else {
            this.q1 = q1;
            this.q2 = q2;
        }
        this.crossover = crossoverNode;
        this.orig = orig;
        this.graphNames = new HashSet<String>();
        graphNames.add(q1.getSubgraphName());
        graphNames.add(q2.getSubgraphName());

        System.out.println(
                "[INFO] Created new splitinstance");

    }

    public Set<String> getUIDs() {
        return graphNames;

    }

    public QueryGraph getQueryGraph1() {
        return q1;

    }

    public QueryGraph getQueryGraph2() {
        return q2;

    }

    public QueryGraph getOrig() {
        return orig;

    }

    public TypedNode getCrossoverNode() {
        return crossover;
    }

    public boolean checkSplitSizes() {
        boolean check = true;


        if (q1.getSub().vertexSet().size() > 4) {
            check = false;
        }

        if (q2.getSub().vertexSet().size() > 4) {
            check = false;
        }

        return check;

    }

    public boolean getCheck() {

        return check;

    }

    public void setCheck(boolean c) {

        this.check = c;

    }

    @Override
    public String toString() {

        String summary = "[INFO] Q1: " + q1.getSub().toString() + "  Q2: " + q2.getSub().toString() + "  OverlappingNode(ON): " + crossover;

        return summary;


    }
}
