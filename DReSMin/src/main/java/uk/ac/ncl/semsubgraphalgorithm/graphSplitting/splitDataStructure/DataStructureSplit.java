/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.graphSplitting.splitDataStructure;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.SimpleGraph;
import uk.ac.ncl.semsubgraphalgorithm.SemSearch.AdditionCheck;
import uk.ac.ncl.semsubgraphalgorithm.SemSearch.SemSearch;
import uk.ac.ncl.semsubgraphalgorithm.graphSplitting.GraphSplitInstance;
import uk.ac.ncl.semsubgraphalgorithm.queryGraphs.QueryGraph;
import uk.ac.ncl.semsubgraphalgorithm.sourceGraph.TypedNode;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 *
 * Class represents the data structure for which mappings are held in during a
 * search.
 *
 */
public class DataStructureSplit {

    private UndirectedGraph<SplitNode, SplitEdge> structure;
    private QueryGraph Sub;
    private final static Logger LOGGER = Logger.getLogger(DataStructureSplit.class.getName());
    private SplitNode rootNode;

    public DataStructureSplit(QueryGraph Sub) {
        this.Sub = Sub;
        this.structure = new SimpleGraph<SplitNode, SplitEdge>(SplitEdge.class);
    }

    public SplitNode getRootNode() {
        return rootNode;
    }

    public void addNode(SplitNode add, String parent) {

        //if this is the first node being added it becomes the root node of the graph
        if (structure.vertexSet().size() == 0) {
            this.rootNode = add;
        }
        //add the node
        structure.addVertex(add);

        //then add the edge
        if (parent != null) {
            for (SplitNode sp : structure.vertexSet()) {
                if (sp.getSplit().getQueryGraph1().getSubgraphName().equals(parent) || sp.getSplit().getQueryGraph2().getSubgraphName().equals(parent)) {
                    structure.addEdge(sp, add, new SplitEdge(sp, add));
                }
            }
        }
    }

    public UndirectedGraph<SplitNode, SplitEdge> getStructure() {
        return structure;
    }

    public void printTree() {
        LOGGER.info("Nodes... ");
        for (SplitNode sp : structure.vertexSet()) {
            System.out.println("-------------------------------");
            System.out.println(sp.getUIDs().toString());
            System.out.println("Q1 " + sp.getSplit().getQueryGraph1().getSubgraphName() + "\t" + sp.getSplit().getQueryGraph1().getSub().vertexSet().size());
            for (TypedNode t : sp.getSplit().getQueryGraph1().getSub().vertexSet()) {
                System.out.println("\t" + t.getType() + "\t" + t.getName());
            }
            System.out.println("Q2 " + sp.getSplit().getQueryGraph2().getSubgraphName() + "\t" + sp.getSplit().getQueryGraph2().getSub().vertexSet().size());
            for (TypedNode t : sp.getSplit().getQueryGraph2().getSub().vertexSet()) {
                System.out.println("\t" + t.getType() + "\t" + t.getName());
            }
        }
        LOGGER.info("Edges... ");
        for (SplitEdge sp : structure.edgeSet()) {
            System.out.println("FROM: " + sp.getV1().getUIDs().toString() + "\t" + "TO:" + sp.getV2().getUIDs().toString());
        }

    }

    public QueryGraph getOrigiGraph() {
        return Sub;
    }

    public void deleteNode(String nodeID) {

        //delete the edges
        Set<SplitEdge> edges = structure.edgeSet();
        for (SplitEdge edge : edges) {
            if (edge.getV1().getUIDs().equals(nodeID)) {
                structure.removeEdge(edge);
            } else if (edge.getV1().getUIDs().equals(nodeID)) {
                structure.removeEdge(edge);
            }
        }
        edges =null;
        //delete the node
        Set<SplitNode> nodes = new HashSet<>(); 
        nodes.addAll(structure.vertexSet());      
        for(SplitNode t: nodes){
            if(t.getUIDs().equals(nodeID)){
                structure.removeVertex(t);
            }
        }
        nodes = null;
    }

    public void resetStructure() {
        for (SplitNode sp : structure.vertexSet()) {
            sp.setSplits1(false);
            sp.setSplits2(false);
            sp.setChecked(false);
        }
    }

    public QueryGraph getGraph(String graphName) {
        for (SplitNode sp : structure.vertexSet()) {
            if (sp.getSplit().getUIDs().contains(graphName)) {
                if (sp.getSplit().getQueryGraph1().getSubgraphName().equals(graphName)) {
                    return sp.getSplit().getQueryGraph1();
                }
                if (sp.getSplit().getQueryGraph2().getSubgraphName().equals(graphName)) {
                    return sp.getSplit().getQueryGraph2();
                }
            }
        }
        return null;
    }

    public SplitNode getNode(String graphName) {
        for (SplitNode sp : structure.vertexSet()) {
            if (sp.getUIDs().equals(graphName)) {
                return sp;
            }
        }
        return null;
    }

    public SplitNode getNextNode() {
        SplitNode longest = null;
        int relList = 0;
        for (SplitNode sp : structure.vertexSet()) {
            if (sp.isChecked() == false && sp != rootNode) {
                DijkstraShortestPath tr2 = new DijkstraShortestPath(structure,
                        rootNode, sp);
                int relListloc = tr2.getPath().getEdgeList().size();
                if (relListloc > relList) {
                    longest = sp;
                }
            }
        }

        if (longest == null) {
            if (rootNode.isChecked() == false) {
                longest = rootNode;
                rootNode.setChecked(true);
            }
        } else {
            longest.setChecked(true);
        }
        return longest;
    }
}
