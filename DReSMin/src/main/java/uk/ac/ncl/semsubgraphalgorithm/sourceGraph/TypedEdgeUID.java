/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.sourceGraph;

import java.io.Serializable;
import org.jgrapht.graph.DefaultEdge;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class TypedEdgeUID<V> extends DefaultEdge implements Serializable{

    private TypedNodeUID v1;
    private TypedNodeUID v2;


    public TypedEdgeUID(TypedNodeUID v1, TypedNodeUID v2) {
        this.v1 = v1;
        this.v2 = v2;

    }

    public TypedNodeUID getV1() {
        return v1;
    }

    public TypedNodeUID getV2() {
        return v2;
    }

}
