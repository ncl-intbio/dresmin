/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.sourceGraph;

import java.io.Serializable;
import org.jgrapht.graph.DefaultEdge;

/**
 *
 * @author Joe Mullen http://homepages.cs.ncl.ac.uk/j.mullen/
 */
public class TypedEdge<V> extends DefaultEdge implements Serializable{

    private TypedNode v1;
    private TypedNode v2;
    private String label;

    public TypedEdge(TypedNode v1, TypedNode v2, String label) {
        this.v1 = v1;
        this.v2 = v2;
        this.label = label;
    }

    public TypedNode getV1() {
        return v1;
    }

    public TypedNode getV2() {
        return v2;
    }

    public String getType() {
        return label;
    }
    
    public String toString(){
        return label+"\nFROM:\t"+v1.toString()+"\nTO:\t"+v2.toString();    
    }
}
