#!/usr/bin/python


import sys, getopt
import re
from sets import Set
import subprocess
import os

def main(argv):
    inputdirectory = ''
    outputdirectory = ''
    
    try:
        opts, args = getopt.getopt(argv,"hi:o:",["idir=","odir="])
    except getopt.GetoptError:
        print 'strip.py -i <inputdirectory> -o <outputdirectory>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'strip.py -i <inputdirectory> -o <outputdirectory>'
            sys.exit()
        elif opt in ("-i", "--idir"):
            inputdirectory = arg
        elif opt in ("-o", "--odir"):
            outputdirectory = arg
    print 'Input file is "', inputdirectory
    print 'Output file is "', outputdirectory
    # create the output direcoty if it does not exist
    if not os.path.exists(outputdirectory):
        os.makedirs(outputdirectory)
    # iterate every file in the directory
    iterateFiles(inputdirectory,outputdirectory)


def iterateFiles(inputdirectory, outputdirectory):
    files_in_dir = os.listdir(inputdirectory)
    for file_in_dir in files_in_dir:
        # create the same file in the new directory
        print file_in_dir
        if not os.path.exists(outputdirectory+'/'+file_in_dir):
            open(outputdirectory+'/'+file_in_dir, 'w').close()
        f = open(outputdirectory+'/'+file_in_dir,'w')
        for line in open(inputdirectory+'/'+file_in_dir,'r').readlines():
            if (line.startswith('>>>') or line.startswith('IN:')):
                f.write(line)
        f.close()

if __name__ == "__main__":
    main(sys.argv[1:])