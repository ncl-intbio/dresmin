#!/usr/bin/python

#############################
# Takes the output from the programme for the sub_scores and creates a file with the subs ready for latex
#############################


#############################
#RUNS::
# 1. python appendSubIDToFile.py -m subId2Name.txt -i /Users/joemullen/Desktop/SemSubNEW/DReSMin/Results/DB3WORK/RankedAss_V4_192/sub_scores.txt -o /Users/joemullen/Desktop/SemSubNEW/DReSMin/Results/DB3WORK/RankedAss_V4_192/LATEX_1.txt -s 0
# 2. python appendSubIDToFile.py -m subId2ShortestPath.txt -i //Users/joemullen/Desktop/SemSubNEW/DReSMin/Results/DB3WORK/RankedAss_V4_192/LATEX_1.txt -o /Users/joemullen/Desktop/SemSubNEW/DReSMin/Results/DB3WORK/RankedAss_V4_192/LATEX_2.txt -s 0
# 3. python subScoresTXT2Latex.py -i /Users/joemullen/Desktop/SemSubNEW/DReSMin/Results/DB3WORK/RankedAss_V4_192/LATEX_2.txt -o /Users/joemullen/Desktop/SemSubNEW/DReSMin/Results/DB3WORK/RankedAss_V4_192/sub_scoresLATEX_TABLE.txt
#############################

import sys, getopt
import re
from sets import Set
import os
import collections
import math


def main(argv):
    inputfile = ''
    outputfile = ''
    mapping = ''
    argList=sys.argv[1:]
    
    try:
        opts, args = getopt.getopt(argv,"hi:o:s:",["ifile=","ofile="])
    except getopt.GetoptError:
        print 'test.py -i <inputfile> -o <outputfile> -s <subNamePos>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'test.py -i <inputfile> -o <outputfile> -s <subNamePos>'
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
    print 'Input file', inputfile
    print 'Output file is', outputfile


    try:
        fp = open(outputfile)
    except IOError:
        # If not exists, create the file
        fp = open(outputfile, 'w+')

    #MAKE SURE YOU HAVE RAN THESE FIRST
    #get the sub id in there from the other script
    #os.system('python appendSubIDToFile.py %s' % ' '.join(argList))
    #get the number of shortest paths in there from the other script
    #os.system('python appendShortestPathToFile.py %s' % ' '.join(argList))
    createLatextable(inputfile,outputfile)

def createLatextable(inputfile,outputfile):
    separator = " & "
    dictionary = {};
    patt = re.compile("[^\t]+")
    for line in open(inputfile,'r').readlines():
        split = patt.findall(line)
        subID = split[5].replace("\n", "")
        subScore= float(split[4])
        subName = split[0]
        mappings = thous(split[1])
        munique = thous(split[2])
        validated = thous(split[3])
        shortestPath = str(split[6])
        if not math.isnan(subScore):
            subScore = "%0.5f" % subScore
        dictionary[subID] = subID+separator+shortestPath.replace("\n", "")+separator+mappings+separator+munique+separator+validated+separator+str(subScore)+" \\\\";
    
    print dictionary
    f = open(outputfile,'w')
    for x in range (0,200):
        if str(x) in dictionary:
            f.write(dictionary.get(str(x)).replace('_', '\\_')+'\n')
            print dictionary.get(str(x))
        
        else:
            print 'No element for value '+str(x)
    f.close()

def thous(x, sep=',', dot='.'):
    num, _, frac = str(x).partition(dot)
    num = re.sub(r'(\d{3})(?=\d)', r'\1'+sep, num[::-1])[::-1]
    if frac:
        num += dot + frac
    return num

if __name__ == "__main__":
    main(sys.argv[1:])