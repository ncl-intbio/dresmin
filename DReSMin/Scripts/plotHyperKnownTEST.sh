#!/bin/bash

args=("$@")

#########################
# Assign arguments
#########################

IPFILE="${args[0]}"
OPFILE="${args[1]}"
KNOWNFILE="${args[2]}"
SLIDING_WINDOW="${args[3]}"

#########################
# Data on associations
#########################

PREDASSOCIATIONS=$(wc -l < $IPFILE)
PREDASSOCIATIONS="${PREDASSOCIATIONS#"${PREDASSOCIATIONS%%[![:space:]]*}"}"
KNOWNASSOCIATIONS=$(wc -l < $KNOWNFILE)
KNOWNASSOCIATIONS="${KNOWNASSOCIATIONS#"${KNOWNASSOCIATIONS%%[![:space:]]*}"}"

#########################
# Some OP info
#########################

echo "Calculating hyper from file $IPFILE"
echo "Containing $PREDASSOCIATIONS associations"
echo "Retrieving known associations from $KNOWNFILE"
echo "Which contains $KNOWNASSOCIATIONS known associations"
echo "Output for hyper plot will be stored in $OPFILE"

#########################
# Function to check if array contains element
#########################


#########################
# Add known associations to KNWONARRAY
#########################

declare -A KNOWNARRAY
ASSOCIATION=""
ARRCOUNT=0
while read -r line
do
    FROM=$( echo "$line" | cut -d$'\t' -f1)
    TO=$( echo "$line" | cut -d$'\t' -f2)
    ASSOCIATION="$FROM$TO"
    KNOWNARRAY[$ASSOCIATION]="$ASSOCIATION"
    ARRCOUNT=$[ARRCOUNT +1]
done < "$KNOWNFILE"

echo "${!KNOWNARRAY[@]}"

echo ${#KNOWNARRAY[@]}


#########################
# Check to see if scored associations are known
#########################

COUNTER=1
MATCHES=0
SCORE=""
MATCHES_CUM=0
COUNTER_CUM=0
SCORE_CUM=0

while read -r line
do
    SCORE=$( echo "$line" | cut -d$'\t' -f1)
    FROM=$( echo "$line" | cut -d$'\t' -f2)
    TO=$( echo "$line" | cut -d$'\t' -f3)
    ASSOCIATION="$FROM$TO"
    if [ ${KNOWNARRAY["$ASSOCIATION"]+_} ]; then
        MATCHES=$[MATCHES +1]
    fi
    MATCHES_CUM=$[MATCHES_CUM + $MATCHES]
    COUNTER_CUM=$[COUNTER_CUM + $COUNTER]
    SCORE_CUM=$( echo $SCORE_CUM + $SCORE | bc -l)
    if (( $COUNTER%SLIDING_WINDOW == 0 )); then
      echo -e $( echo 'scale=1;' $MATCHES_CUM / $SLIDING_WINDOW | bc)'\t'$( echo 'scale=1;' $COUNTER_CUM / $SLIDING_WINDOW | bc -l)'\t'$KNOWNASSOCIATIONS'\t'$PREDASSOCIATIONS'\t'$( echo 'scale=8;' $SCORE_CUM / $SLIDING_WINDOW | bc -l) >> $OPFILE
      MATCHES_CUM=0
      COUNTER_CUM=0
      SCORE_CUM=0
    fi
    COUNTER=$[COUNTER +1] 
done < "$IPFILE"

