#!/usr/bin/python


import sys, getopt
import re
from sets import Set
import subprocess


def main(argv):
    inputfile = ''
    outputfile = ''
    knownfile = ''
    slidingwindow = ''
    known = ''
    knownNumber = ''
    inferredNumber = ''
    
    try:
        opts, args = getopt.getopt(argv,"hi:o:k:s:",["ifile=","ofile=","kfile=", "slidingwindow="])
    except getopt.GetoptError:
        print 'test.py -i <inputfile> -o <outputfile> -k <knownfile> -s <slidingwindow>'
        sys.exit(4)
    for opt, arg in opts:
        if opt == '-h':
            print 'test.py -i <inputfile> -o <outputfile> -k <knownfile> -s <slidingwindow>'
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
        elif opt in ("-k", "--knownfile"):
            knownfile = arg
        elif opt in ("-s", "--slidingwindow"):
            slidingwindow = int(arg)
    print 'Input file is "', inputfile
    print 'Output file is "', outputfile
    print 'Known file is "', knownfile
    print 'Sliding window is "', slidingwindow

    try:
        fp = open(outputfile)
    except IOError:
    # If not exists, create the file
        fp = open(outputfile, 'w+')

    knownNumber = file_len(inputfile)
    inferredNumber = file_len(knownfile)
    known = getKnownAssociations(knownfile);
    calculateHyper(inputfile, outputfile, known, slidingwindow, knownNumber, inferredNumber)
    print knownNumber


def calculateHyper(inferredfile, outputfile, knownAssociations, slidingwindow, knownNum, infNum):
    COUNTER=1
    MATCHES=0
    MATCHES_CUM=0
    COUNTER_CUM=0
    SCORE_CUM=0.0
    
    f = open(outputfile,'w')
    patt = re.compile("[^\t]+")
    for line in open(inferredfile,'r').readlines():
        split = patt.findall(line)
        association = '%s\t%s'%(split[1], split[2].replace("\n", ""))
        score = float(split[0])
        if association in knownAssociations:
            MATCHES +=1
        MATCHES_CUM= MATCHES_CUM + MATCHES
        COUNTER_CUM= COUNTER_CUM +COUNTER
        SCORE_CUM= SCORE_CUM + score
        if (int(COUNTER) % int(slidingwindow) == 0):
            f.write(str(float(MATCHES_CUM) / slidingwindow)+'\t'+str(float(COUNTER_CUM) / slidingwindow)+'\t'+str(knownNum)+'\t'+str(infNum)+'\t'+str(SCORE_CUM / slidingwindow)+'\n')
            MATCHES_CUM=0
            COUNTER_CUM=0
            SCORE_CUM=0
        COUNTER += 1
        
    f.close()

def getKnownAssociations(knownfile):
    known = Set()
    for line in open(knownfile,'r').readlines():
        known.add(line.replace("\n",""))
    return known

def file_len(fname):
    p = subprocess.Popen(['wc', '-l', fname], stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    result, err = p.communicate()
    if p.returncode != 0:
        raise IOError(err)
    return int(result.strip().split()[0])


if __name__ == "__main__":
    main(sys.argv[1:])