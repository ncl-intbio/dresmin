#!/usr/bin/python

##############################################
# To run:
#   python getSubNumsForBoxPlot.py -i /Users/joemullen/Desktop/SemSubNEW/DReSMin/target/scored_Associations_TRUE.txt -k /Users/joemullen/Desktop/SemSubNEW/DReSMin/Results/DB3WORK/validDB3_V_dataset.txt -o boxPlottest.txt
#
##############################################




import sys, getopt
import re
from sets import Set
import subprocess


def main(argv):
    inputfile = ''
    knownfile = ''
    known = ''
    outputfile = ''
    
    try:
        opts, args = getopt.getopt(argv,"hi:o:k:",["ifile=","kfile=","ofile"])
    except getopt.GetoptError:
        print 'test.py -i <inputfile> -k <knownfile> -o <outputfile>'
        sys.exit(3)
    for opt, arg in opts:
        if opt == '-h':
            print 'test.py -i <inputfile> -k <knownfile> -o <outputfile>'
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-k", "--knownfile"):
            knownfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
    print 'Input file is', inputfile
    print 'Output file is', inputfile
    print 'Known file is', knownfile


    try:
        fp = open(outputfile)
    except IOError:
        # If not exists, create the file
        fp = open(outputfile, 'w+')

    known = getKnownAssociations(knownfile);
    createOutput(inputfile, outputfile, known)


def createOutput(inferredfile, outputfile, known):
    
    f = open(outputfile,'w')
    patt = re.compile("[^\t]+")
    for line in open(inferredfile,'r').readlines():
        split = patt.findall(line)
        association = '%s\t%s'%(split[1], split[2].replace("\n", ""))
        numberOfSubs = split[3].replace('\n', '')
        if association in known:
            f.write(numberOfSubs+'\t'+'VALID'+'\n')
        else:
            f.write(numberOfSubs+'\t'+'NON_VALID'+'\n')
    f.close()

def getKnownAssociations(knownfile):
    known = Set()
    for line in open(knownfile,'r').readlines():
        known.add(line.replace("\n",""))
    return known


if __name__ == "__main__":
    main(sys.argv[1:])