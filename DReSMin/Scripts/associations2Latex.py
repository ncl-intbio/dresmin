#!/usr/bin/python

#############################
# Takes the output from the programme for the scored_Associations.txt and creates a file with 'n' associations ready for latex
#############################


#############################
#RUNS::
# 1. java -jar SemSubgraphAlgorithm-PROG-jar-with-dependencies.jar /Users/joemullen/Desktop/testResults /Users/joemullen/Desktop/SemSubNEW/DReSMin/Graph/con_listOP_55e0c.tsv /Users/joemullen/Desktop/SemSubNEW/DReSMin/Results/DB3WORK/uniqueDB3_V_dataset.txt /Users/joemullen/Desktop/SemSubNEW/DReSMin/sub_scores_DB3_FALSE.txt
#############################

import sys, getopt
import re
from sets import Set
import os
import collections
import math


def main(argv):
    inputfile = ''
    outputfile = ''
    number = ''
    
    try:
        opts, args = getopt.getopt(argv,"hi:o:s:",["ifile=","ofile=","numberAss="])
    except getopt.GetoptError:
        print 'test.py -i <inputfile> -o <outputfile> -s <numberAss>'
        sys.exit(3)
    for opt, arg in opts:
        if opt == '-h':
            print 'test.py -i <inputfile> -o <outputfile> -s <numberAss>'
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
        elif opt in ("-s", "--numberAss"):
            number = arg
    print 'Input file', inputfile
    print 'Output file is', outputfile
    print 'Table will contain ', number, ' associations'


    try:
        fp = open(outputfile)
    except IOError:
        # If not exists, create the file
        fp = open(outputfile, 'w+')

    createLatextable(inputfile,outputfile, number)

def createLatextable(inputfile,outputfile, number):
    f = open(outputfile,'w')
    COUNTER = 1
    separator = " & "
    patt = re.compile("[^\t]+")
    for line in open(inputfile,'r').readlines():
        print COUNTER, number
        if COUNTER == int(number):
            print '---------'
            break
        else:
            split = patt.findall(line)
            assScore = "%0.5f" % float(split[0])
            drugbank = split[1]
            uniprot = split[2].replace("\n", "")
            f.write(drugbank+separator+ uniprot+separator+ assScore+ separator+" \\\\"+'\n')
            COUNTER=COUNTER+1
    f.close()

def thous(x, sep=',', dot='.'):
    num, _, frac = str(x).partition(dot)
    num = re.sub(r'(\d{3})(?=\d)', r'\1'+sep, num[::-1])[::-1]
    if frac:
        num += dot + frac
    return num

if __name__ == "__main__":
    main(sys.argv[1:])