#!/usr/bin/python

#############################
# TO RUN:::
# python appendSubIDToFile.py -m subId2Name.txt -i /Users/joemullen/Desktop/SemSubNEW/DReSMin/Results/DB3WORK/RankedAss_V3_182/sub_scores.txt -o blaf.txt -s 0
#############################

import sys, getopt
import re
from sets import Set
import subprocess


def main(argv):
    mappingsfile = ''
    inputfile = ''
    outputfile = ''
    subNamePos = ''
    mapping = ''
    
    try:
        opts, args = getopt.getopt(argv,"hm:i:o:s:",["mfile=","ifile=","ofile=","subNamePos="])
    except getopt.GetoptError:
        print 'test.py -i <inputfile> -o <outputfile> -s <subNamePos>'
        sys.exit(4)
    for opt, arg in opts:
        if opt == '-h':
            print 'test.py -i <inputfile> -o <outputfile> -s <subNamePos>'
            sys.exit()
        elif opt in ("-m", "--mfile"):
            mappingsfile = arg
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
        elif opt in ("-s", "--subNamePos"):
            subNamePos = arg
    print 'Sub ID2Name extracting from ', mappingsfile
    print 'Input file', inputfile
    print 'Output file is ', outputfile
    print 'subNamePos in input file ', subNamePos

    try:
        fp = open(outputfile)
    except IOError:
    # If not exists, create the file
        fp = open(outputfile, 'w+')
    #get the mappings between subName and SubID
    mapping = getMappings(mappingsfile)
    createNewFile(inputfile, outputfile, mapping, subNamePos)
    fp.close

def getMappings(knownfile):
    known = {};
    for line in open(knownfile,'r').readlines():
        known[line.split("\t")[1].replace("\n", "")] = line.split("\t")[0];
    return known

def createNewFile(inputfile, outputfile, mappings, subNamePos):
    f = open(outputfile,'w')
    known = {};
    patt = re.compile("[^\t]+")
    for line in open(inputfile,'r').readlines():
        split = patt.findall(line)
        subName = split[int(subNamePos)]
        if subName in mappings:
           f.write(line.replace("\n", "")+'\t'+mappings.get(subName)+'\n')
    f.close()



if __name__ == "__main__":
    main(sys.argv[1:])