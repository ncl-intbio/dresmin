#!/usr/bin/python

import sys
import os
import gzip
import subprocess
import sys, getopt
import re

FILENAME=sys.argv[1]
CHUNKS=int(sys.argv[2])
CHUNKNUM=int(sys.argv[3])
CHUNKSTARTPOS=(CHUNKS*CHUNKNUM)-CHUNKS
CHUNKENDPOS=CHUNKSTARTPOS+CHUNKS
COUNTER =0
CHUNK=""
file = gzip.open(FILENAME, 'rb')

for line in file:
    if(line.startswith('IN:')):
        if(COUNTER >= CHUNKSTARTPOS and COUNTER < CHUNKENDPOS):
            CHUNK=CHUNK+line
        COUNTER=COUNTER+1

sys.stdout.write(str(CHUNK))

file.close()


