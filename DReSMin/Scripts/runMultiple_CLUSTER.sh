#!/bin/bash

START=$1
END=$2

while [ $START -lt $END ]; do
	qsub -l h_vmem=16g runSearch_CLUSTER.sh $START
	let COUNTER=COUNTER+1
done
