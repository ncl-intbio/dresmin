#!/usr/bin/python

#############################
# File takes the shortest path output and extracts the number of asoociations identified by the sub and the sub ID
#
# TO RUN:::
# python extractSubID2shortestPathNum.py -i /Users/joemullen/Desktop/SemSubNEW/DReSMin/Results/DB3WORK/ShortestPaths/paths_Collated.txt -o /Users/joemullen/Desktop/SemSubNEW/DReSMin/Scripts/subId2ShortestPath.txt
#############################

import sys, getopt
import re
from sets import Set
import subprocess


def main(argv):
    inputfile = ''
    outputfile = ''

    try:
        opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
    except getopt.GetoptError:
        print 'test.py -i <inputfile> -o <outputfile>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'test.py -i <inputfile> -o <outputfile>'
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
    print 'Input file', inputfile
    print 'Output file is ', outputfile

    try:
        fp = open(outputfile)
    except IOError:
    # If not exists, create the file
        fp = open(outputfile, 'w+')
    extractMappings(inputfile, outputfile)

def extractMappings(inputfile, outputfile):
    mappings = {};
    file = open(inputfile,"r")
    lines = file.read().splitlines()
    patt = re.compile("[^\n]+")
    for i in range(len(lines)):
        line = lines[i]
        numberSP =""
        if line.startswith("---------"):
            numberSP = line.replace("--", "")
            subID = lines[i+1]
            mappings[subID] = numberSP.replace("\n", "");
    file.close()
    f = open(outputfile,'w')
    for k,v in mappings.items():
        f.write(v+'\t'+k+'\n')
    f.close()


if __name__ == "__main__":
    main(sys.argv[1:])